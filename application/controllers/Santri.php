<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class santri extends CI_Controller {
    public function __construct()
    {        parent::__construct();

        $this->load->model("pembayaranbulanan_model");
        $this->load->model("santri_model");
        $this->load->model("sumbanganinstitusi_model");
        $this->load->model("pembayaranbulanan_model");
        if ($this->session->userdata('username')=="") {
            redirect('home');
        }
        $this->load->helper('text');
        //Harusnya dia ngeset
        /*if ($this->session->userdata('username')=="") {
            $dataq=array(
                'title'=>'Home',
                'active_home'=>'active',
            );
            $this->load->view('element/header_login',$dataq);
            $this->load->view('element/footer');
        }*/
    }

    public function index()
	{
		$data=array(
			'title'=>'Dashboard',
			'active_cos_dashboard'=>'active',
		);
		$this->load->view('element/header',$data);
		$this->load->view('element/menu_santri');
        $data['username'] = $this->session->userdata('username');
		$this->load->view('santri/dashboard', $data);
		$this->load->view('element/footer');
	}

	public function datasantri()
	{
        $nis = $this->session->userdata('nis');
		$data=array(
			'title'=>'Data Santri',
			'active_cos_datasantri'=>'active',
		);
		$this->load->view('element/header', $data);
		$this->load->view('element/menu_santri');
        $data['list'] = $this->santri_model->getAllRecordByNis();
		$this->load->view('santri/datasantri', $data);
		//echo $data;
		$this->load->view('element/footer');
	}

    public function sumbanganinstitusi()
    {
        $data=array(
            'title'=>'Pembayaran Sumbangan Institusi',
            'active_cos_sumbanganinstitusi'=>'active',
        );
        $this->load->view('element/header',$data);
        $this->load->view('element/menu_santri');
        $data['list_sumbanganinstitusi'] = $this->sumbanganinstitusi_model->getAllRecordByNis();
        $this->load->view('santri/sumbanganinstitusi', $data);
        $this->load->view('element/footer');
    }

    public function pembayaranbulanan()
	{
    $data=array(
      'title'=>'Pembayaran Bulanan',
			'active_cos_pembayaranbulanan'=>'active',
		);
		$this->load->view('element/header',$data);
		$this->load->view('element/menu_santri');
        $data['list_pembayaranbulanan'] = $this->pembayaranbulanan_model->getAllRecords();
		$this->load->view('santri/pembayaranbulanan', $data);
		$this->load->view('element/footer');
	}



}
