<div class="container" style="position:relative;left:90px;" class="col-md-5">
    <script>
        function simpan()
        {
            var frm = $('#simpanform');
            $.ajax({
                type: frm.attr('method'),
                url: frm.attr('action'),
                data: frm.serialize(),
                success: function (data) {
                    $('#pembayaranPendaftaran').modal('hide');
                    $('#notif').modal('show');

                    console.log('Submission was successful.');
                    console.log(data);
                },
                error: function (data) {
                    console.log('An error occurred.');
                    console.log(data);
                }
            });
        }
    </script>
    <div class="container">
    </div><br />
    <br/>
    <br/>
    <h3 style="text-align: center;">Data Pembayaran Pendaftaran</h3><hr>
    <button type="button" class="btn btn-info btn-sm" data-toggle="modal" data-target="#pembayaranPendaftaran">pembayaran pendaftaran</button>
    <!-- Modal -->
    <div class="modal fade" id="pembayaranPendaftaran" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <h4 class="modal-title" id="myModalLabel">Pembayaran Pendaftaran</h4>
                </div>
                <div class="modal-body">
                    <form id="simpanform" class="form-horizontal " action="<?php echo base_url(). 'pengurus/tambah_datasantri'; ?>" method="post">
                        <!--<input type="hidden" name="level" value="santri">-->
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Nama Lengkap</label>
                            <div class="col-sm-10">
                                <input type="text" name="nama_lengkap" class="form-control">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Jenis Santri</label>
                            <div class="col-sm-10">
                                <select name="jenis_biaya" class="form-control text-left" style="" id="sel1">
                                    <option>Pilih Jenis Santri</option>
                                    <option value="1">Menetap</option>
                                    <option value="2">Tidak Menetap</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Jumlah Bayar</label>
                            <div class="col-sm-10">
                                <input type="text"  name="jumlah_bayar" class="form-control">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Alamat</label>
                            <div class="col-sm-10">
                                <textarea type="text"  name="alamat" class="form-control"></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Nomor Telepon</label>
                            <div class="col-sm-10">
                                <input type="text"  name="no_tlp" class="form-control">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Tanggal Bayar</label>
                            <div class="col-sm-10">
                                <input type="date"  name="tgl_bayar" class="form-control" value="<?php echo date("Y-m-d",strtotime("today"));?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Password</label>
                            <div class="col-sm-10">
                                <input type="password" name="password" class="form-control">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Konfirmasi Password</label>
                            <div class="col-sm-10">
                                <input type="password" name="konfirmasi_password" class="form-control">
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" onclick="simpan();" class="simpan btn btn-primary">Simpan</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal -->

    <!-- Modal Detail-->
    <div class="modal fade" id="detail" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div   class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Data Pembayaran Pendaftaran</h4>
                </div>
                <div id="detailform"" class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <section class="panel">
                            <div class="panel-body">
                                <form class="form-horizontal " method="get">
                                    <table style="font-family:verdana; position:relative;top: -50px;">
                                        <form>
                                        </form>
                                    </table>
                            </div>
                        </section>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
            </div>
        </div>

    </div>
</div>
<!--Modal End Detail-->

<!--Modal Hapus-->
<div class="modal fade" id="hapus" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h3 class="modal-title">Apakah anda yakin akan menghapus pembayaran pendaftaran?</h3>
            </div>
            <div class="modal-body">
                Jika anda menghapus data tidak dapat dipulihkan kembali
            </div>
            <form action="<?=base_url()?>/ajax_pengurus/hapuspembayaranpendaftaran" id="hapusform" method="post">
                <input id="id_hapus" type="hidden" name="id_pembayaranpendaftaran">
            </form>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" onclick="hapus()" data-dismiss="modal">Hapus</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
            </div>
        </div>

    </div>
</div>
<!--Modal End Hapus-->

    <script>
        function detail(id)
        {
            $.ajax({url: base_url+"ajax_pengurus/detailpembayaranpendaftaran/"+id,
                success: function(result){
                    $('#detail').modal('show');

                    $("#detailform").html(result);

                }});
        }

        function hapus()
        {
            var frm = $('#hapusform');
            $.ajax({
                type: 'post',
                url: frm.attr('action'),
                data: frm.serialize(),
                success: function (data) {
                    location.reload();

                    alert(data);
                    console.log('Submission was successful.');
                    console.log(data);
                },
                error: function (data) {
                    alert("Terjadi kesalahan, jika masih berlanjut hubungi system admin");
                    location.reload();
                    console.log('An error occurred.');
                    console.log(data);
                },
            });
        }
        function updatehapus(row)
        {
            $("#id_hapus").val(row);
        }
    </script>
    <div >
        <table style="font-size:12px;" class="table table-striped table-bordered data">
            <thead>
            <tr>
                <th>No</th>
                <th>NIS</th>
                <th>Nama</th>
                <th>Jenis Santri</th>
                <th>Jumlah Bayar</th>
                <th>Tanggal Bayar</th>
                <th>Aksi</th>
            </tr>
            </thead>
            <tbody>

            <?php $no=1; foreach($list as $lists) {
                echo <<<HTML
            <tr>
                <td>$no</td>
                <td>$lists->nis</td>
                <td>$lists->nama_lengkap</td>
                <td>$lists->nama_biaya</td>
                <td>$lists->biayapendaftaran</td>
                <td>$lists->tanggal</td>
                <td>
                   <button type="button" class="btn btn-info btn-xs" onclick="detail($lists->id_pembayaranpendaftaran)"  data-target="#detail">Detail</button>
                    <button type="button" class="btn btn-danger btn-xs" onclick="updatehapus($lists->id_pembayaranpendaftaran)" data-toggle="modal" data-target="#hapus">Hapus</button>
                </td>
            </tr>
HTML;
                $no++;
            } ?>
            </tbody>
        </table>

    </div>

    </body>
    <script type="text/javascript">
        $(document).ready(function(){
            $('.data').DataTable({columnDefs: [
                { orderable: false, targets: -1 }
            ]});

        });
    </script>
</div>

