<div class="container" style="position:relative;left:90px;" class="col-md-5">
    <div class="container"></div><br />
    <br/><br/>
    <h3 style="text-align: center;">Pembayaran Bulanan</h3><hr>
    <button type="button" class="btn btn-info btn-sm" data-toggle="modal" data-target="#pembayaranBulanan">Bayar</button>
    <br><br>
    <div>
        <!-- Modal Pembayaran Bulanan-->
        <div class="modal fade" id="pembayaranBulanan" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Pembayaran Bulanan</h4>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-12">
                                <section class="panel">
                                    <div class="panel-body">
                                        <form id="formBulanan" action="" class="form-horizontal" style="position: relative;top: -30px;" method="post">
                                            <table style="top: -20px;font-family:verdana;top:-35px;position: relative;">
                                                <tr>
                                                    <td><h5>NIS<h5></td>
                                                    <td><h5>:</h5></td>
                                                    <td><h5>17002008</h5></td>
                                                </tr>
                                                <tr>
                                                    <td><h5>Nama<h5></td>
                                                    <td><h5>:</h5></td>
                                                    <td><h5>Budi</h5></td>
                                                </tr>
                                                <tr>
                                                    <td><h5>Jenis Santri</h5></td>
                                                    <td><h5>:</h5></td>
                                                    <td><h5>Menetap</h5></td>
                                                </tr><br>
                                                <tr>
                                                    <td><h5>Bulan Bayar</h5></td>
                                                    <td><h5>:</h5></td>
                                                    <td><select style="font-size: 12px;"><option>Pilih Bulan Bayar</option>
                                                            <option value="januari">Januari</option>
                                                            <option value="februari">Februari</option>
                                                            <option value="maret">Maret</option>
                                                            <option value="april">April</option>
                                                            <option value="mei">Mei</option>
                                                            <option value="juni">Juni</option>
                                                            <option value="juli">Juli</option>
                                                            <option value="agustus">Agustus</option>
                                                            <option value="september">September</option>
                                                            <option value="oktober">Oktober</option>
                                                            <option value="nopember">Nopember</option>
                                                            <option value="desember">Desember</option>
                                                        </select>
                                                    </td>
                                                </tr><br>
                                                <tr>
                                                    <td><h5>Rincian Pembayaran</h5></td>
                                                    <td><h5>:</h5></td>
                                                    <td><h5>
                                                            <table style="font-size:12px;" class="table table-striped table-bordered">
                                                                <thead>
                                                                <tr>
                                                                    <th>No</th>
                                                                    <th>Nama</th>
                                                                    <th>Jumlah</th>
                                                                </tr>
                                                                </thead>
                                                                <tr>
                                                                    <td>1</td>
                                                                    <td>Uang Makan</td>
                                                                    <td value="360000">360000</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>2</td>
                                                                    <td>Syariah Pondok</td>
                                                                    <td value="35000">35000 </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>3</td>
                                                                    <td>Khidmad Manaqib
                                                                    </td>
                                                                    <td value="1000">1000</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>4</td>
                                                                    <td>Syariah Diniyah
                                                                    </td>
                                                                    <td value="15000">15000</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>5
                                                                    </td>
                                                                    <td>Tabungan Haul
                                                                    </td>
                                                                    <td value="15000">15000</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>5
                                                                    </td>
                                                                    <td>Donatur
                                                                    </td>
                                                                    <td><input name="donatur" class="form-control" ></td>
                                                                </tr>
                                                                </tbody>
                                                            </table>
                                                        </h5></td>
                                                </tr>
                                                <tr>
                                                    <td><h5>Upload Bukti Pembayaran</h5></td>
                                                    <td>:</td>
                                                    <td><input type="file" name="uploadBuktiPembayaran" value=""></td>
                                                </tr>
                                            </table>
                                        </form>
                                    </div>
                                </section>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-info" onclick="simpan();" data-dismiss="modal">Simpan</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
                    </div>
                </div>
            </div>
                </div>
        <!--Modal Bayar End-->

        <!-- Modal Detail-->
        <div class="modal fade" id="detail" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Data Pembayaran Bulanan</h4>
                    </div>
                    <div class="modal-body" id="detailform">
                        <div class="row">
                            <div class="col-md-12">
                                <section class="panel">
                                    <div class="panel-body">
                                        <form class="form-horizontal " method="get">
                                            <table style="font-family:verdana;">

                                        </form>
                                        </table>
                                    </div>
                                </section>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
                    </div>
                </div>
            </div>
        </div>
        <!--Modal Detail End-->

        <script>
            function detail(id)
            {
                $.ajax({url: base_url+"ajax_santri/detailpembayaranbulanan/"+id,
                    success: function(result){
                        $('#detail').modal('show');
                        $("#detailform").html(result);

                    }});
            }
            function simpan()
            {
                var frm = $('#formBulanan');
                $.ajax({
                    type: frm.attr('method'),
                    url: frm.attr('action'),
                    data: frm.serialize(),
                    success: function (data) {
                        location.reload();
                        alert(data);
                        console.log('Submission was successful.');
                        console.log(data);
                    },
                    error: function (data) {
                        alert("Terjadi kesalahan, jika masih berlanjut hubungi system admin");

                        console.log('An error occurred.');
                        console.log(data);
                    },
                });
            }
        </script>

        <table style="font-size:12px;" class="table table-striped table-bordered data">
          <thead>
            <tr>
              <th>No</th>
              <th>Bulan</th>
              <th>Jumlah Bayar</th>
              <th>Tanggal Bayar</th>
              <th>Donasi</th>
              <th>Aksi</th>
            </tr>
          </thead>
          <tbody>
          <?php $no=1;foreach($list_pembayaranbulanan as $lists) {
              echo <<<HTML
            <tr>
                <td>$no</td>
                <td>$lists->bulan_bayar</td>
                <td>$lists->bulan_bayar</td>
                <td>$lists->tanggal</td>
                <td>$lists->donasi</td>
                <td>
                    <button type="button" class="btn btn-success btn-xs" data-toggle="modal" data-target="#detail">Belum Konfirmasi</button>
                    <button type="button" class="btn btn-info btn-xs"  data-toggle="modal" data-target="#detail">Detail</button>
                </td>
            </tr>
HTML;
              $no++;

          } ?>
          </tbody>
        </table>
    </div>
</div>
