<?php

class Admin_Model extends CI_Model{
    public $table = 'admin';
    public $primary_key = 'id_admin';
    function __construct(){
        parent::__construct();
    }
    function getAllRecords(){
        return $this->db->get($this->table)->result();

    }
    //function login
    function login($username,$password)
    {
        $password = md5($password);
        return $this->db->select('id_admin')
            ->where('id_admin', $username)
            ->where('password', $password)
            ->get($this->table . ' AS u')
            ->result();
    }



}
