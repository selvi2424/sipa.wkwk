<body>
    <script src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>
    <!-- container section start -->
    <section id="container" class="">
        <header class="header dark-bg">
              <div class="toggle-nav">
                  <div class="icon-reorder tooltips" data-original-title="Toggle Navigation" data-placement="bottom"><i class="icon_menu"></i></div>
              </div>
              <!--logo start-->
              <a class="logo">SISTEM PEMBAYARAN ADMINISTRASI<span class="lite"></span></a>
              <!--logo end-->
              <div class="top-nav notification-row">

                  <!-- notificatoin dropdown start-->
                  <ul class="nav pull-right top-menu">

                      <!-- Percakapan Baru-->
                      <li id="mail_notificatoin_bar" class="dropdown">
                          <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                              <i class="icon-envelope-l"></i>
                              <span class="badge bg-important">Mulai Pesan</span>
                          </a>
                          <ul class="dropdown-menu extended ">
                              <div class="notify-arrow notify-arrow-blue"></div>
                              <li>
                                  <p class="blue">Pesan Baru</p>
                              </li>
                              <li>
                                  <input type="text" name="search" class="form-control " placeholder="Search..">
                                  <a href="#" data-toggle="modal" data-target="#pesan">
                                      <span class="photo"><img alt="avatar" src="<?php echo base_url();?>assets//img/avatar-mini.jpg"></span>
                                      <span class="subject">
                                      <span class="from">Sari</span>
                                      <span class="time">1 min</span>
                                      </span>
                                  </a>
                              </li>
                          </ul>
                      </li>
                      <!-- inbox notificatoin end -->
                      <!-- inbox notificatoin start-->
                      <li id="mail_notificatoin_bar" class="dropdown">
                          <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                              <i class="icon-envelope-l"></i>
                              <span class="badge bg-important">5</span>
                          </a>
                          <ul class="dropdown-menu extended inbox">
                              <div class="notify-arrow notify-arrow-blue"></div>
                              <li>
                                  <p class="blue">5 Pesan</p>
                              </li>
                              <li>
                                  <a href="#" data-toggle="modal" data-target="#pesan">
                                      <span class="photo"><img alt="avatar" src="<?php echo base_url();?>assets//img/avatar-mini.jpg"></span>
                                      <span class="subject">
                                      <span class="from">Sari</span>
                                      <span class="time">1 min</span>
                                      </span>
                                      <span class="message">
                                          Kapan pendaftaran dibuka?
                                      </span>
                                  </a>
                              </li>
                          </ul>
                      </li>
                      <!-- inbox notificatoin end -->
                      <!-- alert notification start-->
                      <li id="alert_notificatoin_bar" class="dropdown">
                          <a data-toggle="dropdown" class="dropdown-toggle" href="#">

                              <i class="icon-bell-l"></i>
                              <span class="badge bg-important">2</span>
                          </a>
                          <ul class="dropdown-menu extended notification">
                              <div class="notify-arrow notify-arrow-blue"></div>
                              <li>
                                  <p class="blue">2 Notifikasi</p>
                              </li>
                              <li>
                                  <a href="#" data-toggle="modal" data-target="#notif_sumbanganinstitusi">
                                      <span class="label label-primary"><i class="icon_profile"></i></span>
                                      Pembayaran Sumbangan Institusi
                                      <span class="small italic pull-right">1 Notifikasi</span>
                                  </a>
                                  <a href="#" data-toggle="modal" data-target="#notif_pembayaranbulanan">
                                      <span class="label label-success"><i class="icon_profile"></i></span>
                                      Pembayaran Bulanan
                                      <span class="small italic pull-right">1 Notifikasi</span>
                                  </a>
                              </li>
                          </ul>
                      </li>
                      <!-- alert notification end-->
                      <!-- user login dropdown start-->
                      <li class="dropdown">
                          <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                              <span class="profile-ava">
                                  <img alt="" src="<?php echo base_url();?>assets/img/avatar1_small.jpg">
                              </span>
                              <span class="username">budi</span>
                              <b class="caret"></b>
                          </a>
                          <ul class="dropdown-menu extended logout">
                              <div class="log-arrow-up"></div>
                              <li>
                                  <a href="" data-toggle="modal" data-target="#gantipassword"><i class="icon_user"></i> Sunting Password</a>
                              </li>
                              <div class="log-arrow-up"></div>
                              <li>
                                  <a href="<?php echo base_url().'auth/logout'?>"><i class="icon_key_alt"></i> Log Out</a>
                              </li>
                          </ul>
                          <ul class="dropdown-menu extended logout">

                          </ul>
                      </li>
                      <!-- user login dropdown end -->
                  </ul>
                  <!-- notificatoin dropdown end-->
              </div>
        </header>
        <!--header end-->

        <!-- Modal Tambah Pendaftaran -->
        <div class="modal fade" id="gantipassword" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div  class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Sunting Password</h4>
                    </div>
                    <div class="modal-body">
                        <form class="form-horizontal" role="form" action="" method="post">
                            <div class="form-group" data-date-format="dd MM yyyy" >
                                <label class="col-sm-2 control-label">Password Lama</label>
                                <div class="col-sm-10">
                                    <input type="password" class="form-control" name="password_lama"
                                           placeholder=""/>
                                </div>
                            </div>
                            <div class="form-group" >
                                <label class="col-sm-2 control-label"
                                       for="inputPassword3" >Password Baru</label>
                                <div class="col-sm-10">
                                    <input type="password" class="form-control" name="password_baru"
                                           placeholder=""/>
                                </div>
                            </div>
                            <div class="form-group" >
                                <label class="col-sm-2 control-label"
                                       for="inputPassword3" >Konfirmasi Password</label>
                                <div class="col-sm-10">
                                    <input type="password" class="form-control" name="konfirmasi_password"
                                           placeholder=""/>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-info">Simpan</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
                    </div>
                </div>

            </div>
        </div>
        <!-- Modal End-->

        <!--sidebar start-->
        <aside>
            <div id="sidebar"  style="padding: -60px;" class="nav-collapse ">
                <!-- sidebar menu start-->
                <ul class="sidebar-menu">
                    <li class="<?php if(isset($active_cpengurus_dashboard)){echo $active_cpengurus_dashboard;;}?>">
                        <a href="<?php echo base_url('pengurus');?>">
                            <i class="icon_house_alt"></i>
                            <span>Dashboard</span>
                        </a>
                    </li>
                    <li class="<?php if(isset($active_cpengurus_datasantri)){echo $active_cpengurus_datasantri;}?>">
                        <a href="<?php echo base_url('pengurus/datasantri');?>">
                            <i class="icon_genius"></i>
                            <span>Data Santri</span>
                        </a>
                    </li>
                    <li class="<?php if(isset($active_cpengurus_pembayaranpendaftaran)){echo $active_cpengurus_pembayaranpendaftaran;}?>">
                        <a href="<?php echo base_url('pengurus/pembayaranpendaftaran');?>">
                            <i class="icon_genius"></i>
                            <span style="font-size:14px;">Pembayaran <br>Pendaftaran</span>
                        </a>
                    </li>
                    <li class="<?php if(isset($active_cpengurus_sumbanganinstitusi)){echo $active_cpengurus_sumbanganinstitusi;}?>">
                        <a href="<?php echo base_url('pengurus/sumbanganinstitusi');?>">
                            <i class="icon_genius"></i>
                            <span style="font-size:14px;">Sumbangan<br> Institusi </span>
                        </a>
                    </li>
                    <li class="<?php if(isset($active_cpengurus_pembayaranbulanan)){echo $active_cpengurus_pembayaranbulanan;}?>">
                        <a href="<?php echo base_url('pengurus/pembayaranbulanan');?>">
                            <i class="icon_genius"></i>
                            <span style="font-size:14px;">Pembayaran<br> Bulanan </span>
                        </a>
                    </li>
                    <li class="<?php if(isset($active_cpengurus_informasi)){echo $active_cpengurus_informasi;}?>">
                        <a href="<?php echo base_url('pengurus/informasi');?>">
                            <i class="icon_genius"></i>
                            <span>Informasi</span>
                        </a>
                    </li>
                    <li class="sub-menu">
                          <a  class="">
                              <i class="icon_genius"></i>
                              <span>Perubahan</span>
                              <span class="menu-arrow arrow_carrot-right"></span>
                          </a>
                          <ul class="sub">
                              <li><a class="" href="<?php echo base_url('pengurus/perubahanbiaya');?>">Biaya</a></li>
                              <li><a class="" href="<?php echo base_url('pengurus/perubahanwaktu');?>">Waktu Pendaftaran</a></li>
                          </ul>
                    </li>
                    <li class="<?php if(isset($active_cpengurus_rekapdata)){echo $active_cpengurus_rekapdata;}?>">
                        <a href="<?php echo base_url('pengurus/rekapdata');?>">
                            <i class="icon_genius"></i>
                            <span>Rekap Data</span>
                        </a>
                    </li>
                </ul>
                <!-- sidebar menu end-->
            </div>
        </aside>
        <!--sidebar end-->
        <!-- Modal Tambah Pendaftaran -->
        <div class="modal fade" id="pesan" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div   class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h3 class="modal-title">Pesan</h3>
                        <div class="dropdown" >
                            <button class="btn btn-default btn-xs dropdown-toggle" type="button" id="menu1" data-toggle="dropdown">opsi
                                <span class="caret"></span></button>
                            <ul class="dropdown-menu" role="menu" aria-labelledby="menu1">
                                <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Hapus Percakapan</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="modal-body">

                    </div>
                    <div class="modal-footer">
                        <tr><td><textarea name="pesan" type="text" cols="2000" class="form-control"></textarea></td>
                        <br><button id="kirim" name="kirim" class="btn btn-info">Kirim</button></tr>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
                    </div>
                </div>

            </div>
        </div>
        <!-- Modal End-->

        <!-- Modal Tambah Notifikasi Sumbangan Institusi-->
        <div class="modal fade" id="notif_sumbanganinstitusi" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div  class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h3 class="modal-title">Notifikasi</h3>
                        <div class="dropdown" >
                            <button class="btn btn-default btn-xs dropdown-toggle" type="button" id="menu1" data-toggle="dropdown">opsi
                                <span class="caret"></span></button>
                            <ul class="dropdown-menu" role="menu" aria-labelledby="menu1">
                                <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Hapus Pemberitahuan</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="modal-body">
                        <form class="form-horizontal" role="form">
                            <div class="form-group" data-date-format="dd MM yyyy" >
                                <label class="col-sm-2 control-label">Sintia<br>12001001</label>
                                <div class="col-sm-10 active">
                                    Pembayaran Sumbangan Institusi Telah Masuk Tanggal 12/13/2017 00:00:00
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
                    </div>
                </div>

            </div>
        </div>
        <!-- Modal End-->

        <!-- Modal Tambah Notifikasi Pembayaran Bulanan-->
        <div class="modal fade" id="notif_pembayaranbulanan" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div   class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h3 class="modal-title">Notifikasi</h3>
                        <div class="dropdown" >
                            <button class="btn btn-default btn-xs dropdown-toggle" type="button" id="menu1" data-toggle="dropdown">opsi
                                <span class="caret"></span></button>
                            <ul class="dropdown-menu" role="menu" aria-labelledby="menu1">
                                <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Hapus Notifikasi</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="modal-body">
                        <form class="form-horizontal" role="form">
                            <div class="form-group" data-date-format="dd MM yyyy" >
                                <label class="col-sm-2 control-label">Sintia<br>12001001</label>
                                <div class="col-sm-10 active" >
                                    Pembayaran Biaya Bulanan Telah Masuk Tanggal 12/13/2017 00:00:00
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
                    </div>
                </div>

            </div>
        </div>
        <!-- Modal End-->