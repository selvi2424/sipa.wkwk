<link href="<?php echo base_url();?>assets/css/bar-chart.css" rel="stylesheet">
<!--main content start-->
<section id="main-content">
    <section class="wrapper">
        <!--overview start-->
        <div class="progress">
            <div class="progress-bar progress-bar-success" role="progressbar" style="width:25%;">
               Pendaftaran
            </div>
            <div class="progress-bar progress-bar-default" role="progressbar" style="width:25%">
                Cicilan Sumbangan Institusi 1
            </div>
            <div class="progress-bar progress-bar-default" role="progressbar" style="width:25%">
                Cicilan Sumbangan Institusi 2
            </div>
            <div class="progress-bar progress-bar-default" role="progressbar" style="width:25%">
                Cicilan Sumbangan Institusi 3
            </div>
        </div>
        <div class="row">
            <div class="col-lg-4 col-md-3 col-sm-12 col-xs-12">
                <div class="info-box blue-bg">
                    <div style="font-size: 21px;text-align: center;">
                        Pengunjung Aktif
                    </div>
                    <div style="text-align: center;" class="count">100</div>
                </div><!--/.info-box-->
            </div><!--/.col-->

            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                <div class="info-box brown-bg">
                    <div style="font-size: 21px;text-align: center;">Jumlah Pendaftar 2017</div>
                    <form>
                        <table>
                            <tr style="color:white;"class="">
                                <td>Santri Menetap</td>
                                <td>:</td>
                                <td>105</td>
                            </tr><br>
                            <tr style="color:white;"class="">
                                <td>Santri Tidak Menetap</td>
                                <td>:</td>
                                <td>105</td>
                            </tr>
                        </table>
                    </form>
                </div><!--/.info-box-->
            </div><!--/.col-->

            <div class="col-lg-5 col-md-3 col-sm-12 col-xs-12">
                <div class="info-box green-bg">
                    <div style="font-size: 21px;text-align: center;">Waktu Pendaftaran</div><br class="">
                    <div style=" font-size: 25px;text-align: center;"><strong>00:22:30:45</strong></div>
                </div><!--/.info-box-->
            </div><!--/.col-->
            <div class="col-lg-6 col-md-3 col-sm-12 col-xs-12">
                <div class="info-box dark-bg">
                    <div style="font-size: 21px;text-align: center;">Sudah Konfirmasi</div>
                    <div>
                        <div style="font-size: 18px;">Sumbangan Institusi</div>
                        <form>
                            <table>
                                <tr style="color:white;"class="">
                                    <td>Lunas</td>
                                    <td>:</td>
                                    <td>105</td>
                                </tr>
                                <tr style="color:white;"class="">
                                    <td>Belum Lunas</td>
                                    <td>:</td>
                                    <td>105</td>
                                </tr>
                            </table>
                        </form>
                    </div>
                    <div>
                        <div style="font-size: 18px;">Pembayaran Bulanan (Januari)</div>
                        <form>
                            <table>
                                <tr style="color:white;"class="">
                                    <td>Lunas</td>
                                    <td>:</td>
                                    <td>105</td>
                                </tr>
                                <tr style="color:white;"class="">
                                    <td>Belum Lunas</td>
                                    <td>:</td>
                                    <td>105</td>
                                </tr>
                            </table>
                        </form>
                    </div>
                </div><!--/.info-box-->
            </div><!--/.info-box-->
            <div class="col-lg-6 col-md-3 col-sm-12 col-xs-12">
                <div class="info-box dark-bg">
                    <div style="font-size: 21px;text-align: center;">Belum Konfirmasi</div>
                    <div>
                        <div style="font-size: 18px;">Sumbangan Institusi</div>
                        <form>
                            <table>
                                <tr style="color:white;"class="">
                                    <td>Lunas</td>
                                    <td>:</td>
                                    <td>105</td>
                                </tr>
                                <tr style="color:white;"class="">
                                    <td>Belum Lunas</td>
                                    <td>:</td>
                                    <td>105</td>
                                </tr>
                            </table>
                        </form>
                    </div>
                    <div>
                        <div style="font-size: 18px;">Pembayaran Bulanan (Januari)</div>
                        <form>
                            <table>
                                <tr style="color:white;"class="">
                                    <td>Lunas</td>
                                    <td>:</td>
                                    <td>105</td>
                                </tr>
                                <tr style="color:white;"class="">
                                    <td>Belum Lunas</td>
                                    <td>:</td>
                                    <td>105</td>
                                </tr>
                            </table>
                        </form>
                    </div>
                </div><!--/.info-box-->
            </div><!--/.info-box-->
        </div><!--/.col-->
        </div><!--/.row-->
        <div>
            <script src="<?php echo base_url();?>assets/js/graph.js"></script>
    </section>
</section>
<!--main content end-->
</section>
</body>