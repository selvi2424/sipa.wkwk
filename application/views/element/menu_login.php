<body>
    <section id="container" class="">
        <header class="header dark-bg">
              <div class="toggle-nav">
                  <div class="icon-reorder tooltips" data-original-title="Toggle Navigation" data-placement="bottom"><i class="icon_menu"></i></div>
              </div>
              <!--logo start-->
              <a class="center-block" style="position:relative; top:10px; font;color: #cccccc; font-family: verdana;font-size: 20px;text-align: center">Sistem Informasi Pembayaran Administrasi<br>Pondok Pesantren Assalafi AlFithrah Meteseh Semarang<br><span class="lite"></span></a>

            <br>
            <!--logo end-->
              <div class="top-nav notification-row">
                  <!-- notificatoin dropdown start-->
                  <ul class="nav pull-right top-menu">
                  </ul>
                  <!-- notificatoin dropdown end-->
              </div>
          <!--sidebar start-->
        </header> <br>
        <!--header end-->
        <!--sidebar start-->
         <aside>
            <div id="sidebar"  class="nav-collapse ">
                <!-- sidebar menu start-->
                <ul class="sidebar-menu text-center" style="font-family: verdana;" >
                   <label for="sel1" class="text-center" style="color: white;font-size: 18px;">Login</label></>
                   <br>
                <form action="auth/cek_login" method="post">
                    <label class="text-left" style="color: white;font-family: verdana;font-size: 10px;">ID Masuk: </label>
                    <input type="username" style="font-size: 10px;color: #000000;" placeholder="" name="username" class="form-control">
                    <label style="color: white;font-family: verdana;font-size: 10px;" >Password: </label>
                    <input style="color: #000000;" type="password" placeholder="" name="password" class="form-control"><br>
                   <button type="submit"  class="btn btn-default btn-sm">Masuk</button>
                </form>
                </ul>
                <!-- sidebar menu end-->
              </div>
            </aside>
            <!--sidebar end-->