<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ajax_admin extends CI_Controller {
    protected $base_url;
    public function __construct()
    {        parent::__construct();

        $this->load->model("santri_model");
        $this->load->model("sumbanganinstitusi_model");
        $this->load->model("pembayaranbulanan_model");
        $this->load->model("pengurus_model");
        $this->base_url = base_url();
    }

//Santri
    function simpanubahsantri()
    {
        $id =$this->input->post('nis');
        $this->load->library('upload');
        $filename = "file_" . time(); //nama file + fungsi time
        $config['upload_path'] = './assets/uploads_fotosantri/'; //Folder untuk menyimpan hasil upload
        $config['allowed_types'] = 'jpg|png|jpeg'; //type yang dapat diakses bisa anda sesuaikan
        $config['max_size'] = '2048'; //maksimum besar file 3M
        $config['max_width'] = '1288'; //lebar maksimum 5000 px
        $config['max_height'] = '768'; //tinggi maksimu 5000 px
        $config['file_name'] = $filename; //nama yang terupload nantinya

        $this->upload->initialize($config);

        if ($_FILES['filefoto']['name']) {
            if ($this->upload->do_upload('filefoto')) {
                $gambar = $this->upload->data();
                $data = array(
                    'foto_santri' => $gambar,
                    'nama_lengkap' => $this->input->post('nama_lengkap'),
                    'nama_panggilan' => $this->input->post('nama_lengkap'),
                    'jenkel' => $this->input->post('jenkel'),
                    'tempat_lahir' => $this->input->post('tempat_lahir'),
                    'tgl_lahir' => $this->input->post('tgl_lahir'),
                    'kewarganegaraan' => $this->input->post('kewarganegaraan'),
                    'anak_ke' => $this->input->post('anak_ke'),
                    'jmlh_saudaraKandung' => $this->input->post('saudara_kandung'),
                    'jmlh_saudaraTiri' => $this->input->post('saudara_tiri'),
                    'jmlh_saudaraAngkat' => $this->input->post('saudara_angkat'),
                    'pendidikan_terakhir' => $this->input->post('pendidikan_terakhir'),
                    'tahun_lulus' => $this->input->post('tahun_lulus'),
                    'alamat' => $this->input->post('alamat'),
                    'propinsi' => $this->input->post('propinsi'),
                    'kabupaten' => $this->input->post('kabupaten'),
                    'no_tlp' => $this->input->post('no_tlp'),
                    'nama_ayah' => $this->input->post('nama_ayah'),
                    'pendidikan_ayah' => $this->input->post('pendidikan_ayah'),
                    'pekerjaan_ayah' => $this->input->post('pekerjaan_ayah'),
                    'nama_ibu' => $this->input->post('nama_ibu'),
                    'pendidikan_ibu' => $this->input->post('pendidikan_ibu'),
                    'pekerjaan_ibu' => $this->input->post('pekerjaan_ibu'),
                    'nama_wali' => $this->input->post('nama_wali'),
                    'pendidikan_wali' => $this->input->post('pendidikan_wali'),
                    'pekerjaan_wali' => $this->input->post('pekerjaan_wali'),
                    'tinggi_badan' => $this->input->post('tinggi_badan'),
                    'berat_badan' => $this->input->post('berat_badan'),
                    'gol_darah' => $this->input->post('goldar'),
                    'jenis_penyakit' => $this->input->post('jenis_penyakit'),
                    'lama_sakit' => $this->input->post('lama_sakit'),
                    'tahun_sakit' => $this->input->post('tahun_sakit'),
                    'cacat' => $this->input->post('cacat'),
                    'hobi' => $this->input->post('hobi'),
                    'prestasi' => $this->input->post('prestasi'),
                    'mapel' => $this->input->post('mapel'),
                    'cita_cita' => $this->input->post('cita_cita'),
                    'surat_pernyataan' => $this->input->post('surat_pernyataan'),
                    'tgl_penetapan' => $this->input->post('tgl_penetapan'),

                );
                if (isset($data) && $this->santri_model->updateSantri($id, $data)) {
                    echo "Data Berhasil Disimpan";
                } else {
                    echo "Data Gagal Disimpan";
                }
                $this->datasantri();
            }
        }
    }

//Sumbangan Institusi
    public function simpansumbanganinstitusi()
    {
        $data = array(
            'id_informasi' => $this->input->post('id_informasi'),
            'isi_informasi' => $this->input->post('isi_informasi'),
        );

        if(isset($data) && $this->informasi_model->insertInformasi($data))
        {
            echo "Data Berhasil Disimpan";
        }
        else {
            echo "Data Gagal Disimpan";
        }
    }

//Pembayaran Bulanan
    public function detailpembayaranbulanan($id)
    {
        $data = $this->pembayaranbulanan_model->get_by_id($id);
        echo <<<HTML
         <div class="row">
            <div class="col-md-12">
                <section class="panel">
                    <div class="panel-body">
                        <form class="form-horizontal" id="detailform" method="get">
                            <table style="font-family:verdana; position:relative;top: -10px;">
                            <tr>
                                <td><h5>NIS<h5></td>
                                <td><h5>:</h5></td>
                                <td><h5>$data->nis</h5></td>
                            </tr>
                            <tr>
                                <td><h5>Nama</h5></td>
                                <td><h5>:</h5></td>
                                <td><h5>$data->nama_lengkap</h5></td>
                            </tr>
                            <tr>
                                <td><h5>Jenis Santri</h5></td>
                                <td><h5>:</h5></td>
                                <td><h5>$data->nama_biaya</h5></td>
                            </tr>
                            <tr>
                                <td><h5>Tanggal Bayar</h5></td>
                                <td><h5>:</h5></td>
                                <td><h5>$data->tanggal</h5></td>
                            </tr>
                            <tr>
                                <td><h5>Bukti Bayar</h5></td>
                                <td><h5>:</h5></td>
                                <td><img src="wrongname.gif" alt="HTML5 Icon" style="width:128px;height:128px;"></td>
                            </tr>
                            <tr>
                                <td><h5>Rincian Pembayaran</h5></td>
                                <td><h5>:</h5></td>
                                <td><h5>
                                        <table style="font-size:12px;" class="table table-striped table-bordered">
                                            <thead>
                                            <tr>
                                                <th>No</th>
                                                <th>Nama</th>
                                                <th>Jumlah</th>
                                            </tr>
                                            </thead>
                                            <tr>
                                                <td>1</td>
                                                <td>Khidmad Manaqib
                                                </td>
                                                <td>$data->khidmad_manaqib</td>
                                            </tr>
                                            <tr>
                                                <td>2</td>
                                                <td>Syariah Diniyah
                                                </td>
                                                <td>$data->syariah_diniyah</td>
                                            </tr>
                                            <tr>
                                                <td>3
                                                </td>
                                                <td>Syariah TPQ
                                                </td>
                                                <td>$data->syariah_tpq</td>
                                            </tr>
                                            <tr>
                                                <td>4</td>
                                                <td>Donatur</td>
                                                <td>$data->donatur</td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </h5></td>
                            </tr>
                             </table>
                            </form>
                      </div>
                </section>
            </div>   
        </div>
HTML;
    }
    public function simpanpembayaranbulanan()
    {
        $data = array(
            'id_informasi' => $this->input->post('id_informasi'),
            'isi_informasi' => $this->input->post('isi_informasi'),
        );

        if(isset($data) && $this->informasi_model->insertInformasi($data))
        {
            echo "Data Berhasil Disimpan";
        }
        else {
            echo "Data Gagal Disimpan";
        }
    }

}
