<?php

class Informasi_Model extends CI_Model{
    public $table = 'informasi';
    public $primary_key = 'id_informasi';
    function __construct(){
        parent::__construct();
    }

    function getAllRecords(){
        return $this->db->get($this->table)->result();

    }
    function insertInformasi($data){
        $result =  $this->db->insert($this->table,$data);
        return $result; // Kode ini digunakan untuk mengembalikan hasil $res

    }

    public function get_by_id($id)
    {
        $query =   $this->db->select('*')
            ->from('informasi')
            ->where('id_informasi',$id)
            ->get();
        return $query->row();

    }

    function updateInformasi($informasi_id,$data){
        $this->db->where('id_informasi',$informasi_id); // Kode ini digunakan untuk merubah record yang sudah ada dalam sebuah tabel
        $result = $this->db->update($this->table, $data); // Kode ini digunakan untuk merubah record yang sudah ada dalam sebuah tabel
        return $result;
    }
    public function deleteInformasi($informasi_id){
        $this->db->where('id_informasi',$informasi_id); // Kode ini digunakan untuk merubah record yang sudah ada dalam sebuah tabel

        $result = $this->db->delete($this->table); // Kode ini digunakan untuk menghapus record yang sudah ada
        return $result;
    }
}
