
<div class="container" style="position:relative;left:90px;" class="col-md-5">
    <div class="container">
    </div><br />
    <br/>
    <br/>
    <h3 style="text-align: center;">Daftar Informasi</h3><hr>
    <button type="button" class="btn btn-info btn-sm" data-toggle="modal" data-target="#tambahInformasi">Tambah Informasi</button><br><br>

    <!-- Modal Tambah Informasi -->
    <div class="modal fade" id="tambahInformasi" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Tambah Informasi</h4>
                </div>
                <div class="modal-body">
                    <form id="formInformasi" action="<?php echo base_url(). 'ajax_pengurus/simpaninformasi'; ?>" method="post">
                        <textarea class="form-control" rows="8" name="isi_informasi" id="isi_informasi" ></textarea>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" onclick="simpan();" class="simpan btn btn-primary">Simpan</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>

                </div>
            </div>

        </div>
    </div>
    <!-- Modal End-->

    <!-- Modal Sunting Informasi -->
    <div class="modal fade" id="sunting" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div  class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Sunting Informasi</h4>
                </div>
                <div class="modal-body" id="suntingform">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-info" onclick="simpanperubahan()">Simpan</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>

                </div>
            </div>

        </div>
    </div>
    <!--Modal Sunting End-->

    <!--Modal Hapus-->
    <div class="modal fade" id="hapus" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h3 class="modal-title">Apakah anda yakin akan menghapus informasi?</h3>
                </div>
                <div class="modal-body">
                    Jika anda menghapus data tidak dapat dipulihkan kembali
                </div>
                <form action="<?=base_url()?>/ajax_pengurus/hapusinformasi" id="hapusform" method="post">
                    <input id="id_hapus" type="hidden" name="id_informasi">
                </form>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" onclick="hapus()" data-dismiss="modal">Hapus</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
                </div>
            </div>

        </div>
    </div>
    <!--Modal Hapus End-->

    <script>
        function simpanperubahan(){
            var frm = $('#ubahinformasi');
            $.ajax({
                type: frm.attr('method'),
                url: frm.attr('action'),
                data: frm.serialize(),
                success: function (data) {
                    alert(data);
                    location.reload();
                    console.log('Submission was successful.');
                    console.log(data);
                },
                error: function (data) {
                    alert("Terjadi kesalahan, jika masih berlanjut hubungi system admin");

                    console.log('An error occurred.');
                    console.log(data);
                },
            });
        }
        function sunting(id)
        {
            $.ajax({url: base_url+"ajax_pengurus/ubahinformasi/"+id,
                success: function(result){
                    $('#sunting').modal('show');

                    $("#suntingform").html(result);

                }});
        }
        function simpan()
        {
            var frm = $('#formInformasi');
            $.ajax({
                type: frm.attr('method'),
                url: frm.attr('action'),
                data: frm.serialize(),
                success: function (data) {
                    location.reload();
                    alert(data);
                    console.log('Submission was successful.');
                    console.log(data);
                },
                error: function (data) {
                    alert("Terjadi kesalahan, jika masih berlanjut hubungi system admin");

                    console.log('An error occurred.');
                    console.log(data);
                },
            });
        }
        function hapus()
        {
            var frm = $('#hapusform');
            $.ajax({
                type: 'post',
                url: frm.attr('action'),
                data: frm.serialize(),
                success: function (data) {
                    location.reload();

                    alert(data);
                    console.log('Submission was successful.');
                    console.log(data);
                },
                error: function (data) {
                    alert("Terjadi kesalahan, jika masih berlanjut hubungi system admin");
                    location.reload();
                    console.log('An error occurred.');
                    console.log(data);
                },
            });
        }
        function updatehapus(row)
        {
            $("#id_hapus").val(row);
        }
    </script>

    <!-- Modal End-->
    <div>
        <table style="font-size:12px;" class="table table-striped table-bordered data">
            <thead>
            <tr>
                <th>No</th>
                <th>Informasi</th>
                <th>Aksi</th>
            </tr>
            </thead>
            <tbody>
            <?php
            $no=1;
            foreach($list as $lists) {
                echo <<<HTML
            <tr>
                <td>$no</td>
                <td>$lists->isi_informasi</td>
                <td>
                    
                    <button type="button" class="btn btn-info btn-xs" onclick="sunting($lists->id_informasi)" data-toggle="modal" data-target="#sunting">Sunting</button>
                    <button type="button" class="btn btn-danger btn-xs" onclick="updatehapus($lists->id_informasi)" data-toggle="modal" data-target="#hapus">Hapus</button>
                </td>
            </tr>
HTML;
                $no++;
            } ?>
            </tbody>
        </table>
    </div>
