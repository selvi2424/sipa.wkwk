<?php

class Pengurus_Model extends CI_Model{
    public $table = 'pengurus';
    public $primary_key = 'id_pengurus';
    function __construct(){
        parent::__construct();
    }
    function getAllRecords(){
        return $this->db->get($this->table)->result();

    }
    function get_last_id()
    {
        $query =   $this->db->select('id_pengurus')
            ->from('pengurus')
           ->order_by("id_pengurus", "desc")
            ->get();
        return $query->row("id_pengurus")+1;
    }
    function login($username,$password)
    {
        $password = md5($password);
        return $this->db->select('id_pengurus')
            ->where('nama', $username)
            ->where('password', $password)
            ->get($this->table . ' AS u')
            ->result();
    }
    function insertPenguruss($data, $id_pengurus,$level){
        $result =  $this->db->insert($this->table,$data);
        $insert_id = $this->db->insert_id();
        $data2 = array(
            'username' => $id_pengurus,
            'password' => $data["password"],
            'level' => $level,
        );
        $result =  $this->db->insert('login',$data2);

        return $result; // Kode ini digunakan untuk mengembalikan hasil $res

    }
    function insertPengurus($data){
        $result =  $this->db->insert($this->table,$data);
        return $result; // Kode ini digunakan untuk mengembalikan hasil $result

    }
    public function get_by_id($id)
    {
        $query =   $this->db->select('*')
            ->from('pengurus')
            ->where('id_pengurus',$id)
            ->get();
        return $query->row();
    }

    function updatePengurus($pengurus_id,$data){
        $this->db->where('id_pengurus',$pengurus_id); // Kode ini digunakan untuk merubah record yang sudah ada dalam sebuah tabel
        $result = $this->db->update($this->table, $data); // Kode ini digunakan untuk merubah record yang sudah ada dalam sebuah tabel
        return $result;
    }
    public function deletePengurus($pengurus_id){
        $this->db->where('id_pengurus',$pengurus_id); // Kode ini digunakan untuk merubah record yang sudah ada dalam sebuah tabel
        $result = $this->db->delete($this->table); // Kode ini digunakan untuk menghapus record yang sudah ada
        return $result;
    }

}
?>