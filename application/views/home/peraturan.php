<section class="wrapper">
    <div class="row">
        <div style="left:170px;" class="col-lg-10">
            <h3 class="page-header"><i class="fa fa-file-text-o"></i> Formulir Pendaftaran </h3>
        </div>
    </div>
    <div class="container">
        <button style="position:relative;left:80px;"type="button" class="btn btn-info btn-md" data-toggle="modal" data-target="#myModal">Tata Tertib</button>
        <!-- Modal -->
        <div class="modal fade" id="myModal" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div style="left:-600px;" class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Pembayaran</h4>
                    </div>
                    <div  class="modal-body">
                        <p><center>Tata Tertib Santri</p></center>
                        1. Tata tertib santri putra<br>
                        Kewajiban<br>
                        1. Berakhlaqul Karimah dalam segala hal<br>
                        2. Menjaga nama baik pondok pesantren, baik di dalam maupun luar<br>
                        3. Disiplin dalam segala hal<br>
                        4. Mengikuti sholat maktubah berjamaah dan sholat sunah Rawatib (qobliyah dan ba'diyah)<br>
                        5. Mengkuti sholat-sholat sunah berjamaah yang telah ditetapkan<br>
                        6. Mengikuti wadlifah - wadlifah yang telah dituntun dan dibimbing oleh hadlrotus syaikh<br>
                        7. Mengikuti pengajian yang diasuh oleh Hadrotus Syaikh<br>
                        8. Mengikuti pengajian Al-Quran Al Karim atau kitab sesuai dengan tingkatan kemampuan<br>
                        9. Mengikuti sekolah, musyawarah dan dirosah idlofi <br>
                        10. Makan di kantin yang telah dikelola pondok peantran<br>
                        11. Memakai jubah putih dan kopyah putih ketika sholat dan sekolah<br>
                        12. Berpuasa pada hari senin dan kamis bagi yang sudah berumur lima belas tahun keatas<br>
                        13. Berpamitan dengan pengasuh dan/atau pengurus ketika akan meninggalkan pondok pesantren jika selesai masa study atau sebab lain<br>
                        14. Melaporkan kepada pengurus pondok atau petugas bila ada tamu<br>
                        15. Membayar uang makan, syahriyyah dan iuran lain yang telah ditentukan<br>
                        16. Menjaga 7K (Kesucian, Keamanan, Kebersihan, Ketertiban, Keindahan, Kekeluargaan, dan Kerindangan)<br>
                        17. Menabung atau menitipkan uang kepada pengurus<br>
                        Larangan<br>
                        1. Melanggar hukum syara'<br>
                        2. Mencuri<br>
                        3. Menggunakan hal-hal yang memabukkan<br>
                        4. Berhubungan atau berkenalan dengan perempuan yang bukan mahromnya<br>
                        5. Melakukan mutamarrid<br>
                        6. Ghoshob berupa apa saja<br>
                        7. Bertengkar dengan siapa saja<br>
                        8. Keluar masuk pondok pesantren tanpa izin pengurus<br>
                        9. Berkata jorok dan mengintip santri putri<br>
                        10. Melawan atau menentang penjaga atau pengurus yang sedang melaksanakan tugas<br>
                        11. Berambut panjang<br>
                        12. Menyemir rambut<br>
                        13. Begadang atau cangkruk setelah sholat malam (00.30)<br>
                        14. Menyimpan dan menggunakan <i>tape recorder</i>, <i>handphone</i>, dan alat atau media elektronika lainnya<br>
                        15. Menyimpan dan membawa senjata tajam, gambar porno, komik, novel, jenis obat-obatan terlarang<br>
                        16. Membeli atau titip makan di luar area pondok<br>
                        17. Tidak mengikuti kegiatan ubudiyah<br>
                        18. Kembali ke pondok melebihi batas izin yang sudah ditentukan (tanpa penjelasan)<br>
                        19. Mengganggu proses berlangsungnya kegiatan Pondok<br>
                        20. Membuang sampah di sembarang tempat <br>
                        21. Bermain atau menitipkan barang di rumah tetangga pondok<br>
                        22. Merokok bagi santri yang berumur dibawah 20 tahun <br>
                        23. Tindian<br>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Keluar</button>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <div class="row">
        <div style="left:170px;" class="col-md-10">
            <section class="panel">
                <div class="panel-body">
                    <form class="form-horizontal " method="get">
                        <h4><b><center>Data Identitas Santri</center></h4>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Nomor Induk</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Jenis Santri</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Nama Lengkap</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Nama Panggilan</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Jenis Kelamin</label>
                            <div >
                                <form action="">
                                    <input type="radio" checked name="jenkel" value="pria"> Pria <br>
                                    <input type="radio" name="jenkel" value="wanita"> Wanita <br>
                                </form>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Tempat</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Tanggal Lahir</label>
                            <div class="col-sm-10">
                                <input type="date" class="form-control">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Kewarganegaraan</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Anak Ke</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Jumlah Saudara</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Pendidikan Terakhir</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Lulus Tahun</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control">
                            </div>
                        </div>
                        <h4><b>Data Tempat Tinggal</h4>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Kabupaten</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Propinsi</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Nomor Telpon</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control">
                            </div>
                        </div>
                        <h4><b>Data Keluarga</h4>
                        <h5><b><u>Ayah</u></h5>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Nama</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Pendidikan Terakhir</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Pekerjaan</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control">
                            </div>
                        </div>
                        <h5><b><u>Ibu</u></b></h5>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Nama</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Pendidikan Terakhir</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Pekerjaan</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control">
                            </div>
                        </div>
                        <h5><b><u>Wali (Selain Orang Tua)</u></b></h5>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Nama</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Pendidikan Terakhir</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Pekerjaan</label>
                            <div class="col-sm-4">
                                <input type="text" class="form-control">
                            </div>
                        </div>
                        <h4>Data Kesehatan</h4>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Tinggi Badan</label>
                            <div class="col-sm-2">
                                <input type="number" min="50" max="200" class="form-control"><p>dalam centimeter
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Berat Badan</label>
                            <div class="col-sm-2">
                                <input type="number" min="5" max="100" class="form-control">dalam kiogram</p>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Golongan Darah</label>
                            <div class="col-sm-2">
                                <form action="">
                                    <input type="radio" checked name="jenkel" value="a"> A <br>
                                    <input type="radio" name="jenkel" value="b"> B <br>
                                    <input type="radio" name="jenkel"  value="ab"> AB <br>
                                    <input type="radio" name="jenkel" value="O"> O <br>
                                </form>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Penyakit yang Pernah Diderita</label>
                            <label class="col-sm-2 control-label">Jenis</label>
                            <div class="col-sm-4">
                                <input type="text" class="form-control">
                            </div>
                            <br><br><label class="col-sm-2 control-label">Lama</label>
                            <div class="col-sm-4">
                                <input type="text" class="form-control">
                            </div>
                            <br><br><label class="col-sm-4 control-label">Tahun</label>
                            <div class="col-sm-4">
                                <input type="text" class="form-control">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Penyandang Cacat</label>
                            <div class="col-sm-10">
                                <div class="col-sm-4">
                                    <input type="text" class="form-control">
                                </div>
                            </div>
                        </div>
                        <h4><b>Data Minat Bakat</u></b></h5>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Hobi</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Prestasi</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Mata Pelajaran Disenangi</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Cita-cita</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control">
                                </div>
                            </div>
                            <button type="button" style="left:100px;" class="btn btn-primary">Simpan</button>
                    </form>
                </div>
            </section>
        </div>
    </div>