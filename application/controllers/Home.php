<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {
    public function __construct()
    {
        parent::__construct();
        $this->load->model('informasi_model');
    }
    public function index()
	{
		$data=array(
			'title'=>'Home',
			'active_home'=>'active',
		);

		$this->load->view('element/header_login',$data);
		$this->load->view('element/menu_login');
        $data['list'] = $this->informasi_model->getAllRecords();
		$this->load->view('home/login',$data);
		$this->load->view('element/footer');
	}
}
