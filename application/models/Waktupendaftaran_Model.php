<?php

class Waktupendaftaran_Model extends CI_Model{
    public $primary_key = 'id_waktupendaftaran';
    public $table = 'waktupendaftaran';

    function __construct(){
        parent::__construct();
    }

    public function get_last_waktu()
    {
        $query = $this->db->select('id_waktu')
            ->from('waktupendaftaran')
            ->order_by("created_on", "desc")
            ->get();
        return $query->row()->id_waktu;
    }

    function getAllRecords(){
        return $this->db->get($this->table)->result();
    }

    function insertWaktuPendaftaran($data){
        $result =  $this->db->insert($this->table,$data);
        return $result; // Kode ini digunakan untuk mengembalikan hasil $res
    }

    public function get_by_id($id)
    {
        $this->db->from($this->table);
        $this->db->where('id_waktu', $id);
        $query = $this->db->get();

        return $query->row();
    }

    function updateWaktuPendaftaran($waktupendaftaran_id,$data){
        $this->db->where('id_waktu',$waktupendaftaran_id); // Kode ini digunakan untuk merubah record yang sudah ada dalam sebuah tabel
        $result = $this->db->update($this->table, $data); // Kode ini digunakan untuk merubah record yang sudah ada dalam sebuah tabel
        return $result;
    }

    public function deleteWaktuPendaftaran($waktupendaftaran_id){
        $this->db->where('id_waktu',$waktupendaftaran_id); // Kode ini digunakan untuk merubah record yang sudah ada dalam sebuah tabel

        $result = $this->db->delete($this->table); // Kode ini digunakan untuk menghapus record yang sudah ada
        return $result;

    }

}
?>
