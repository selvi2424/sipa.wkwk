
<div class="container" style="position:relative;left:90px;" class="col-md-5">
    <div class="container">
    </div><br />
    <br/>
    <br/>
    <h3 style="text-align: center;">Daftar Biaya</h3><hr>
    <!-- Modal Sunting Biaya-->
    <div class="modal fade" id="sunting" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div  class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Pengaturan Biaya Santri</h4>
                </div>
                <div class="modal-body" id="suntingform">
                </div>

                <div class="modal-footer">
                    <button type="button" onclick="simpanperubahan()" class="btn btn-info">Simpan</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
                </div>
            </div>
        </div>
    </div>
    <!--Modal End-->

    <!-- Modal Detail-->
    <div id="detail" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Detail Biaya</h4>
                </div>
                <div class="modal-body" id="detailform">
                    <div class="row">
                        <div class="col-md-12">
                            <section class="panel">
                                <div class="panel-body">
                                    <form class="form-horizontal " method="get">
                                        <table style="font-family:verdana; position:relative;top: -50px;">
                                            <form>
                                            </form>
                                        </table>
                                </div>
                            </section>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
                </div>
            </div>

        </div>
    </div>
    <!-- Modal End-->

    <script>
        function detail(id)
        {
            $.ajax({url: base_url+"ajax_pengurus/detailbiaya/"+id,
                success: function(result){
                    $('#detail').modal('show');
                    $("#detailform").html(result);

                }});
        }
        function simpanperubahan(){
            var frm = $('#ubahbiaya');
            $.ajax({
                type: frm.attr('method'),
                url: frm.attr('action'),
                data: frm.serialize(),
                success: function (data) {
                    alert(data);
                    location.reload();
                    console.log('Submission was successful.');
                    console.log(data);
                },
                error: function (data) {
                    alert("Terjadi kesalahan, jika masih berlanjut hubungi system admin");

                    console.log('An error occurred.');
                    console.log(data);
                },
            });
        }
        function sunting(id)
        {
            $.ajax({url: base_url+"ajax_pengurus/ubahbiaya/"+id,
                success: function(result){
                    $('#sunting').modal('show');

                    $("#suntingform").html(result);

                }});
        }

    </script>
    <div>
        <table style="font-size:12px;" class="table table-striped table-bordered data">
            <thead>
            <tr>
                <th>No</th>
                <th>Jenis Biaya</th>
                <th>Waktu Awal Berlaku</th>
                <th>Waktu Akhir Berlaku</th>
                <th>Aksi</th>
            </tr>
            </thead>
            <tbody>
            <?php $no=1; foreach($list as $lists) {
                echo <<<HTML
            <tr>
                <td>$no</td>
                <td>$lists->nama_biaya</td>
                <td>$lists->waktu_awalberlaku</td>
                <td>$lists->waktu_akhirberlaku</td>
                <td>
                   <button type="button" class="btn btn-info btn-xs" onclick="detail($lists->idbiaya)" data-target="#detail">Detail</button>
                   <button type="button" class="btn btn-success btn-xs" onclick="sunting($lists->idbiaya)"  data-toggle="modal" data-target="#sunting">Sunting</button>
                </td>
            </tr>
HTML;

                $no++;  } ?>
            </tbody>
        </table>
    </div>
    </body>
</div>