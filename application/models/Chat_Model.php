<?php

class Chat_Model extends CI_Model{
    public $table = 'chat';
    public $primary_key = 'id_chat';

    function __construct(){
        parent::__construct();
    }

    function getAllRecords(){
        return $this->db->get($this->table)->result();

    }


}
