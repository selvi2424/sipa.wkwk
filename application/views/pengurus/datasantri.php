    <div class="container" style="position:relative;left:90px;" class="col-md-5">
    <div class="container">
    </div><br /><br/><br/>
    <h3 style="text-align: center">Data Santri</h3><hr>
    <!-- Modal Detail-->
        <!-- Modal Detail-->
        <div class="modal fade" id="detail" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div   class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Data Identitas Santri</h4>
                    </div>
                    <div id="detailform"" class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <section class="panel">
                                <div class="panel-body">
                                    <form class="form-horizontal " method="get">
                                        <table style="font-family:verdana; position:relative;top: -50px;">
                                            <form>
                                            </form>
                                        </table>
                                </div>
                            </section>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
                </div>
            </div>

        </div>
    </div>
    <!--Modal End Detail-->
    <!--Modal End Detail-->
        <script>
            function detail(id)
            {
                $.ajax({url: base_url+"ajax_pengurus/detailsantri/"+id,
                    success: function(result){
                        $('#detail').modal('show');

                        $("#detailform").html(result);

                    }});
            }
        </script>
        <div >
        <table style="font-size:12px;" class="table table-striped table-bordered data">
            <thead>
            <tr>
                <th>No</th>
                <th>NIS</th>
                <th>Jenis Santri</th>
                <th>Nama</th>
                <th>Alamat</th>
                <th>Telepon</th>
                <th>Aksi</th>
            </tr>
            </thead>
            <tbody>
            <?php $no=1;foreach($list as $lists) {

echo <<<HTML
            <tr>
                <td>$no</td>
                <td>$lists->nis</td>
                <td>$lists->nama_biaya</td>
                <td>$lists->nama_lengkap</td>
                <td>$lists->alamat</td>
                <td>$lists->no_tlp</td>
                <td>
                    <button type="button" class="btn btn-info btn-xs" onclick="detail($lists->nis)" data-target="#detail">Detail</button>
                </td>
            </tr>
HTML;
$no++;
            } ?>
    </div>
    </body>
    <script type="text/javascript">
        $(document).ready(function(){
            $('.data').DataTable({columnDefs: [
                { orderable: false, targets: -1, }
            ]});

        });
    </script>
    </div>
