<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {
    public function __construct()
    {
        parent::__construct();
        $this->load->model("admin_model");
        $this->load->model("santri_model");
        $this->load->model("pengurus_model");
        if ($this->session->userdata('username')=="") {
            redirect('auth');
        }
        $this->load->helper('text');
        /*if ($this->session->userdata('username')=="") {
            $dataq=array(
                'title'=>'Home',
                'active_home'=>'active',
            );
            $this->load->view('element/header_login',$dataq);
            $this->load->view('element/footer');
        }*/
    }
    public function index()
	{
    $data=array(
      'title'=>'Kelola Santri',
			'active_cadmin_kelolaos'=>'active',
		);

        $this->load->view('element/header',$data);
		$this->load->view('element/menu_admin');
        $data['list'] = $this->santri_model->getAllRecords();
        $this->load->view('admin/kelolaos',$data);
		$this->load->view('element/footer');
	}

    public function kelolapengurus()
	{
    $data=array(
      'title'=>'Data pengurus',
			'active_cadmin_kelolapengurus'=>'active',
		);

		$this->load->view('element/header',$data);
		$this->load->view('element/menu_admin');
        $data['list'] = $this->pengurus_model->getAllRecords();
        $data['last_id'] = $this->pengurus_model->get_last_id();
		$this->load->view('admin/kelolapengurus',$data);
		$this->load->view('element/footer');
	}
}
