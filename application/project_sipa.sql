-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jul 27, 2017 at 02:28 AM
-- Server version: 10.1.9-MariaDB
-- PHP Version: 5.6.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `project_sipa`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id_admin` int(3) NOT NULL,
  `nama` varchar(25) NOT NULL,
  `password` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `biaya`
--

CREATE TABLE `biaya` (
  `idbiaya` int(4) NOT NULL,
  `nama_biaya` varchar(13) NOT NULL,
  `waktu_awalberlaku` datetime NOT NULL,
  `waktu_akhirberlaku` datetime NOT NULL,
  `uang_makan` int(7) NOT NULL,
  `syariah_pondok` int(7) NOT NULL,
  `khidmad_manaqib` int(7) NOT NULL,
  `syariah_tpq` int(7) DEFAULT '0',
  `syariah_diniyah` int(7) DEFAULT '0',
  `tabungan_haul` int(7) NOT NULL,
  `sumbanganInstitusi_1` int(7) NOT NULL,
  `sumbanganInstitusi_2` int(7) NOT NULL,
  `sumbanganInstitusi_3` int(7) NOT NULL,
  `biayapendaftaran` int(7) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `biaya`
--

INSERT INTO `biaya` (`idbiaya`, `nama_biaya`, `waktu_awalberlaku`, `waktu_akhirberlaku`, `uang_makan`, `syariah_pondok`, `khidmad_manaqib`, `syariah_tpq`, `syariah_diniyah`, `tabungan_haul`, `sumbanganInstitusi_1`, `sumbanganInstitusi_2`, `sumbanganInstitusi_3`, `biayapendaftaran`) VALUES
(1, 'menetap', '2017-07-06 08:28:00', '2017-07-21 11:00:00', 360000, 35000, 1000, 0, 15000, 25000, 167000, 167000, 167000, 60000),
(2, 'tidak menetap', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 0, 1000, 15000, 15000, 0, 167000, 167000, 167000, 25000);

-- --------------------------------------------------------

--
-- Table structure for table `chat`
--

CREATE TABLE `chat` (
  `id_chat` int(8) NOT NULL,
  `nis` int(8) NOT NULL,
  `id_pengurus` int(3) NOT NULL,
  `isi_pesan` text NOT NULL,
  `tanggal` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` tinyint(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `informasi`
--

CREATE TABLE `informasi` (
  `id_informasi` char(8) NOT NULL,
  `isi_informasi` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `informasi`
--

INSERT INTO `informasi` (`id_informasi`, `isi_informasi`) VALUES
('', 'dddd'),
('1', 'hghghg');

-- --------------------------------------------------------

--
-- Table structure for table `pembayaranbulanan`
--

CREATE TABLE `pembayaranbulanan` (
  `id_biayabulanan` int(3) NOT NULL,
  `nis` int(8) NOT NULL,
  `idbiaya` int(4) NOT NULL,
  `id_pengurus` int(3) NOT NULL,
  `bulan_bayar` varchar(15) NOT NULL,
  `tanggal` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  `konfirmasi` tinyint(1) NOT NULL DEFAULT '0',
  `donatur` int(9) NOT NULL DEFAULT '0',
  `bukti_pembayaran` varchar(25) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pembayaranbulanan`
--

INSERT INTO `pembayaranbulanan` (`id_biayabulanan`, `nis`, `idbiaya`, `id_pengurus`, `bulan_bayar`, `tanggal`, `konfirmasi`, `donatur`, `bukti_pembayaran`) VALUES
(1, 12001101, 1, 111, 'Januari', '2017-07-18 05:00:00', 0, 10000, '');

-- --------------------------------------------------------

--
-- Table structure for table `pembayaranpendaftaran`
--

CREATE TABLE `pembayaranpendaftaran` (
  `id_pembayaranpendaftaran` int(11) NOT NULL,
  `nis` int(8) NOT NULL,
  `idbiaya` int(4) NOT NULL,
  `tanggal` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pembayaranpendaftaran`
--

INSERT INTO `pembayaranpendaftaran` (`id_pembayaranpendaftaran`, `nis`, `idbiaya`, `tanggal`) VALUES
(1, 12001101, 1, '2017-07-04'),
(2, 50321, 2, '2017-07-05'),
(3, 47251, 1, '0000-00-00'),
(4, 83277, 1, '0000-00-00'),
(5, 77298, 2, '0000-00-00');

-- --------------------------------------------------------

--
-- Table structure for table `pengurus`
--

CREATE TABLE `pengurus` (
  `id_pengurus` int(3) NOT NULL,
  `nama` varchar(25) NOT NULL,
  `password` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pengurus`
--

INSERT INTO `pengurus` (`id_pengurus`, `nama`, `password`) VALUES
(111, 'Tino', '123');

-- --------------------------------------------------------

--
-- Table structure for table `santri`
--

CREATE TABLE `santri` (
  `nis` int(8) NOT NULL,
  `password` varchar(32) NOT NULL,
  `idbiaya` int(1) NOT NULL,
  `id_waktu` int(3) NOT NULL,
  `nama_lengkap` varchar(30) NOT NULL,
  `nama_panggilan` varchar(10) NOT NULL,
  `jenkel` varchar(6) NOT NULL,
  `tempat_lahir` varchar(20) NOT NULL,
  `tgl_lahir` date NOT NULL,
  `kewarganegaraan` varchar(20) NOT NULL,
  `anak_ke` varchar(2) NOT NULL,
  `jmlh_saudaraKandung` varchar(2) DEFAULT NULL,
  `jmlh_saudaraTiri` varchar(2) DEFAULT NULL,
  `jmlh_saudaraAngkat` varchar(2) DEFAULT NULL,
  `foto_santri` varchar(20) NOT NULL,
  `pendidikan_terakhir` varchar(10) NOT NULL,
  `tahun_lulus` varchar(4) NOT NULL,
  `alamat` varchar(50) NOT NULL,
  `propinsi` varchar(30) NOT NULL,
  `kabupaten` varchar(30) NOT NULL,
  `no_tlp` varchar(12) NOT NULL,
  `nama_ayah` varchar(25) NOT NULL,
  `pendidikan_ayah` varchar(25) NOT NULL,
  `pekerjaan_ayah` varchar(25) NOT NULL,
  `nama_ibu` varchar(25) NOT NULL,
  `pekerjaan_ibu` varchar(25) NOT NULL,
  `pendidikan_ibu` varchar(10) NOT NULL,
  `nama_wali` varchar(25) DEFAULT NULL,
  `pendidikan_wali` varchar(10) DEFAULT NULL,
  `pekerjaan_wali` varchar(25) DEFAULT NULL,
  `tinggi_badan` varchar(3) NOT NULL,
  `berat_badan` varchar(3) NOT NULL,
  `gol_darah` char(2) NOT NULL,
  `jenis_penyakit` varchar(255) DEFAULT NULL,
  `lama_sakit` varchar(2) DEFAULT NULL,
  `tahun_sakit` varchar(4) DEFAULT NULL,
  `cacat` varchar(255) DEFAULT NULL,
  `hobi` varchar(25) NOT NULL,
  `prestasi` varchar(255) DEFAULT NULL,
  `mapel` varchar(255) NOT NULL,
  `cita_cita` varchar(255) NOT NULL,
  `surat_pernyataan` varchar(15) NOT NULL,
  `tgl_penetapan` date NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `santri`
--

INSERT INTO `santri` (`nis`, `password`, `idbiaya`, `id_waktu`, `nama_lengkap`, `nama_panggilan`, `jenkel`, `tempat_lahir`, `tgl_lahir`, `kewarganegaraan`, `anak_ke`, `jmlh_saudaraKandung`, `jmlh_saudaraTiri`, `jmlh_saudaraAngkat`, `foto_santri`, `pendidikan_terakhir`, `tahun_lulus`, `alamat`, `propinsi`, `kabupaten`, `no_tlp`, `nama_ayah`, `pendidikan_ayah`, `pekerjaan_ayah`, `nama_ibu`, `pekerjaan_ibu`, `pendidikan_ibu`, `nama_wali`, `pendidikan_wali`, `pekerjaan_wali`, `tinggi_badan`, `berat_badan`, `gol_darah`, `jenis_penyakit`, `lama_sakit`, `tahun_sakit`, `cacat`, `hobi`, `prestasi`, `mapel`, `cita_cita`, `surat_pernyataan`, `tgl_penetapan`, `status`) VALUES
(19520, 'e7de9abd2abe6288bbbc928c62ae58ad', 2, 2, 'xxx', '', '', '', '0000-00-00', '', '', NULL, NULL, NULL, '', '', '', 'ssss', '', '', '088888', '', '', '', '', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, '', NULL, '', '', '', '0000-00-00', 0),
(21039, 'e7de9abd2abe6288bbbc928c62ae58ad', 1, 2, 'xxx', '', '', '', '0000-00-00', '', '', NULL, NULL, NULL, '', '', '', 'ssss', '', '', '088888', '', '', '', '', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, '', NULL, '', '', '', '0000-00-00', 0),
(47251, 'd41d8cd98f00b204e9800998ecf8427e', 1, 2, 'selvi', '', '', '', '0000-00-00', '', '', NULL, NULL, NULL, '', '', '', 'Jl Mawar', '', '', '09888888', '', '', '', '', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, '', NULL, '', '', '', '0000-00-00', 0),
(50321, 'e7de9abd2abe6288bbbc928c62ae58ad', 1, 2, 'xxx', '', '', '', '0000-00-00', '', '', NULL, NULL, NULL, '', '', '', 'ssss', '', '', '088888', '', '', '', '', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, '', NULL, '', '', '', '0000-00-00', 0),
(77298, 'd41d8cd98f00b204e9800998ecf8427e', 2, 2, 'kkkk', '', '', '', '0000-00-00', '', '', NULL, NULL, NULL, '', '', '', 'kkkk', '', '', '889999', '', '', '', '', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, '', NULL, '', '', '', '0000-00-00', 0),
(83277, 'd41d8cd98f00b204e9800998ecf8427e', 1, 2, 'selvi', '', '', '', '0000-00-00', '', '', NULL, NULL, NULL, '', '', '', 'jhjhj', '', '', '099999', '', '', '', '', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, '', NULL, '', '', '', '0000-00-00', 0),
(89795, 'e7de9abd2abe6288bbbc928c62ae58ad', 1, 2, 'xxx', '', '', '', '0000-00-00', '', '', NULL, NULL, NULL, '', '', '', 'ssss', '', '', '088888', '', '', '', '', '', '', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, '', NULL, '', '', '', '0000-00-00', 0),
(12001101, 'kjjiojoio', 1, 2, 'jjioooo', 'jnjkj', 'Pria', 'jjjk', '2017-07-05', 'jnjkjk', '1', '3', '-', '-', 'mmmm.jpg', 'SMP', '2012', 'LKKKKOO', 'KJK', 'JJNJ', '0988888888', 'njjjnn', 'jjkkk', 'jkjklkll', 'njkjkll', 'njjjj', 'jjjjjj', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '2017-07-18', 0);

-- --------------------------------------------------------

--
-- Table structure for table `sumbanganinstitusi`
--

CREATE TABLE `sumbanganinstitusi` (
  `id_sumbanganinstitusi` int(3) NOT NULL,
  `idbiaya` int(4) NOT NULL,
  `nis` int(8) NOT NULL,
  `bukti_sumbanganInstitusi1` varchar(30) NOT NULL,
  `bukti_sumbanganInstitusi2` varchar(30) NOT NULL,
  `bukti_sumbanganInstitusi3` varchar(30) NOT NULL,
  `konfirmasi1` int(1) DEFAULT '0',
  `konfirmasi2` int(1) DEFAULT '0',
  `konfirmasi3` int(1) DEFAULT '0',
  `id_pengurus1` int(3) NOT NULL,
  `id_pengurus2` int(3) NOT NULL,
  `id_pengurus3` int(3) NOT NULL,
  `tanggal1` datetime NOT NULL,
  `tanggal2` datetime NOT NULL,
  `tanggal3` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sumbanganinstitusi`
--

INSERT INTO `sumbanganinstitusi` (`id_sumbanganinstitusi`, `idbiaya`, `nis`, `bukti_sumbanganInstitusi1`, `bukti_sumbanganInstitusi2`, `bukti_sumbanganInstitusi3`, `konfirmasi1`, `konfirmasi2`, `konfirmasi3`, `id_pengurus1`, `id_pengurus2`, `id_pengurus3`, `tanggal1`, `tanggal2`, `tanggal3`) VALUES
(1, 2, 12001101, 'aaa.jpg', '', '', 1, 0, 0, 0, 0, 0, '2017-07-05 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `waktupendaftaran`
--

CREATE TABLE `waktupendaftaran` (
  `id_waktu` int(11) NOT NULL,
  `waktu_mulai` datetime NOT NULL,
  `waktu_akhir` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `waktupendaftaran`
--

INSERT INTO `waktupendaftaran` (`id_waktu`, `waktu_mulai`, `waktu_akhir`) VALUES
(1, '2017-06-01 00:00:00', '2017-06-10 00:00:00'),
(2, '2017-06-11 00:00:00', '2017-06-24 00:00:00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id_admin`);

--
-- Indexes for table `biaya`
--
ALTER TABLE `biaya`
  ADD PRIMARY KEY (`idbiaya`);

--
-- Indexes for table `chat`
--
ALTER TABLE `chat`
  ADD PRIMARY KEY (`id_chat`),
  ADD KEY `nis` (`nis`),
  ADD KEY `id_pengurus` (`id_pengurus`);

--
-- Indexes for table `informasi`
--
ALTER TABLE `informasi`
  ADD PRIMARY KEY (`id_informasi`);

--
-- Indexes for table `pembayaranbulanan`
--
ALTER TABLE `pembayaranbulanan`
  ADD PRIMARY KEY (`id_biayabulanan`),
  ADD KEY `nis` (`nis`),
  ADD KEY `idbiaya` (`idbiaya`),
  ADD KEY `id_pengurus` (`id_pengurus`);

--
-- Indexes for table `pembayaranpendaftaran`
--
ALTER TABLE `pembayaranpendaftaran`
  ADD PRIMARY KEY (`id_pembayaranpendaftaran`),
  ADD KEY `nis` (`nis`),
  ADD KEY `id_biaya` (`idbiaya`);

--
-- Indexes for table `pengurus`
--
ALTER TABLE `pengurus`
  ADD PRIMARY KEY (`id_pengurus`);

--
-- Indexes for table `santri`
--
ALTER TABLE `santri`
  ADD PRIMARY KEY (`nis`),
  ADD KEY `id_waktu` (`id_waktu`),
  ADD KEY `idbiaya` (`idbiaya`);

--
-- Indexes for table `sumbanganinstitusi`
--
ALTER TABLE `sumbanganinstitusi`
  ADD PRIMARY KEY (`id_sumbanganinstitusi`),
  ADD KEY `idbiaya` (`idbiaya`),
  ADD KEY `nis` (`nis`),
  ADD KEY `id_pengurus1` (`id_pengurus1`),
  ADD KEY `id_pengurus2` (`id_pengurus2`),
  ADD KEY `id_pengurus3` (`id_pengurus3`);

--
-- Indexes for table `waktupendaftaran`
--
ALTER TABLE `waktupendaftaran`
  ADD PRIMARY KEY (`id_waktu`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id_admin` int(3) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `biaya`
--
ALTER TABLE `biaya`
  MODIFY `idbiaya` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `chat`
--
ALTER TABLE `chat`
  MODIFY `id_chat` int(8) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `pembayaranbulanan`
--
ALTER TABLE `pembayaranbulanan`
  MODIFY `id_biayabulanan` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `pembayaranpendaftaran`
--
ALTER TABLE `pembayaranpendaftaran`
  MODIFY `id_pembayaranpendaftaran` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `pengurus`
--
ALTER TABLE `pengurus`
  MODIFY `id_pengurus` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=112;
--
-- AUTO_INCREMENT for table `sumbanganinstitusi`
--
ALTER TABLE `sumbanganinstitusi`
  MODIFY `id_sumbanganinstitusi` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `waktupendaftaran`
--
ALTER TABLE `waktupendaftaran`
  MODIFY `id_waktu` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `chat`
--
ALTER TABLE `chat`
  ADD CONSTRAINT `c_nis` FOREIGN KEY (`nis`) REFERENCES `santri` (`nis`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `pengurus` FOREIGN KEY (`id_pengurus`) REFERENCES `pengurus` (`id_pengurus`);

--
-- Constraints for table `pembayaranbulanan`
--
ALTER TABLE `pembayaranbulanan`
  ADD CONSTRAINT `biaya` FOREIGN KEY (`idbiaya`) REFERENCES `biaya` (`idbiaya`),
  ADD CONSTRAINT `c_pengurus` FOREIGN KEY (`id_pengurus`) REFERENCES `pengurus` (`id_pengurus`) ON UPDATE CASCADE,
  ADD CONSTRAINT `c_santri` FOREIGN KEY (`nis`) REFERENCES `santri` (`nis`) ON UPDATE CASCADE;

--
-- Constraints for table `pembayaranpendaftaran`
--
ALTER TABLE `pembayaranpendaftaran`
  ADD CONSTRAINT `c_nisDaftar` FOREIGN KEY (`nis`) REFERENCES `santri` (`nis`) ON UPDATE CASCADE,
  ADD CONSTRAINT `m` FOREIGN KEY (`idbiaya`) REFERENCES `biaya` (`idbiaya`) ON UPDATE CASCADE;

--
-- Constraints for table `santri`
--
ALTER TABLE `santri`
  ADD CONSTRAINT `c_biaya` FOREIGN KEY (`idbiaya`) REFERENCES `biaya` (`idbiaya`) ON UPDATE CASCADE,
  ADD CONSTRAINT `c_waktu` FOREIGN KEY (`id_waktu`) REFERENCES `waktupendaftaran` (`id_waktu`) ON UPDATE CASCADE;

--
-- Constraints for table `sumbanganinstitusi`
--
ALTER TABLE `sumbanganinstitusi`
  ADD CONSTRAINT `nis` FOREIGN KEY (`nis`) REFERENCES `santri` (`nis`),
  ADD CONSTRAINT `sumbanganinstitusi_ibfk_1` FOREIGN KEY (`idbiaya`) REFERENCES `biaya` (`idbiaya`) ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
