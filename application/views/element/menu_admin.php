<body>
<script src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>
<!-- container section start -->
<section id="container" class="">
    <header class="header dark-bg">
        <div class="toggle-nav">
            <div class="icon-reorder tooltips" data-original-title="Toggle Navigation" data-placement="bottom"><i class="icon_menu"></i></div>
        </div>
        <!--logo start-->
        <a class="logo">SISTEM PEMBAYARAN ADMINISTRASI <span class="lite"></span></a>
        <!--logo end-->
        <div class="top-nav notification-row">
            <!-- notificatoin dropdown start-->
            <ul class="nav pull-right top-menu">
                <li class="dropdown">
                    <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                              <span class="profile-ava">
                                  <img alt="" src="<?php echo base_url();?>assets/img/avatar1_small.jpg">
                              </span>
                        <span class="username">budi</span>
                        <b class="caret"></b>
                    </a>
                    <ul class="dropdown-menu extended logout">
                        <div class="log-arrow-up"></div>
                        <li>
                            <a href="" data-toggle="modal" data-target="#gantipassword"><i class="icon_user"></i> Sunting Password</a>
                        </li>
                        <div class="log-arrow-up"></div>
                        <li>
                            <a href="<?php echo base_url('auth/logout');?>"><i class="icon_key_alt"></i> Log Out</a>
                        </li>
                    </ul>
                    <ul class="dropdown-menu extended logout">

                    </ul>
                </li>
                <!-- user login dropdown end -->
            </ul>
            <!-- notificatoin dropdown end-->
        </div>
    </header>
    <!--header end-->

    <!-- Modal Tambah Pendaftaran -->
    <div class="modal fade" id="gantipassword" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Sunting Password</h4>
                </div>
                <div class="modal-body">
                    <form class="form-horizontal" role="form">
                        <div class="form-group" data-date-format="dd MM yyyy" >
                            <label class="col-sm-2 control-label"
                            >Password Lama</label>
                            <div class="col-sm-10">
                                <input type="password" class="form-control" name="password_lama"
                                       placeholder=""/>
                            </div>
                        </div>
                        <div class="form-group" >
                            <label class="col-sm-2 control-label"
                                   for="inputPassword3" >Password Baru</label>
                            <div class="col-sm-10">
                                <input type="password" class="form-control" name="password_baru"
                                       placeholder=""/>
                            </div>
                        </div>
                        <div class="form-group" >
                            <label class="col-sm-2 control-label"
                                   for="inputPassword3" >Konfirmasi Password</label>
                            <div class="col-sm-10">
                                <input type="password" class="form-control" name="konfirmasi_password"
                                       placeholder=""/>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-info">Simpan</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
                </div>
            </div>

        </div>
    </div>
    <!-- Modal End-->

    <!--sidebar start-->
    <aside>
        <div id="sidebar"  class="nav-collapse ">
            <!-- sidebar menu start-->
            <ul class="sidebar-menu">
                <li class="<?php if(isset($active_cadmin_kelolaos)){echo $active_cadmin_kelolaos;}?>">
                    <a href="<?php echo base_url('admin');?>">
                        <i class="icon_genius"></i>
                        <span style="font-size:14px;">Data<br>Santri</span>
                    </a>
                </li>
                <li class="<?php if(isset($active_cadmin_kelolapengurus)){echo $active_cadmin_kelolapengurus;}?>">
                    <a href="<?php echo base_url('admin/kelolapengurus');?>">
                        <i class="icon_genius"></i><span style="font-size:14px;">Data<br>Pengurus</span>
                    </a>
                </li>
            </ul>
            <!-- sidebar menu end-->
        </div>
    </aside>
    <!--sidebar end-->
