<link href="<?php echo base_url();?>assets/css/bar-chart.css" rel="stylesheet">
<!--main content start-->
  <section id="main-content">
      <section class="wrapper">
      <!--overview start-->
        <div class="row">
            <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">
                 <div class="info-box blue-bg">
                     <div style="font-size: 22px;text-align: center;">
                        Pengunjung Aktif
                     </div>
                     <div style="text-align: center;" class="count">100</div>
                  </div><!--/.info-box-->
            </div><!--/.col-->

                <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                  <div class="info-box brown-bg">
                      <div style="font-size: 21px;text-align: center;">Jumlah Pendaftar 2017</div>
                      <form>
                          <table>
                              <tr style="color:white;"class="">
                                <td>Santri Menetap</td>
                                <td>:</td>
                                <td>105</td>
                              </tr><br>
                              <tr style="color:white;"class="">
                                  <td>Santri Tidak Menetap</td>
                                  <td>:</td>
                                  <td>105</td>
                              </tr>
                          </table>
                      </form>
                  </div><!--/.info-box-->
                </div><!--/.col-->

                <div class="col-lg-5 col-md-3 col-sm-12 col-xs-12">
                    <div class="info-box green-bg">
                        <div style="font-size: 21px;text-align: center;">Waktu Pendaftaran</div><br class="">
                        <div style=" font-size: 25px;text-align: center;"><strong>00:22:30:45</strong></div>
                    </div><!--/.info-box-->
                </div><!--/.col-->
                <div class="col-lg-6 col-md-3 col-sm-12 col-xs-12">
                      <div class="info-box dark-bg">
                          <div style="font-size: 21px;text-align: center;">Sudah Konfirmasi</div>
                          <div>
                              <div style="font-size: 18px;">Sumbangan Institusi</div>
                              <form>
                                  <table>
                                      <tr style="color:white;"class="">
                                          <td>Lunas</td>
                                          <td>:</td>
                                          <td>105</td>
                                      </tr>
                                      <tr style="color:white;"class="">
                                          <td>Belum Lunas</td>
                                          <td>:</td>
                                          <td>105</td>
                                      </tr>
                                  </table>
                              </form>
                          </div>
                          <div>
                              <div style="font-size: 18px;">Pembayaran Bulanan (Januari)</div>
                              <form>
                                  <table>
                                      <tr style="color:white;"class="">
                                          <td>Lunas</td>
                                          <td>:</td>
                                          <td>105</td>
                                      </tr>
                                      <tr style="color:white;"class="">
                                          <td>Belum Lunas</td>
                                          <td>:</td>
                                          <td>105</td>
                                      </tr>
                                  </table>
                              </form>
                          </div>
                      </div><!--/.info-box-->
                  </div><!--/.info-box-->
                    <div class="col-lg-6 col-md-3 col-sm-12 col-xs-12">
                        <div class="info-box dark-bg">
                            <div style="font-size: 21px;text-align: center;">Belum Konfirmasi</div>
                            <div>
                                <div style="font-size: 18px;">Sumbangan Institusi</div>
                                <form>
                                    <table>
                                        <tr style="color:white;"class="">
                                            <td>Lunas</td>
                                            <td>:</td>
                                            <td>105</td>
                                        </tr>
                                        <tr style="color:white;"class="">
                                            <td>Belum Lunas</td>
                                            <td>:</td>
                                            <td>105</td>
                                        </tr>
                                    </table>
                                </form>
                            </div>
                            <div>
                                <div style="font-size: 18px;">Pembayaran Bulanan (Januari)</div>
                                <form>
                                    <table>
                                        <tr style="color:white;"class="">
                                            <td>Lunas</td>
                                            <td>:</td>
                                            <td>105</td>
                                        </tr>
                                        <tr style="color:white;"class="">
                                            <td>Belum Lunas</td>
                                            <td>:</td>
                                            <td>105</td>
                                        </tr>
                                    </table>
                                </form>
                            </div>
                        </div><!--/.info-box-->
                    </div><!--/.info-box-->
                <div class="col-lg-12" style="height:100px; display: inline-block;">
                    <div class="" style="height: 100px;">
                        <script type="text/javascript">

                            window.onload = function () {
                                var chart = new CanvasJS.Chart("chartContainer", {
                                    theme: "theme2",//theme1
                                    title:{
                                        text: "Rekapitulasi Data Pendaftar Santri"
                                    },
                                    animationEnabled: false,   // change to true
                                    data: [
                                        {
                                            // Change type to "bar", "area", "spline", "pie",etc.
                                            type: "column",
                                            dataPoints: [
                                                { label: "2017",  y: 10  },
                                                { label: "2018", y: 15  },
                                                { label: "2019", y: 25  },
                                                { label: "2020",  y: 30  },
                                                { label: "2021",  y: 28  }
                                            ]
                                        }
                                    ]
                                });
                                chart.render();
                            }
                        </script>
                        <div id="chartContainer" style="height: 300px; width: 100%;"></div>
                    </div>
                </div>
            </div><!--/.row-->
          </section>
      </section>
      <!--main content end-->
  </section>
</body>