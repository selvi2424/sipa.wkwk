<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ajax_admin extends CI_Controller {
    protected $base_url;
    public function __construct()
    {        parent::__construct();

        $this->load->model("santri_model");
        $this->load->model("sumbanganinstitusi_model");
        $this->load->model("pembayaranbulanan_model");
        $this->load->model("pengurus_model");
        $this->base_url = base_url();
    }
//Santri
    public function ubahsantri($id)
    {
        $data = $this->santri_model->get_by_id($id);
//print_r($data);
//die;
       /// $base_url = base_url();
        echo <<<HTML
         <div class="row">
            <div class="col-md-12">
                <section class="panel">
                    <div class="panel-body">
                        <form class="form-horizontal" action="$this->base_url/ajax_admin/simpanubahsantri" id="ubahsantri" method="post">
                            <table style="font-family:verdana; position:relative;top: -30px;">
                                    <tr>
                                        <td><h5><b>Data Pribadi<b></h5></td>
                                    </tr>
                                    <tr>
                                        <td><h5>Tanggal Penetapan Santri</h5></td>
                                        <td><h5>:</h5></td>
                                        <input class="form-control"type="hidden" name="tgl_penetapan" style="font-size: 13px;" value="$data->tgl_penetapan">
                                        <td style="font-size:13px;">$data->tgl_penetapan</td>
                                    </tr>
                                    <tr>
                                        <td><h5>NIS<h5></td>
                                        <td><h5>:</h5></td>
                                        <input class="form-control"type="hidden" name="nis" style="font-size: 13px;" value="$data->nis">
                                        <td style="font-size: 13px;">$data->nis</td>
                                    </tr>
                                    <tr>
                                        <td><h5>Jenis Santri</h5></td>
                                        <td><h5>:</h5></td>
                                        <td style="font-size: 13px;">$data->nama_biaya</td>
                                    </tr><br>
                                    <tr>
                                        <td><h5>Nama Lengkap</h5></td>
                                        <td><h5>:</h5></td>
                                        <td><input class="form-control" name="nama_lengkap" style="font-size: 13px;" value="$data->nama_lengkap" ></td>
                                    </tr>
                                    <tr>
                                        <td><h5>Nama Panggilan</h5></td>
                                        <td><h5>:</h5></td>
                                        <td><input class="form-control" name="nama_panggilan" style="font-size: 13px;" value="$data->nama_panggilan" ></td>
                                    </tr>
                                    <tr>
                                        <td><h5>Jenis Kelamin</h5></td>
                                        <td><h5>:</h5></td>
                                        <td><input class="form-control" name="jenkel" style="font-size: 13px;" value="$data->jenkel" >
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><h5>Tempat Lahir</h5></td>
                                        <td><h5>:</h5></td>
                                        <td><input class="form-control" name="tempat_lahir" style="font-size: 13px;" value="$data->tempat_lahir" ></td>
                                    </tr>
                                    <tr>
                                        <td><h5>Tanggal Lahir</h5></td>
                                        <td><h5>:</h5></td>
                                        <td><input class="form-control" name="tgl_lahir" style="font-size: 13px;" value="$data->tgl_lahir" ></td>
                                    </tr>
                                    <tr>
                                        <td><h5>Kewarganegaraan</h5></td>
                                        <td><h5>:</h5></td>
                                        <td><input class="form-control" name="kewarganegaraan" style="font-size: 13px;" value="$data->kewarganegaraan" ></td>
                                    </tr>
                                    <tr>
                                        <td><h5>Anak Ke-</h5></td>
                                        <td><h5>:</h5></td>
                                        <td><input class="form-control" name="anak_ke" style="font-size: 13px;" value="$data->anak_ke" ></td>
                                    </tr>
                                    <tr>
                                        <td><h5>Jumlah Saudara Kandung</h5></td>
                                        <td><h5>:</h5></td>
                                        <td><input class="form-control" name="jmlh_saudaraKandung" style="font-size: 13px;" value="$data->jmlh_saudaraKandung" ></td>
                                    </tr>
                                    <tr>
                                        <td><h5>Jumlah Saudara Tiri</h5></td>
                                        <td><h5>:</h5></td>
                                        <td><input class="form-control" name="jmlh_saudaraTiri" style="font-size: 13px;" value="$data->jmlh_saudaraTiri" ></td>
                                    </tr>
                                    <tr>
                                        <td><h5>Jumlah Saudara Angkat</h5></td>
                                        <td><h5>:</h5></td>
                                        <td><input class="form-control" name="jmlh_saudaraAngkat" style="font-size: 13px;" value="$data->jmlh_saudaraAngkat" ></td>
                                    </tr>
                                    <tr>
                                        <td><h5>Pendidikan Terakhir</h5></td>
                                        <td><h5>:</h5></td>
                                        <td><input class="form-control" name="pendidikan_terakhir" style="font-size: 13px;" value="$data->pendidikan_terakhir" ></td>
                                    </tr>
                                    <tr>
                                        <td><h5>Tahun Lulus</h5></td>
                                        <td><h5>:</h5></td>
                                        <td><input class="form-control" name="tahun_lulus" style="font-size: 13px;" value="$data->tahun_lulus" ></td>
                                    </tr>
                                    <tr>
                                        <td><h5><b>Data Tempat Tinggal<b></h5></td>
                                    </tr>
                                    <tr>
                                        <td><h5>Alamat</h5></td>
                                        <td><h5>:</h5></td>
                                        <td><input class="form-control" name="alamat" style="font-size: 13px;" value="$data->alamat" ></td>
                                    </tr>
                                    <tr>
                                        <td><h5>Kabupaten</h5></td>
                                        <td><h5>:</h5></td>
                                        <td><input class="form-control" name="kabupaten" style="font-size: 13px;" value="$data->kabupaten" ></td>
                                    </tr>
                                    <tr>
                                        <td><h5>Propinsi</h5></td>
                                        <td><h5>:</h5></td>
                                        <td><input class="form-control" name="propinsi" style="font-size: 13px;" value="$data->propinsi" ></td>
                                    </tr>
                                    <tr>
                                        <td><h5>Nomor Telepon</h5></td>
                                        <td><h5>:</h5></td>
                                        <td><input class="form-control" name="tlp" style="font-size: 13px;" value="$data->no_tlp" ></td>
                                    </tr>
                                    <tr>
                                        <td><h5><b> Data Keluarga<b></h5></td>
                                    </tr>
                                    <tr>
                                        <td><h5><b>ayah<b></h5></td>
                                    </tr>
                                    <tr>
                                        <td><h5>Nama</h5></td>
                                        <td><h5>:</h5></td>
                                        <td><input class="form-control" name="nama_ayah" style="font-size: 13px;" value="$data->nama_ayah" ></td>
                                    </tr>
                                    <tr>
                                        <td><h5>Pendidikan Terakhir</h5></td>
                                        <td><h5>:</h5></td>
                                        <td><input class="form-control" name="pendidikan_terakhir_ayah" style="font-size: 13px;" value="$data->pendidikan_ayah" ></td>
                                    </tr>
                                    <tr>
                                        <td><h5>Pekerjaan</h5></td>
                                        <td><h5>:</h5></td>
                                        <td><input class="form-control" name="pekerjaan_ayah" style="font-size: 13px;" value="$data->pekerjaan_ayah" ></td>
                                    </tr>
                                    <tr>
                                        <td><h5><b>Ibu<b></h5></td>
                                    </tr>
                                    <tr>
                                        <td><h5>Nama</h5></td>
                                        <td><h5>:</h5></td>
                                        <td><input class="form-control" name="nama_ibu" style="font-size: 13px;" value="$data->nama_ibu" ></td>
                                    </tr>
                                    <tr>
                                        <td><h5>Pendidikan Terakhir</h5></td>
                                        <td><h5>:</h5></td>
                                        <td><input class="form-control" name="pendidikan_terakhir_ibu" style="font-size: 13px;" value="$data->pendidikan_ibu" ></td>
                                    </tr>
                                    <tr>
                                        <td><h5>Pekerjaan</h5></td>
                                        <td><h5>:</h5></td>
                                        <td><input class="form-control" name="pekerjaan_ibu" style="font-size: 13px;" value="$data->pekerjaan_ibu" ></td>
                                    </tr>
                                    <tr>
                                        <td><h5><b>Wali(Selain Orang Tua)</b></h5></td>
                                    </tr>

                                    <tr>
                                        <td><h5>Nama</h5></td>
                                        <td><h5>:</h5></td>
                                        <td><input class="form-control" name="nama_wali" style="font-size: 13px;" value="$data->nama_wali " ></td>
                                    </tr>
                                    <tr>
                                        <td><h5>Pendidikan Terakhir</h5></td>
                                        <td><h5>:</h5></td>
                                        <td><input class="form-control" name="pendidikan_terakhir_wali" style="font-size: 13px;" value="$data->pendidikan_wali" ></td>
                                    </tr>
                                    <tr>
                                        <td><h5>Pekerjaan</h5></td>
                                        <td><h5>:</h5></td>
                                        <td><input class="form-control" name="pekerjaan_wali" style="font-size: 13px;" value="$data->pekerjaan_wali" ></td>
                                    </tr>
                                    <tr>
                                        <td><h5><b> Data Kesehatan<b></h5></td>
                                    </tr>
                                    <tr>
                                        <td><h5>Tinggi Badan</h5></td>
                                        <td><h5>:</h5></td>
                                        <td><input class="form-control" name="tinggi_badan" style="font-size: 13px;" value="$data->tinggi_badan" ></td>
                                    </tr>
                                    <tr>
                                        <td><h5>Berat Badan</h5></td>
                                        <td><h5>:</h5></td>
                                        <td><input class="form-control" name="berat_badan" style="font-size: 13px;" value="$data->berat_badan" ></td>
                                    </tr>
                                    <tr>
                                        <td><h5>Golongan Darah</h5></td>
                                        <td><h5>:</h5></td>
                                        <td><input class="form-control" name="berat_badan" style="font-size: 13px;" value="$data->gol_darah" ></td>
                                    </tr>
                                    <tr>
                                        <td><h5><b>Penyakit yang Pernah Diderita<b></h5></td>
                                    </tr>

                                    <tr>
                                        <td><h5>Jenis</h5></td>
                                        <td><h5>:</h5></td>
                                        <td><input class="form-control" name="jenis_penyakit" style="font-size: 13px;" value="$data->jenis_penyakit" ></td>
                                    </tr>
                                    <tr>
                                        <td><h5>Lama</h5></td>
                                        <td><h5>:</h5></td>
                                        <td><input class="form-control" name="lama_sakit" style="font-size: 13px;" value="$data->lama_sakit" ></td>
                                    </tr>
                                    <tr>
                                        <td><h5>Tahun</h5></td>
                                        <td><h5>:</h5></td>
                                        <td><input class="form-control" name="tahun_sakit" style="font-size: 13px;" value="$data->tahun_sakit" ></td>
                                    </tr>
                                    <tr>
                                        <td><h5>Penyandang Cacat</h5></td>
                                        <td><h5>:</h5></td>
                                        <td><input class="form-control" name="cacat" style="font-size: 13px;" value="$data->cacat" ></td>
                                    </tr>
                                    <tr>
                                        <td><h5><b>Data Minat Dan Bakat<b></h5></td>
                                    </tr>
                                    <tr>
                                        <td><h5>Hobi</h5></td>
                                        <td><h5>:</h5></td>
                                        <td><input class="form-control" name="hobi" style="font-size: 13px;" value="$data->hobi" ></td>
                                    </tr>
                                    <tr>
                                        <td><h5>Prestasi</h5></td>
                                        <td><h5>:</h5></td>
                                        <td><input class="form-control" name="prestasi" style="font-size: 13px;" value="$data->prestasi" ></td>
                                    </tr>
                                    <tr>
                                        <td><h5>Mata Pelajaran Disenangi</h5></td>
                                        <td><h5>:</h5></td>
                                        <td><input class="form-control" name="pelajaran_disenangi" style="font-size: 13px;" value="$data->mapel" ></td>
                                    </tr>
                                    <tr>
                                        <td><h5>Cita-cita</h5></td>
                                        <td><h5>:</h5></td>
                                        <td><input class="form-control" name="cita_cita" style="font-size: 13px;" value="$data->cita_cita" ></td>
                                    </tr>
                                  </table>
                                </form>
                      </div>
                </section>
            </div>   
        </div>
HTML;
    }
    public function simpanubahsantri()
    {
        $id =$this->input->post('nis');
        $data = array(
            'nama_lengkap' => $this->input->post('nama_lengkap'),
            'nama_panggilan' => $this->input->post('nama_lengkap'),
            'jenkel' => $this->input->post('jenkel'),
            'tempat_lahir' => $this->input->post('tempat_lahir'),
            'tgl_lahir' => $this->input->post('tgl_lahir'),
            'kewarganegaraan'=> $this->input->post('kewarganegaraan'),
            'anak_ke'=> $this->input->post('anak_ke'),
            'jmlh_saudaraKandung'=> $this->input->post('jmlh_saudaraKandung'),
            'jmlh_saudaraTiri'=> $this->input->post('jmlh_saudaraTiri'),
            'jmlh_saudaraAngkat'=> $this->input->post('jmlh_saudaraAngkat'),
            'foto_santri'=> $this->input->post('foto_santri'),
            'pendidikan_terakhir'=> $this->input->post('pendidikan_terakhir'),
            'tahun_lulus'=> $this->input->post('tahun_lulus'),
            'alamat'=> $this->input->post('alamat'),
            'propinsi'=> $this->input->post('propinsi'),
            'kabupaten'=> $this->input->post('kabupaten'),
            'no_tlp'=> $this->input->post('no_tlp'),
            'nama_ayah'=> $this->input->post('nama_ayah'),
            'pendidikan_ayah'=> $this->input->post('pendidikan_ayah'),
            'pekerjaan_ayah'=> $this->input->post('pekerjaan_ayah'),
            'nama_ibu'=> $this->input->post('nama_ibu'),
            'pendidikan_ibu'=> $this->input->post('pendidikan_ibu'),
            'pekerjaan_ibu'=> $this->input->post('pekerjaan_ibu'),
            'nama_wali'=> $this->input->post('nama_wali'),
            'pendidikan_wali'=> $this->input->post('pendidikan_wali'),
            'pekerjaan_wali'=> $this->input->post('pekerjaan_wali'),
            'tinggi_badan'=> $this->input->post('tinggi_badan'),
            'berat_badan'=> $this->input->post('berat_badan'),
            'gol_darah'=> $this->input->post('gol_darah'),
            'jenis_penyakit'=> $this->input->post('jenis_penyakit'),
            'lama_sakit'=> $this->input->post('lama_sakit'),
            'tahun_sakit'=> $this->input->post('tahun_sakit'),
            'cacat'=> $this->input->post('cacat'),
            'hobi'=> $this->input->post('hobi'),
            'prestasi'=> $this->input->post('prestasi'),
            'mapel'=> $this->input->post('mapel'),
            'cita_cita'=> $this->input->post('cita_cita'),
            'surat_pernyataan'=> $this->input->post('surat_pernyataan'),
            'tgl_penetapan'=> $this->input->post('tgl_penetapan'),
        );
        if(isset($data) && $this->santri_model->updateSantri($id,$data))
        {
            echo "sukses";
        }
        else {
            echo "gagal";
        }
    }
    public function detailsantri($id)
    {
        $data = $this->santri_model->get_by_id($id);
//print_r($data);
//die;

        echo <<<HTML
         <div class="row">
            <div class="col-md-12">
                <section class="panel">
                    <div class="panel-body">
                        <form class="form-horizontal" id="detailsantri" method="get">
                            <table style="font-family:verdana; position:relative;top: -30px;">
                                <tr>
                                    <td><h5><b>Data Pribadi<b></h5></td>
                                </tr>
                                <tr>
                                    <td><h5>Tanggal Penetapan Santri</h5></td>
                                    <td><h5>:</h5></td>
                                    <td><h5>$data->tgl_penetapan</h5></td>
                                </tr>
                                <tr>
                                    <td><h5>NIS<h5></td>
                                    <td><h5>:</h5></td>
                                    <td><h5>$data->nis</h5></td>
                                </tr>
                                <tr>
                                    <td><h5>Jenis Santri</h5></td>
                                    <td><h5>:</h5></td>
                                    <td><h5>$data->nama_biaya</h5></td>
                                </tr><br>
                                <tr>
                                    <td><h5>Nama Lengkap</h5></td>
                                    <td><h5>:</h5></td>
                                    <td><h5>$data->nama_lengkap</h5></td>
                                </tr>
                                <tr>
                                    <td><h5>Nama Panggilan</h5></td>
                                    <td><h5>:</h5></td>
                                    <td><h5>$data->nama_panggilan</h5></td>
                                </tr>
                                <tr>
                                    <td><h5>Jenis Kelamin</h5></td>
                                    <td><h5>:</h5></td>
                                    <td><h5>$data->jenkel</h5></td>
                                </tr>
                                <tr>
                                    <td><h5>Tempat Lahir</h5></td>
                                    <td><h5>:</h5></td>
                                    <td><h5>$data->tempat_lahir</h5></td>
                                </tr>
                                <tr>
                                    <td><h5>Tanggal Lahir</h5></td>
                                    <td><h5>:</h5></td>
                                    <td><h5>$data->tgl_lahir</h5></td>
                                </tr>
                                <tr>
                                    <td><h5>Kewarganegaraan</h5></td>
                                    <td><h5>:</h5></td>
                                    <td><h5>$data->kewarganegaraan</h5></td>
                                </tr>
                                <tr>
                                    <td><h5>Anak Ke-</h5></td>
                                    <td><h5>:</h5></td>
                                    <td><h5>$data->anak_ke</h5></td>
                                </tr>
                                <tr>
                                    <td><h5>Jumlah Saudara Kandung</h5></td>
                                    <td><h5>:</h5></td>
                                    <td><h5>$data->jmlh_saudaraKandung</h5></td>
                                </tr>
                                <tr>
                                    <td><h5>Jumlah Saudara Tiri</h5></td>
                                    <td><h5>:</h5></td>
                                    <td><h5>$data->jmlh_saudaraTiri</h5></td>
                                </tr>
                                <tr>
                                    <td><h5>Jumlah Saudara Angkat</h5></td>
                                    <td><h5>:</h5></td>
                                    <td><h5>$data->jmlh_saudaraAngkat</h5></td>
                                </tr>
                                <tr>
                                    <td><h5>Pendidikan Terakhir</h5></td>
                                    <td><h5>:</h5></td>
                                    <td><h5>$data->pendidikan_terakhir</h5></td>
                                </tr>
                                <tr>
                                    <td><h5>Tahun Lulus</h5></td>
                                    <td><h5>:</h5></td>
                                    <td><h5>$data->tahun_lulus</h5></td>
                                </tr>
                                <tr>
                                    <td><h5><b>Data Tempat Tinggal<b></h5></td>
                                </tr>
                                <tr>
                                    <td><h5>Alamat</h5></td>
                                    <td><h5>:</h5></td>
                                    <td><h5>$data->alamat</h5></td>
                                </tr>
                                <tr>
                                    <td><h5>Kabupaten</h5></td>
                                    <td><h5>:</h5></td>
                                    <td><h5>$data->kabupaten</h5></td>
                                </tr>
                                <tr>
                                    <td><h5>Propinsi</h5></td>
                                    <td><h5>:</h5></td>
                                    <td><h5>$data->propinsi</h5></td>
                                </tr>
                                <tr>
                                    <td><h5>Nomor Telepon</h5></td>
                                    <td><h5>:</h5></td>
                                    <td><h5>$data->no_tlp</h5></td>
                                </tr>
                                <tr>
                                    <td><h5><b> Data Keluarga<b></h5></td>
                                </tr>
                                <tr>
                                    <td><h5><b>ayah<b></h5></td>
                                </tr>
                                <tr>
                                    <td><h5>Nama</h5></td>
                                    <td><h5>:</h5></td>
                                    <td><h5>$data->nama_ayah</h5></td>
                                </tr>
                                <tr>
                                    <td><h5>Pendidikan Terakhir</h5></td>
                                    <td><h5>:</h5></td>
                                    <td><h5>$data->pendidikan_ayah</h5></td>
                                </tr>
                                <tr>
                                    <td><h5>Pekerjaan</h5></td>
                                    <td><h5>:</h5></td>
                                    <td><h5>$data->pekerjaan_ayah</h5></td>
                                </tr>
                                <tr>
                                    <td><h5><b>Ibu<b></h5></td>
                                </tr>
                                <tr>
                                    <td><h5>Nama</h5></td>
                                    <td><h5>:</h5></td>
                                    <td><h5>$data->nama_ibu</h5></td>
                                </tr>
                                <tr>
                                    <td><h5>Pendidikan Terakhir</h5></td>
                                    <td><h5>:</h5></td>
                                    <td><h5>$data->pendidikan_ibu</h5></td>
                                </tr>
                                <tr>
                                    <td><h5>Pekerjaan</h5></td>
                                    <td><h5>:</h5></td>
                                    <td><h5>$data->pekerjaan_ibu</h5></td>
                                </tr>
                                <tr>
                                    <td><h5><b>Wali(Selain Orang Tua)</b></h5></td>
                                </tr>

                                <tr>
                                    <td><h5>Nama</h5></td>
                                    <td><h5>:</h5></td>
                                    <td><h5>$data->nama_wali</h5></td>
                                </tr>
                                <tr>
                                    <td><h5>Pendidikan Terakhir</h5></td>
                                    <td><h5>:</h5></td>
                                    <td><h5>$data->pendidikan_wali</h5></td>
                                </tr>
                                <tr>
                                    <td><h5>Pekerjaan</h5></td>
                                    <td><h5>:</h5></td>
                                    <td><h5>$data->pekerjaan_wali</h5></td>
                                </tr>
                                <tr>
                                    <td><h5><b> Data Kesehatan<b></h5></td>
                                </tr>
                                <tr>
                                    <td><h5>Tinggi Badan(cm)</h5></td>
                                    <td><h5>:</h5></td>
                                    <td><h5>$data->tinggi_badan</h5></td>
                                </tr>
                                <tr>
                                    <td><h5>Berat Badan<kg></h5></td>
                                    <td><h5>:</h5></td>
                                    <td><h5>$data->berat_badan</h5></td>
                                </tr>
                                <tr>
                                    <td><h5><b>Penyakit yang Pernah Diderita<b></h5></td>
                                </tr>

                                <tr>
                                    <td><h5>Jenis</h5></td>
                                    <td><h5>:</h5></td>
                                    <td><h5>$data->jenis_penyakit</h5></td>
                                </tr>
                                <tr>
                                    <td><h5>Lama</h5></td>
                                    <td><h5>:</h5></td>
                                    <td><h5>$data->lama_sakit</h5></td>
                                </tr>
                                <tr>
                                    <td><h5>Tahun</h5></td>
                                    <td><h5>:</h5></td>
                                    <td><h5>$data->tahun_sakit</h5></td>
                                </tr>
                                <tr>
                                    <td><h5>Penyandang Cacat</h5></td>
                                    <td><h5>:</h5></td>
                                    <td><h5>$data->cacat</h5></td>
                                </tr>
                                <tr>
                                    <td><h5><b>Data Minat Dan Bakat<b></h5></td>
                                </tr>
                                <tr>
                                    <td><h5>Hobi</h5></td>
                                    <td><h5>:</h5></td>
                                    <td><h5>$data->hobi</h5></td>
                                </tr>
                                <tr>
                                    <td><h5>Prestasi</h5></td>
                                    <td><h5>:</h5></td>
                                    <td><h5>$data->prestasi</h5></td>
                                </tr>
                                <tr>
                                    <td><h5>Mata Pelajaran Disenangi</h5></td>
                                    <td><h5>:</h5></td>
                                    <td><h5>$data->mapel</h5></td>
                                </tr>
                                <tr>
                                    <td><h5>Cita-cita</h5></td>
                                    <td><h5>:</h5></td>
                                    <td><h5>$data->cita_cita</h5></td>
                                </tr>
                                  </table>
                                </form>
                      </div>
                </section>
            </div>   
        </div>
HTML;
    }
    public function gantipasswordsantri($id)
    {
        $data = $this->santri_model->get_by_id($id);
//print_r($data);
//die;

        echo <<<HTML
         <div class="row">
            <div class="col-md-12">
                <section class="panel">
                    <div class="panel-body">
                        <form class="form-horizontal" action="$this->base_url/ajax_admin/simpanubahpasswordsantri" id="ubahsantri" method="post">
                            <div class="form-group" data-date-format="dd MM yyyy" >
                            <label class="col-sm-2 control-label">Password Baru</label>
                            <div class="col-sm-10">
                                <input type="password" name="password_baru" class="form-control"
                                       placeholder="Password Baru"/>
                            </div>
                        </div>
                        <div class="form-group" >
                            <label class="col-sm-2 control-label"
                                   for="inputPassword3" >Konfirmasi Password</label>
                            <div class="col-sm-10">
                                <input type="password" name="confirm_password" class="form-control"
                                       placeholder="Konfirmasi Password"/>
                            </div>
                        </div>
                    </div>
                </section>
            </div  >   
        </div>
HTML;
    }
    public function hapussantri()
    {
        $id =$this->input->post('nis');


        if($this->santri_model->deletesantri($id))
        {
            echo "sukses";
        }
        else {
            echo "gagal";
        }
    }
//Pengurus
    public function ubahpengurus($id)
{
    $data = $this->pengurus_model->get_by_id($id);
//print_r($data);
//die;

    echo <<<HTML
         <div class="row">
            <div class="col-md-12">
                <section class="panel">
                    <div class="panel-body">
                        <form class="form-horizontal" action="$this->base_url/ajax_admin/simpanubahpengurus" id="ubahpengurus" method="post">
                            <table style="font-family:verdana; position:relative;top: -10px;">
                              
                                    <tr>
                                        <td style="font-size: 13px;">
                                        <input type="hidden" name="id_pengurus"  value="$data->id_pengurus" 
$data->id_pengurus</td>
                                    </tr>
                                    <tr>
                                        <td><h5>Nama Pengurus</h5></td>
                                        <td><h5>:</h5></td>
                                        <td><input class="form-control" name="nama" style="font-size: 13px;" value="$data->nama" ></td>
                                    </tr>
                                    <tr>
                                        <td><h5>Password baru*</h5></td>
                                        <td><h5>:</h5></td>
                                        <td><input class="form-control" type="password" name="password_baru" style="font-size: 13px;" ></td>
                                    </tr>
                                    <tr>
                                        <td><h5>Konfirmasi Password baru*</h5></td>
                                        <td><h5>:</h5></td>
                                        <td><input class="form-control" type="password" name="confirm_password" style="font-size: 13px;"  ></td>
                                    </tr>
                                    <tr>
                                </table>
                            </form>
                      <div>* kosongkan bila tidak ingin mengganti password</div>
                    </div>
                </section>
            </div  >   
        </div>
HTML;
    }
    public function simpanubahpengurus()
    {
        $id =$this->input->post('id_pengurus');
        if($this->input->post('password_baru') == "")
            $data = array(
                'nama' => $this->input->post('nama'),
            );
        else{
            if($this->input->post('password_baru') == $this->input->post('confirm_password'))
            {
                $data = array(
                    'nama' => $this->input->post('nama'),
                    'password' => md5($this->input->post('password_baru')),
                );
            }
            else{
                echo "invalid password";
            }
        }

        if(isset($data) && $this->pengurus_model->updatePengurus($id,$data))
        {
            echo "sukses";
        }
        else {
            echo "gagal";
        }
    }
    public function simpanpengurus()
    {
        $this->form_validation->set_rules('nama','Nama','required|xss');
        $this->form_validation->set_rules('password','Password','required');
        $this->form_validation->set_rules('confirm_password','Konfirmasi Password','required');
        if($this->input->post('password') == $this->input->post('confirm_password'))
        {
            $data = array(
                'nama' => $this->input->post('nama'),
                //'password' => md5($this->input->post('password')),
            );
        }
        else{
            echo "konfirmasi password salah";
        }


        if(isset($data) && $this->pengurus_model->insertPengurus($data))
        {
            echo "sukses";
        }
        else {
            echo "gagal";
        }
    }
    public function simpanpenguruss()
    {
        $this->form_validation->set_rules('nama','Nama','required|xss');
        $this->form_validation->set_rules('password','Password','required');
        $this->form_validation->set_rules('confirm_password','Konfirmasi Password','required');
        if($this->input->post('password') == $this->input->post('confirm_password'))
        {
        $data = array(
            'nama' => $this->input->post('nama'),
            'password' => md5($this->input->post('password')),
        );
        /*$data1= array(
            'level' => $this->input->post('nama'),
            'password' => md5($this->input->post('password')),
        );*/
        }
        else{
            echo "konfirmasi password salah";
        }
        if(isset($data) && $this->pengurus_model->insertPengurus($data))
        {
            echo "sukses";
        }
        else {
            echo "gagal";
        }
    }
    public function hapuspengurus()
{
    $id =$this->input->post('id_pengurus');


    if($this->pengurus_model->deletePengurus($id))
    {
        echo "sukses";
    }
    else {
        echo "gagal";
    }
    }
}
