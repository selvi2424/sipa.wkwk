<?php

class Biaya_Model extends CI_Model{
    public $table = 'biaya';
    public $primary_key = 'idbiaya';

    function __construct(){
        parent::__construct();
    }
    function getAllRecords(){
        return $this->db->get($this->table)->result();
    }
    function insertBiaya($data){
        $result =  $this->db->insert($this->table,$data);
        return $result; // Kode ini digunakan untuk mengembalikan hasil $res
    }
    public function get_by_id($id)
    {
        $query =   $this->db->select('*')
            ->from('biaya')
            ->where('idbiaya',$id)
            ->get();
        return $query->row();
    }
    function updateBiaya($biaya_id,$data){
        $this->db->where('idbiaya',$biaya_id); // Kode ini digunakan untuk merubah record yang sudah ada dalam sebuah tabel
        $result = $this->db->update($this->table, $data); // Kode ini digunakan untuk merubah record yang sudah ada dalam sebuah tabel
        return $result;
    }

}
