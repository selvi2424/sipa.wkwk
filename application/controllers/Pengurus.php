<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pengurus extends CI_Controller {
    public function __construct()
    {        parent::__construct();

        $this->load->model("pengurus_model");
        $this->load->model("santri_model");
        $this->load->model("sumbanganinstitusi_model");
        $this->load->model("pembayaranbulanan_model");
        $this->load->model("informasi_model");
        $this->load->model("pembayaranpendaftaran_model");
        $this->load->model("biaya_model");
        $this->load->model("waktupendaftaran_model");
        if ($this->session->userdata('username')=="") {
            redirect('home');
        }
        $this->load->helper('text');
    }
//Dashboard
    public function index()
	{
		$data=array(
			'title'=>'Dashboard',
			'active_cpengurus_dashboard'=>'active',
		);

		$this->load->view('element/header', $data);
		$this->load->view('element/menu_pengurus');
        $data['username'] = $this->session->userdata('username');
		$this->load->view('pengurus/dashboard', $data);
		$this->load->view('element/footer');
	}
//Data Santri
	public function datasantri()
	{
    $data=array(
      'title'=>'Data Santri',
			'active_cpengurus_datasantri'=>'active',

		);
        $this->load->view('element/header',$data);
		$this->load->view('element/menu_pengurus');
        $data['list'] = $this->santri_model->getAllRecords();
        $this->load->view('pengurus/datasantri',$data);
		$this->load->view('element/footer');
	}
    function tambah_datasantri(){
        $nama_lengkap = $this->input->post('nama_lengkap');
        $jenis_biaya = $this->input->post('jenis_biaya');
        $alamat = $this->input->post('alamat');
        $no_tlp= $this->input->post('no_tlp');
        $id_old_waktu=$this->waktupendaftaran_model->get_last_waktu();
        $tgl_bayar= $this->input->post('tgl_bayar');
        $password= $this->input->post('password');
        $id_old =$this->santri_model->get_last_santri();
        $id_old++;
        $tahun = substr($id_old,0,2); //mengambil angka jurusannya
        $jumlah_mhs = substr($id_old,-4); //mengambil angka jumlah mahasiswa

        if($tahun <= 16)
        {
            $jumlah_mhs="0001";
        }
        else{
        }
        $tahun = date("y"); //mengambil tahun dari sistem.

        $data = array(
            'nis' => $tahun.$jenis_biaya.$jumlah_mhs,
            'nama_lengkap' => $nama_lengkap,
            'idbiaya' => $jenis_biaya,
            'alamat' => $alamat,
            'no_tlp' => $no_tlp,
            //nanti diganti waktu sekarang dalam table waktu_pendaftarAN
            'id_waktu' => $id_old_waktu,
           // 'password' => md5($password),
        );
        if($this->santri_model->insertSantri($data,$tgl_bayar))
        {
            echo "sukses";
        }
        else{
            echo "gagal";
        }
    }

    function tambah_datasantris(){
        $nama_lengkap = $this->input->post('nama_lengkap');
        $level = $this->input->post('level');
        $jenis_biaya = $this->input->post('jenis_biaya');
        $alamat = $this->input->post('alamat');
        $no_tlp= $this->input->post('no_tlp');
        $id_old_waktu=$this->waktupendaftaran_model->get_last_waktu();
        $tgl_bayar= $this->input->post('tgl_bayar');
        $password= $this->input->post('password');
        $id_old =$this->santri_model->get_last_santri();
        $id_old++;
        $tahun = substr($id_old,0,2); //mengambil angka jurusannya
        $jumlah_mhs = substr($id_old,-4); //mengambil angka jumlah mahasiswa

        if($tahun <= 16)
        {
            $jumlah_mhs="0001";
        }
        else{
        }
        $tahun = date("y"); //mengambil tahun dari sistem.

        $data = array(
            'nis' => $tahun.$jenis_biaya.$jumlah_mhs,
            'nama_lengkap' => $nama_lengkap,
            'idbiaya' => $jenis_biaya,
            'alamat' => $alamat,
            'no_tlp' => $no_tlp,
            //nanti diganti waktu sekarang dalam table waktu_pendaftarAN
            'id_waktu' => $id_old_waktu,

        );
        if($this->santri_model->insertSantri($data,$tgl_bayar))
        {
         echo "sukses";
        }
        else{
            echo "gagal";
        }
    }
//Sumbangan Institusi
    public function sumbanganinstitusi()
    {
        $data=array(
            'title'=>'Sumbangan Institusi',
            'active_cpengurus_sumbanganinstitusi'=>'active',
        );
        $this->load->view('element/header',$data);
        $this->load->view('element/menu_pengurus');
        $data['list'] = $this->sumbanganinstitusi_model->getAllRecords();
        $this->load ->view('pengurus/sumbanganinstitusi', $data);
        $this->load->view('element/footer');
    }

//Informasi
	public function informasi()
	{
        $data=array(
      'title'=>'Informasi',
			'active_cpengurus_informasi'=>'active',
		);
		$this->load->view('element/header',$data);
		$this->load->view('element/menu_pengurus');
        $data['list'] = $this->informasi_model->getAllRecords();
		$this->load ->view('pengurus/informasi', $data);
		$this->load->view('element/footer');
	}

//Pembayaran Bulanan
	public function pembayaranbulanan()
	{
		$data=array(
			'title'=>'Pembayaran Bulanan',
			'active_cpengurus_pembayaranbulanan'=>'active',
		);
		$this->load->view('element/header',$data);
		$this->load->view('element/menu_pengurus');
        $data['list'] = $this->pembayaranbulanan_model->getAllRecords();
		$this->load->view('pengurus/pembayaranbulanan', $data);
		$this->load->view('element/footer');
	}

//Pembayaran Pendaftaran
	public function pembayaranpendaftaran()
	{
    $data=array(
      'title'=>'Pembayaran Pendaftaran',
			'active_cpengurus_pembayaranpendaftaran'=>'active',
		);
		$this->load->view('element/header',$data);
		$this->load->view('element/menu_pengurus');
        $data['list'] = $this->pembayaranpendaftaran_model->getAllRecords();
		$this->load->view('pengurus/pembayaranpendaftaran', $data);
		$this->load->view('element/footer');
	}

//Perubahan Biaya
	public function perubahanbiaya()
	{
		$data=array(
			'title'=>'Perubahan Biaya',
			'active_cpengurus_perubahanbiaya'=>'active',
		);
		$this->load->view('element/header',$data);
		$this->load->view('element/menu_pengurus');
        $data['list'] = $this->biaya_model->getAllRecords();
		$this->load->view('pengurus/perubahanbiaya', $data);
		$this->load->view('element/footer');
	}

//Perubahan Waktu Pendaftaran
	public function perubahanwaktu()
	{
    $data=array(
      'title'=>'Perubahan Waktu',
			'active_cpengurus_perubahanwaktu'=>'active',
		);
		$this->load->view('element/header',$data);
		$this->load->view('element/menu_pengurus');
        $data['list'] = $this->waktupendaftaran_model->getAllRecords();
		$this->load->view('pengurus/perubahanwaktu', $data);
		$this->load->view('element/footer');
	}

//Rekap Data
	public function rekapdata()
	{
    $data=array(
      'title'=>'Rekap Data',
			'active_cpengurus_rekapdata'=>'active',
		);
		$this->load->view('element/header',$data);
		$this->load->view('element/menu_pengurus');
		$this->load->view('pengurus/rekapdata');
		$this->load->view('element/footer');
	}

}
