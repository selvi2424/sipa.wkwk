<?php

class Pembayaranpendaftaran_Model extends CI_Model{
    public $table_pembayaranpendaftaran = 'pembayaranpendaftaran';
    public $primary_key = 'id_pembayaranpendaftaran';

    function __construct(){
        parent::__construct();
    }
    function getAllRecords(){
        $query =   $this->db->select('t1.*, t2.nama_lengkap as nama_lengkap, t3.nama_biaya as nama_biaya, t3.biayapendaftaran as biayapendaftaran, t1.tanggal as tanggal')
            ->from('pembayaranpendaftaran as t1')
            ->join('santri as t2', 't1.nis = t2.nis','left')
            ->join('biaya as t3', 't1.idbiaya = t3.idbiaya', 'left')
            ->get();
        return $query->result();
    }

    function insertPembayaranPendaftaran($data){
        $result =  $this->db->insert($this->table_pembayaranpendaftaran,$data);
        return $result; // Kode ini digunakan untuk mengembalikan hasil $res

    }
    public function get_by_id($id)
    {
        $query =   $this->db->select('t1.*, t2.nama_lengkap as nama_lengkap, , t2.alamat as alamat, t2.no_tlp as no_tlp, t3.nama_biaya as nama_biaya, t3.biayapendaftaran as biayapendaftaran')
            ->from('pembayaranpendaftaran as t1')
            ->join('santri as t2', 't1.nis = t2.nis','left')
            ->join('biaya as t3', 't1.idbiaya = t3.idbiaya', 'left')
            ->where('id_pembayaranpendaftaran',$id)
            ->get();
        return $query->row();
    }
    function updatePembayaranPendaftaran($pembayaranpendaftaran_id,$data){
        $this->db->where('id_pembayaranpendaftaran',$pembayaranpendaftaran_id); // Kode ini digunakan untuk merubah record yang sudah ada dalam sebuah tabel
        $result = $this->db->update($this->table_pembayaranpendaftaran, $data); // Kode ini digunakan untuk merubah record yang sudah ada dalam sebuah tabel
        return $result;
    }
    public function deletePembayaranPendaftaran( $pembayaranpendaftaran_id){
        $this->db->where('id_pembayaranpendaftaran',$pembayaranpendaftaran_id); // Kode ini digunakan untuk merubah record yang sudah ada dalam sebuah tabel

        $result = $this->db->delete($this->table_pembayaranpendaftaran); // Kode ini digunakan untuk menghapus record yang sudah ada
        return $result;

    }
}
