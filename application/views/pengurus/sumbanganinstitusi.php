<div class="container" style="position:relative;left:90px;" class="col-md-5">
    <div class="container">
    </div><br />
    <br/>
    <br/>
    <h3 style="text-align: center;">Data Sumbangan Institusi</h3><hr>
    <!-- Modal Detail -->
    <div class="modal fade" id="detail" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Data Sumbangan Institusi</h4>
                </div>
                <div class="modal-body" id="detailform">
                    <div class="row">
                        <div class="col-md-12">
                            <section class="panel">
                                <div class="panel-body">
                                    <form class="form-horizontal " method="get">
                                        <table style="font-family:verdana;">
                                            <form>

                                            </form>
                                        </table>
                                </div>
                            </section>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
                </div>
            </div>

        </div>
    </div>
    <!--Modal End -->

    <!--Modal Hapus-->
    <div class="modal fade" id="hapus" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h3 class="modal-title">Apakah anda yakin akan menghapus sumbangan institusi?</h3>
                </div>
                <div class="modal-body">
                    Jika anda menghapus data tidak dapat dipulihkan kembali
                </div>
                <form action="<?=base_url()?>/ajax_pengurus/hapussumbanganinstitusi" id="hapusform" method="post">
                    <input id="id_hapus" type="hidden" name="id_sumbanganinstitusi">
                </form>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" onclick="hapus()" data-dismiss="modal">Hapus</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
                </div>
            </div>

        </div>
    </div>
    <!--Modal End Hapus-->

    <script>
        function detail(id)
        {
            $.ajax({url: base_url+"ajax_pengurus/detailsumbanganinstitusi/"+id,
                success: function(result){
                    $('#detail').modal('show');

                    $("#detailform").html(result);

                }});
        }
        function hapus()
        {
            var frm = $('#hapusform');
            $.ajax({
                type: 'post',
                url: frm.attr('action'),
                data: frm.serialize(),
                success: function (data) {
                    location.reload();

                    alert(data);
                    console.log('Submission was successful.');
                    console.log(data);
                },
                error: function (data) {
                    alert("Terjadi kesalahan, jika masih berlanjut hubungi system admin");
                    location.reload();
                    console.log('An error occurred.');
                    console.log(data);
                },
            });
        }
        function updatehapus(row)
        {
            $("#id_hapus").val(row);
        }
    </script>
    //comment
    <div >
        <table style="font-size:12px;" class="table table-striped table-bordered data">
            <thead>
            <tr>
                <th>No</th>
                <th>NIS</th>
                <th>Nama</th>
                <th>Jenis Santri</th>
                <th>Cicilan Ke</th>
                <th>Jumlah Bayar</th>
                <th>Tanggal Bayar</th>
                <th>Aksi</th>
            </tr>
            </thead>
            <tbody>
            <?php $no=1; foreach($list as $lists) {
                echo <<<HTML
            <tr>
                <td>$no</td>
                <td>$lists->nis</td>
                <td>$lists->nama_lengkap</td>
                <td>$lists->nama_biaya</td>
                <td>$lists->SI_1</td>
                <td>$lists->SI_1</td>
                <td>$lists->tanggal1</td>
                <td>
                    <button type="button" class="btn btn-success btn-xs" data-target="#">Konfirmasi</button>
                    <button type="button" class="btn btn-info btn-xs" onclick="detail($lists->id_sumbanganinstitusi)" data-target="#detail">Detail</button>
                    <button type="button" class="btn btn-danger btn-xs" onclick="updatehapus($lists->id_sumbanganinstitusi)" data-toggle="modal" data-target="#hapus">Hapus</button>
                </td>
            </tr>
HTML;
                $no++;     } ?>
            </tbody>
        </table>
    </div>
    </body>
    <script type="text/javascript">
        $(document).ready(function(){
            $('.data').DataTable({columnDefs: [
                { orderable: false, targets: -1 }
            ]});

        });
    </script>
</div>