<?php

class Santri_Model extends CI_Model{
    public $table = 'santri';
    public $primary_key = 'nis';
    function __construct(){
        parent::__construct();
    }
//PENGURUS
    public function get_last_santri()
    {
        $query =   $this->db->select('nis')
            ->from('santri')
            ->order_by("created_on","desc")
            ->get();
        return $query->row()->nis;
    }
    public function getAllRecords(){
      $query =   $this->db->select('t2.nama_biaya as nama_biaya, t1.nis, t1.nama_lengkap, t1.alamat, t1.cita_cita, t1.tempat_lahir, t1.no_tlp')
            ->from('santri as t1')
            ->join('biaya as t2', 't1.idbiaya = t2.idbiaya', 'LEFT')
            ->get();
        return $query->result();
    }
    public function login($username,$password)
    {
        $password = md5($password);
        return $this->db->select('nis')
            ->where('nis', $username)
            ->where('password', $password)
            ->get($this->table . ' AS u')
            ->result();
    }

    public function insertSantri($data,$tgl_bayar){
        $result =  $this->db->insert($this->table,$data);
        $insert_id = $this->db->insert_id();
        $data2 = array(
            'nis' => $data["nis"],
            'idbiaya' => $data["idbiaya"],
            'tanggal' => $tgl_bayar,
        );
        $result =  $this->db->insert('pembayaranpendaftaran',$data2);

        return $result; // Kode ini digunakan untuk mengembalikan hasil $res
    }

    public function insertSantris($data,$tgl_bayar,$level){
        $result =  $this->db->insert($this->table,$data);
        $insert_id = $this->db->insert_id();
        $data1 = array(
            'username' => $data["nis"],
            'password' => md5($password),
            'level' => $level,
        );
        $result= $this->db->insert('login', $data1);
        $data2 = array(
            'nis' => $data["nis"],
            'idbiaya' => $data["idbiaya"],
            'tanggal' => $tgl_bayar,
        );
        $result =  $this->db->insert('pembayaranpendaftaran',$data2);

        return $result; // Kode ini digunakan untuk mengembalikan hasil $res

    }

    public function get_by_id($id)
    {
        $query =   $this->db->select('t2.nama_biaya as nama_biaya, t1.*')
            ->from('santri as t1')
            ->join('biaya as t2', 't1.idbiaya = t2.idbiaya', 'LEFT')
            ->where('nis',$id)
            ->get();
        return $query->row();
    }

    public function updateSantri($santri_id,$data){
        $this->db->where('nis',$santri_id); // Kode ini digunakan untuk merubah record yang sudah ada dalam sebuah tabel
        $result = $this->db->update($this->table, $data); // Kode ini digunakan untuk merubah record yang sudah ada dalam sebuah tabel
        return $result;
    }

    public function deleteSantri($santri_id){
      $this->db->where('nis',$santri_id); // Kode ini digunakan untuk merubah record yang sudah ada dalam sebuah tabel
        $result = $this->db->delete($this->table); // Kode ini digunakan untuk menghapus record yang sudah ada
        return $result;
    }
//Fungsi Untuk Dashboard
    public function jumlahSantriMenetap()
    {
        $jumlah_santri_menetap = $this->db->get_data("select count('nis') from santri where idbiaya = 1");
        return $jumlah_santri_menetap->result();
    }

    public function jumlahSantriTidakMenetap()
    {
        $jumlah_santri_tidakmenetap = $this->db->get_data("select count('nis') from santri where idbiaya = 2");
        return $jumlah_santri_tidakmenetap->result();
    }
//SANTRI
    public function getAllRecordByNis(){
        $user_id = $this->session->userdata('nis');
        $query =   $this->db->select('t2.nama_biaya as nama_biaya, t1.nis, t1.nama_lengkap, t1.alamat, t1.cita_cita, t1.tempat_lahir, t1.no_tlp')
            ->from('santri as t1')
            ->join('biaya as t2', 't1.idbiaya = t2.idbiaya', 'LEFT')
            ->where('t1.nis= "'.$user_id.'"')
            ->get();
        return $query->result();
    }

}
