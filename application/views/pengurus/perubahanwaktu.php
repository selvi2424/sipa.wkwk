<div class="container" style="position:relative;left:90px;" class="col-md-5">
    <div class="container">
    </div><br />
    <br/>
    <br/>
    <h3 style="text-align: center;">Daftar Perubahan Waktu Pendaftaran</h3><hr>
    <button type="button" class="btn btn-info btn-sm" data-toggle="modal" data-target="#perubahanwaktu">Tambah Waktu Pendaftaran</button><br><br>
    <!-- Modal Tambah Pendaftaran -->
    <div class="modal fade" id="perubahanwaktu" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Tambah Waktu Pendaftaran</h4>
                </div>
                <div class="modal-body">
                    <form id="simpanPerubahanWaktu" class="form-horizontal" role="form" action="<?php echo base_url(). 'ajax_pengurus/simpanwaktupendaftaran'; ?>" method="post">
                        <div class="form-group" data-date-format="dd MM yyyy" >
                            <label class="col-sm-2 control-label">Waktu Pembukaan</label>
                            <div class="col-sm-10">
                                <div class='input-group date' id='datetimepicker1'>
                                    <input type='text' class="form-control"  name="tgl_pembukaan"/>
                                    <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-calendar"></span></span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group" >
                            <label class="col-sm-2 control-label" for="inputPassword3" >Waktu Penutupan</label>
                            <div class="col-sm-10">
                                <div class='input-group date' id='datetimepicker2'>
                                    <input type='text' class="form-control"  name="tgl_penutupan"/>
                                    <span class="input-group-addon">
                                        <span class="glyphicon glyphicon-calendar"></span></span>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>

                <script type="text/javascript">
                    $(function () {
                        $('#datetimepicker1').datetimepicker();
                        $('#datetimepicker2').datetimepicker();
                    });
                </script>
                <div class="modal-footer">
                    <button type="button"  onclick="simpan();" class="btn btn-info">Simpan</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
                </div>
            </div>

        </div>
    </div>
    <!-- Modal End-->

    <!-- Modal Sunting-->

    <div class="modal fade" id="sunting" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Sunting Waktu Pendaftaran</h4>
                </div>
                <div class="modal-body" id="suntingform">
                </div>
                <div class="modal-footer">
                    <button type="button" onclick="simpanperubahan()"class="btn btn-info">Simpan</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
                </div>
            </div>

        </div>
    </div>
    <!-- Modal End-->

    <!--Modal Hapus-->
    <div class="modal fade" id="hapus" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h3 class="modal-title">Apakah anda yakin akan menghapus waktu pendaftaran?</h3>
                </div>
                <div class="modal-body">
                    Jika anda menghapus data tidak dapat dipulihkan kembali
                </div>
                <form action="<?=base_url()?>/ajax_pengurus/hapuswaktupendaftaran" id="hapusform" method="post">
                    <input id="id_hapus" type="hidden" name="id_waktu">
                </form>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" onclick="hapus()" data-dismiss="modal">Hapus</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
                </div>
            </div>

        </div>
    </div>
    <!--Modal Hapus End-->

    <script>
        function simpanperubahan(){
            var frm = $('#ubahwaktupendaftaran');
            $.ajax({
                type: frm.attr('method'),
                url: frm.attr('action'),
                data: frm.serialize(),
                success: function (data) {
                    alert(data);
                    location.reload();
                    console.log('Submission was successful.');
                    console.log(data);
                },
                error: function (data) {
                    alert("Terjadi kesalahan, jika masih berlanjut hubungi system admin");

                    console.log('An error occurred.');
                    console.log(data);
                },
            });
        }
        function sunting(id)
        {
            $.ajax({url: base_url+"ajax_pengurus/ubahwaktupendaftaran/"+id,
                success: function(result){
                    $('#sunting').modal('show');

                    $("#suntingform").html(result);

                }});
        }
        function simpan()
        {
            var frm = $('#simpanPerubahanWaktu');
            $.ajax({
                type: frm.attr('method'),
                url: frm.attr('action'),
                data: frm.serialize(),
                success: function (data) {
                    $('#perubahanwaktu').modal('hide');
                    alert(data);;
                    location.reload();
                    console.log('Submission was successful.');
                    console.log(data);
                },
                error: function (data) {
                    console.log('An error occurred.');
                    console.log(data);
                },
            });
        }

        function hapus()
        {
            var frm = $('#hapusform');
            $.ajax({
                type: 'post',
                url: frm.attr('action'),
                data: frm.serialize(),
                success: function (data) {
                    location.reload();

                    alert(data);
                    console.log('Submission was successful.');
                    console.log(data);
                },
                error: function (data) {
                    alert("Terjadi kesalahan, jika masih berlanjut hubungi system admin");
                    location.reload();
                    console.log('An error occurred.');
                    console.log(data);
                },
            });
        }
        function updatehapus(row)
        {
            $("#id_hapus").val(row);
        }
    </script>
    <div>
        <table style="font-size:12px;" class="table table-striped table-bordered data">
            <thead>
            <tr>
                <th>No</th>
                <th>Waktu Pembukaan</th>
                <th>Waktu Penutupan</th>
                <th>Aksi</th>
            </tr>
            </thead>
            <tbody>
            <?php $no=1;foreach($list as $lists) {

                echo <<<HTML
            <tr>
                <td>$no</td>
                <td>$lists->waktu_mulai</td>
                <td>$lists->waktu_akhir</td>
                <td>
                    <button type="button" class="btn btn-success btn-xs" onclick="sunting($lists->id_waktu)" data-toggle="modal" data-target="#sunting">Sunting</button>
                    <button type="button" class="btn btn-danger btn-xs" onclick="updatehapus($lists->id_waktu)" data-toggle="modal" data-target="#hapus">Hapus</button>
                </td>
            </tr>
HTML;
$no++;
            } ?>
            </tbody>
        </table>
    </div>
