<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller {
    public function __construct() {
        parent::__construct();

// Load form helper library
        $this->load->helper('form');

// Load form validation library
        $this->load->library('form_validation');

// Load session library
        $this->load->library('session');

// Load database
        $this->load->model('Users_Model', 'users');
    }
    // Check for user login process
    public function process()
{

    $this->form_validation->set_rules('id', 'Id', 'trim|required|xss_clean');
    $this->form_validation->set_rules('password', 'Password', 'trim|required|md5|xss_clean');
    $data = array('id' => $this->input->post('id', TRUE),
        'password' => md5($this->input->post('password', TRUE))
    );
    echo $data;
    if ($this->form_validation->run() == FALSE) {
        echo "<script>alert('Gagal login: Cek username, password!');</script>";
        header('location:'.base_url().'Home');
    } else {
        $hasil = $this->login->cek_user($data);
        if ($hasil->num_rows() == 1) {
            foreach ($hasil->result() as $sess) {
                $sess_data['logged_in'] = 'Sudah Loggin';
                $sess_data['id'] = $sess->id;
                $sess_data['jenis'] = $sess->jenis;
                $this->session->set_userdata($sess_data);
            }
            if ($this->session->userdata('jenis')=='admin') {
                header('location:'.base_url().'admin');
            }
            elseif ($this->session->userdata('jenis')=='pengurus') {
                header('location:'.base_url().'pengurus');
            }else{
                redirect('/admin/login');
            }
        }
        else {
            echo "<script>alert('Gagal login: Cek username, password!');history.go(-1);</script>";
        }
    }
}

    public function cek_login() {
        $data = array('username' => $this->input->post('username', TRUE),
            'password' => md5($this->input->post('password', TRUE))
        );
        $this->load->model('Users_Model'); // load model_user
        $hasil = $this->users->cek_user($data);
        if ($hasil->num_rows() == 1) {
            foreach ($hasil->result() as $sess) {
                $sess_data['logged_in'] = 'Sudah Loggin';
                $sess_data['uid'] = $sess->uid;
                $sess_data['username'] = $sess->username;
                $sess_data['level'] = $sess->level;
                $this->session->set_userdata($sess_data);
            }
            if ($this->session->userdata('level')=='admin') {
                redirect('admin');
            }
            elseif ($this->session->userdata('level')=='santri') {
                redirect('santri');
            }elseif ($this->session->userdata('level')=='pengurus') {
                redirect('pengurus');
            }
        }
        else {
            echo "<script>alert('Gagal login: Cek username, password!');history.go(-1);</script>";
        }
    }

    function login()
    {
        if(isset($_POST['submit'])){

            // proses login disini
            $username   =   $this->input->post('username');
            $password   =   $this->input->post('password');
            $hasil=  $this->model_operator->login($username,$password);
            if($hasil==1)
            {
                // update last login
                $this->db->where('username',$username);
                $this->db->update('operator',array('last_login'=>date('Y-m-d')));
                $this->session->set_userdata(array('status_login'=>'oke','username'=>$username));
                redirect('dashboard');
            }
            else{
                redirect('auth/login');
            }
        }
        else{
            //$this->load->view('form_login2');
            chek_session_login();
            $this->load->view('form_login');
        }
    }


    public function logout() {
        $this->session->unset_userdata('username');
        session_destroy();
        redirect('home');
    }
}

