<?php

class Pembayaranbulanan_Model extends CI_Model{
    public $primary_key = 'id_pembayaranbulanan';
    public $table_pembayaranbulanan = 'pembayaranbulanan';

    function __construct(){
        parent::__construct();
    }
//Pengurus
    function getAllRecords(){
        $query =   $this->db->select('t1.*, t2.nama_lengkap as nama_lengkap, t3.nama_biaya as nama_biaya, 
                                      t1.bulan_bayar as bulan_bayar, t1.tanggal as tanggal, t1.bukti_pembayaran as bukti_pembayaran')
            ->from('pembayaranbulanan as t1')
            ->join('santri as t2', 't1.nis = t2.nis','left')
            ->join('biaya as t3', 't1.idbiaya = t3.idbiaya', 'left')
            ->get();
        return $query->result();
    }

    function insertpembayaranbulanan($data){
        $result =  $this->db->insert($this->table_pembayaranbulanan,$data);
        return $result; // Kode ini digunakan untuk mengembalikan hasil $res

    }

    public function get_by_id($id)
    {
        $query =   $this->db->select('t1.*, t2.nama_lengkap as nama_lengkap, t3.*')
            ->from('pembayaranbulanan as t1')
            ->join('santri as t2', 't1.nis = t2.nis','left')
            ->join('biaya as t3', 't1.idbiaya = t3.idbiaya', 'left')
            ->where('id_biayabulanan',$id)
            ->get();
        return $query->row();

    }

    function update_pembayaranbulanan($pembayaranbulanan_id,$data){
        $this->db->where('id_pembayaranbulanan',$pembayaranbulanan_id); // Kode ini digunakan untuk merubah record yang sudah ada dalam sebuah tabel
        $result = $this->db->update($this->table_pembayaranbulanan, $data); // Kode ini digunakan untuk merubah record yang sudah ada dalam sebuah tabel
        return $result;
    }

    public function deletePembayaranBulanan( $pembayaranbulanan_id){
        $this->db->where('id_biayabulanan',$pembayaranbulanan_id); // Kode ini digunakan untuk merubah record yang sudah ada dalam sebuah tabel
        $result = $this->db->delete($this->table_pembayaranbulanan); // Kode ini digunakan untuk menghapus record yang sudah ada
        return $result;

    }

    /*public function jumlahSumbanganInstitusi(){
        $query = $this->db->query("SELECT t1.*, t2.id_waktu as id_waktu, count(nis) AS nis FROM santri as t1, waktupendaftaran as t2 where t1.id_waktu=t2.id_waktu");
        // print_r($query->result());
        return $query->result();
    }*/

    /* function jumlahSumbanganInstitusim(){
            $this->db->select('SalerName, count(*)');
            $this->db->from('tblSaler');
            $this->db->join('tblProduct', 'tblSaler.SalerID = tblProduct.SalerID');
            $this->db->group_by('tblSaler.SalerID');
            $query = $this->db->get();
        }*/
//SANTRI
    function getAllRecordsByNis($nis){

        $query =   $this->db->select('t1.*, t2.nama_lengkap as nama_lengkap, t3.nama_biaya as nama_biaya, 
                                      t1.bulan_bayar as bulan_bayar, t1.tanggal as tanggal, t1.bukti_pembayaran as bukti_pembayaran')
            ->from('pembayaranbulanan as t1')
            ->join('santri as t2', 't1.nis = t2.nis','left')
            ->join('biaya as t3', 't1.idbiaya = t3.idbiaya', 'left')
            ->where('t1.nis= "'.$user_id.'"')
            ->get();
        return $query->result();
    }


}
