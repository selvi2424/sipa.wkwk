  <section class="wrapper" style="left: 40px;position: relative">
    <div class="row">

      <!-- Modal Tata Tertib-->
      <div class="modal fade" id="tataTertib" role="dialog">
        <div class="modal-dialog">     
          <!-- Modal content-->
          <div  class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              <h4 class="modal-title">Tata Tertib Santri</h4>
            </div>
            <div  class="modal-body">
                 1. Tata tertib santri putra<br>
                 Kewajiban<br>
                 1. Berakhlaqul Karimah dalam segala hal<br>
                 2. Menjaga nama baik pondok pesantren, baik di dalam maupun luar<br>
                 3. Disiplin dalam segala hal<br>
                 4. Mengikuti sholat maktubah berjamaah dan sholat sunah Rawatib (qobliyah dan ba'diyah)<br>
                 5. Mengkuti sholat-sholat sunah berjamaah yang telah ditetapkan<br>
                 6. Mengikuti wadlifah - wadlifah yang telah dituntun dan dibimbing oleh hadlrotus syaikh<br>
                 7. Mengikuti pengajian yang diasuh oleh Hadrotus Syaikh<br>
                 8. Mengikuti pengajian Al-Quran Al Karim atau kitab sesuai dengan tingkatan kemampuan<br>
                 9. Mengikuti sekolah, musyawarah dan dirosah idlofi <br>
                 10. Makan di kantin yang telah dikelola pondok peantran<br>
                 11. Memakai jubah putih dan kopyah putih ketika sholat dan sekolah<br>
                 12. Berpuasa pada hari senin dan kamis bagi yang sudah berumur lima belas tahun keatas<br>
                 13. Berpamitan dengan pengasuh dan/atau pengurus ketika akan meninggalkan pondok pesantren jika selesai masa study atau sebab lain<br>
                 14. Melaporkan kepada pengurus pondok atau petugas bila ada tamu<br>
                 15. Membayar uang makan, syahriyyah dan iuran lain yang telah ditentukan<br>
                 16. Menjaga 7K (Kesucian, Keamanan, Kebersihan, Ketertiban, Keindahan, Kekeluargaan, dan Kerindangan)<br>
                 17. Menabung atau menitipkan uang kepada pengurus<br>
                 Larangan<br>
                 1. Melanggar hukum syara'<br>
                 2. Mencuri<br>
                 3. Menggunakan hal-hal yang memabukkan<br>
                 4. Berhubungan atau berkenalan dengan perempuan yang bukan mahromnya<br>
                 5. Melakukan mutamarrid<br>
                 6. Ghoshob berupa apa saja<br>
                 7. Bertengkar dengan siapa saja<br>
                 8. Keluar masuk pondok pesantren tanpa izin pengurus<br>
                 9. Berkata jorok dan mengintip santri putri<br>
                 10. Melawan atau menentang penjaga atau pengurus yang sedang melaksanakan tugas<br>
                 11. Berambut panjang<br>
                 12. Menyemir rambut<br>
                 13. Begadang atau cangkruk setelah sholat malam (00.30)<br>
                 14. Menyimpan dan menggunakan <i>tape recorder</i>, <i>handphone</i>, dan alat atau media elektronika lainnya<br>
                 15. Menyimpan dan membawa senjata tajam, gambar porno, komik, novel, jenis obat-obatan terlarang<br>
                 16. Membeli atau titip makan di luar area pondok<br>
                 17. Tidak mengikuti kegiatan ubudiyah<br>
                 18. Kembali ke pondok melebihi batas izin yang sudah ditentukan (tanpa penjelasan)<br>
                 19. Mengganggu proses berlangsungnya kegiatan Pondok<br>
                 20. Membuang sampah di sembarang tempat <br>
                 21. Bermain atau menitipkan barang di rumah tetangga pondok<br>
                 22. Merokok bagi santri yang berumur dibawah 20 tahun <br>
                 23. Tindian<br>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Keluar</button>
            </div>
          </div>
          
        </div>
      </div>
        <!--Modal Tata Tertib End-->

        <button style="position:relative;left:200px;"type="button" class="btn btn-info btn-xs" data-toggle="modal" data-target="#tataTertib">Tata Tertib</button>

        <h3 style="text-align: center;"><b><strong>Data Identitas Santri</strong><b></h3>
        <form id="suntingform" action="<?php echo base_url() . "santri/updatesantri"?>"
              enctype="multipart/form-data" class="form-horizontal " style="position: relative;top: -50px;left:200px;" method="post">
            <table style="top: -20px;font-family:verdana;">
                <div class="row">

                    <div class="col-sm-5" style="background-color:lavenderblush;">
                        <tr>
                            <td><strong>Data Pribadi</strong></td>
                        </tr>
                        <tr>
                            <td><h5>NIS<h5></td>
                            <td><h5>:</h5></td>
                            <td><h5>$data->$nis</h5></td>
                            <input type="hidden" name="nis" id="nis">
                        </tr>
                        <tr>
                            <td><h5>Jenis Santri<h5></td>
                            <td><h5>:</h5></td>
                            <td><h5>Menetap</h5></td>
                            <input type="hidden" name="nama_biaya" id="nama_biaya">
                        </tr>
                        <tr>
                            <td><h5>Unggah Foto (format .jpg/.jpeg, max 2MB)</h5></td>
                            <td><h5>:</h5></td>
                            <td><input type="file" name="foto_santri" id="foto_santri"></td>
                        </tr><br>
                        <tr>
                            <td><h5>Nama Lengkap</h5></td>
                            <td><h5>:</h5></td>
                            <td><input type="text" name="nama_lengkap" class=""></td>
                        </tr><br>
                        <tr>
                            <td><h5>Nama Panggilan</h5></td>
                            <td><h5>:</h5></td>
                            <td><input type="text" name="nama_panggilan" class="">
                            </td>
                        </tr>
                        <tr>
                            <td><h5>Jenis Kelamin</h5></td>
                            <td><h5>:</h5></td>
                            <td><input type="radio" checked name="jenkel" value="pria"> Pria <br>
                                    <input type="radio" name="jenkel" value="wanita"> Wanita <br>
                            </td>
                        </tr><br>
                        <tr>
                            <td><h5>Tempat Lahir</h5></td>
                            <td><h5>:</h5></td>
                            <td><input type="text" name="tempat_lahir" class="">
                            </td>
                        </tr>
                        <tr>
                            <td><h5>Tanggal Lahir</h5></td>
                            <td><h5>:</h5></td>
                            <td><input type="text" name="tgl_lahir" class="">
                            </td>
                        </tr>
                        <tr>
                            <td><h5>Kewarganegaraan</h5></td>
                            <td><h5>:</h5></td>
                            <td><input type="text" name="kewarganegaraan" class="">
                            </td>
                        </tr>
                        <tr>
                            <td><h5>Anak Ke</h5></td>
                            <td><h5>:</h5></td>
                            <td><input type="text" name="anak_ke" class="">
                            </td>
                        </tr>
                        <tr>
                            <td><h5>Jumlah Saudara Kandung</h5></td>
                            <td><h5>:</h5></td>
                            <td><input type="text" name="saudara_kandung" class="">
                            </td>
                        </tr>
                        <tr>
                            <td><h5>Jumlah Saudara Tiri</h5></td>
                            <td><h5>:</h5></td>
                            <td><input type="text" name="saudara_tiri" class="">
                            </td>
                        </tr>
                        <tr>
                            <td><h5>Jumlah Saudara Angkat</h5></td>
                            <td><h5>:</h5></td>
                            <td><input type="text" name="saudara_angkat" class="">
                            </td>
                        </tr>
                        <tr>
                            <td><h5>Pendidikan Terakhir</h5></td>
                            <td><h5>:</h5></td>
                            <td><input type="text" name="pendidikan_terakhir" class="">
                            </td>
                        </tr>
                        <tr>
                            <td><h5>Tahun Lulus</h5></td>
                            <td><h5>:</h5></td>
                            <td><input type="text" name="tahun_lulus" class="">
                            </td>
                        </tr>
                        <tr>
                            <td><h5>Nomor Telepon</h5></td>
                            <td><h5>:</h5></td>
                            <td><input type="text" name="no_tlp" class="">
                            </td>
                        </tr>
                        <tr>
                            <td><h5><b>Data Tempat Tinggal<b></h5></td>
                        </tr>
                        <tr>
                            <td><h5>Alamat</h5></td>
                            <td><h5>:</h5></td>
                            <td><textarea type="text" name="alamat" class=""></textarea>
                            </td>
                        </tr>
                        <tr>
                            <td><h5>Kabupaten</h5></td>
                            <td><h5>:</h5></td>
                            <td><input type="text" name="kabupaten" class="">
                            </td>
                        </tr>
                        <tr>
                            <td><h5>Propinsi</h5></td>
                            <td><h5>:</h5></td>
                            <td><input type="text" name="propinsi" class="">
                            </td>
                        </tr>
                        <tr>
                            <td><h5><b>Data Keluarga<b></h5></td>
                        </tr>
                        <tr>
                            <td><h5><u>Ayah</u></h5></td>
                        </tr>
                        <tr>
                            <td><h5>Nama Ayah</h5></td>
                            <td><h5>:</h5></td>
                            <td><input type="text" name="nama_ayah" class="">
                            </td>
                        </tr>
                        <tr>
                            <td><h5>Pendidikan Ayah</h5></td>
                            <td><h5>:</h5></td>
                            <td><input type="text" name="pendidikan_ayah" class="">
                            </td>
                        </tr>
                        <tr>
                            <td><h5>Pekerjaan Ayah</h5></td>
                            <td><h5>:</h5></td>
                            <td><input type="text" name="pekerjaan_ayah" class="">
                            </td>
                        </tr>
                        <tr>
                            <td><h5><u>Ibu</u></h5></td>
                        </tr>
                        <tr>
                            <td><h5>Nama Ibu</h5></td>
                            <td><h5>:</h5></td>
                            <td><input type="text" name="nama_ibu" class="">
                            </td>
                        </tr>
                        <tr>
                            <td><h5>Pendidikan Ibu</h5></td>
                            <td><h5>:</h5></td>
                            <td><input type="text" name="pendidikan_ibu" class="">
                            </td>
                        </tr>
                        <tr>
                            <td><h5>Pekerjaan Ibu</h5></td>
                            <td><h5>:</h5></td>
                            <td><input type="text" name="pekerjaan_ibu" class="">
                            </td>
                        </tr>
                        <tr>
                            <td><h5><u>Wali</u></h5></td>
                        </tr>
                        <tr>
                            <td><h5>Nama Wali</h5></td>
                            <td><h5>:</h5></td>
                            <td><input type="text" name="nama_wali" class="">
                            </td>
                        </tr>
                        <tr>
                            <td><h5>Pendidikan Wali</h5></td>
                            <td><h5>:</h5></td>
                            <td><input type="text" name="pendidikan_wali" class="">
                            </td>
                        </tr>
                        <tr>
                            <td><h5>Pekerjaan Wali</h5></td>
                            <td><h5>:</h5></td>
                            <td><input type="text" name="pekerjaan_wali" class="">
                            </td>
                        </tr>
                        <tr>
                            <td><h5><b>Data Kesehatan</b></h5></td>
                        </tr>
                        <tr>
                            <td><h5>Tinggi Badan (cm)</h5></td>
                            <td><h5>:</h5></td>
                            <td><input type="number" min="50" max="200" name="tinggi_badan" class="">
                            </td>
                        </tr>
                        <tr>
                            <td><h5>Berat Badan (kg)</h5></td>
                            <td><h5>:</h5></td>
                            <td><input type="number" min="10" max="200" name="berat_badan" class="">
                            </td>
                        </tr>
                        <tr>
                            <td><h5>Golongan Darah  </h5></td>
                            <td><h5>:</h5></td>
                            <td><input type="radio" checked name="goldar" value="a"> A <br>
                                <input type="radio" name="goldar" value="b"> B <br>
                                <input type="radio" name="goldar"  value="ab"> AB <br>
                                <input type="radio" name="goldar" value="O"> O <br>
                            </td>
                        </tr>
                        <tr>
                            <td><h5><b>Penyakit yang Pernah Diderita</b></h5></td>
                        </tr>
                        <tr>
                            <td><h5>Jenis Penyakit</h5></td>
                            <td><h5>:</h5></td>
                            <td><input type="text" name="jenis_penyakit" class="">
                            </td>
                        </tr>
                        <tr>
                            <td><h5>Lama Sakit (bulan)</h5></td>
                            <td><h5>:</h5></td>
                            <td><input type="number" min="0" name="lama_sakit" class="">
                            </td>
                        </tr>
                        <tr>
                            <td><h5>Tahun Mulai Sakit</h5></td>
                            <td><h5>:</h5></td>
                            <td><input type="number" min="1980" name="tahun_sakit" class="">
                            </td>
                        </tr>
                        <tr>
                            <td><h5>Penyandang Cacat</h5></td>
                            <td><h5>:</h5></td>
                            <td><input type="text" name="cacat" class="">
                            </td>
                        </tr>
                        <tr>
                            <td><h5><b>Data Minat dan Bakat</b></h5></td>
                        </tr>
                        <tr>
                            <td><h5>Hobi</h5></td>
                            <td><h5>:</h5></td>
                            <td><textarea type="text" name="hobi" class=""></textarea>
                            </td>
                        </tr>
                        <tr>
                            <td><h5>Prestasi</h5></td>
                            <td><h5>:</h5></td>
                            <td><textarea type="text" name="prestasi" class=""></textarea>
                            </td>
                        </tr>
                        <tr>
                            <td><h5>Mata Pelajaran yang Disenangi</h5></td>
                            <td><h5>:</h5></td>
                            <td><input type="text" name="pelajaran_suka" class="">
                            </td>
                        </tr>
                        <tr>
                            <td><h5>Cita-Cita</h5></td>
                            <td><h5>:</h5></td>
                            <td><input type="text" name="cita_cita" class="">
                            </td>
                        </tr>
                        <tr>
                            <td><br><br><button type="submit" onclick="simpanperubahan()" class="btn btn-info">Simpan</button></td>
                        </tr>

                    </div>
                </div>
            </table>
        </form>
    </div>
  </section>