<!DOCTYPE >
<html lang="en">
  <head>

        <link rel="shortcut icon" type="image/x-icon" href="<?=base_url('assets/img/logo.png')?>">
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="Creative - Bootstrap 3 Responsive Admin Template">
        <meta name="author" content="GeeksLabs">
        <meta name="keyword" content="Creative, Dashboard, Admin, Template, Theme, Bootstrap, Responsive, Retina, Minimal">
        <title><?php echo $title;?></title>
        <!--CSS -->

        <link href="<?php echo base_url();?>assets/css/bootstrap-theme.css" rel="stylesheet">
        <link href="<?php echo base_url();?>assets/css/elegant-icons-style.css" rel="stylesheet" />
        <link href="<?php echo base_url();?>assets/css/font-awesome.min.css" rel="stylesheet" />
        <link href="<?php echo base_url();?>assets/css/owl.carousel.css" rel="stylesheet" />
        <link href="<?php echo base_url();?>assets/css/jquery-jvectormap-1.2.2.css" rel="stylesheet">
        <link href="<?php echo base_url();?>assets/css/widgets.css" rel="stylesheet">
        <link href="<?php echo base_url();?>assets/css/style.css" rel="stylesheet">
        <link href="<?php echo base_url();?>assets/css/style-responsive.css" rel="stylesheet" />
        <link href="<?php echo base_url();?>assets/css/xcharts.min.css" rel=" stylesheet">
        <link href="<?php echo base_url();?>assets/css/jquery-ui-1.10.4.min.css" rel="stylesheet">
        <link href="<?php echo base_url();?>assets/vendor/metisMenu/metisMenu.min.css" rel="stylesheet">
        <link href="<?php echo base_url();?>assets/vendor/morrisjs/morris.css" rel="stylesheet">
        <link href="<?php echo base_url();?>assets/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/assets_datatabel/DataTables/media/css/jquery.dataTables.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/assets_datatabel/DataTables/media/css/dataTables.bootstrap.css">
        <link href="<?php echo base_url();?>assets/css/bar.css" rel=" stylesheet">
        <link href="<?php echo base_url();?>assets/css/style-chart.css" rel=" stylesheet">
        <link href="<?php echo base_url();?>assets/css/bootstrap-datepicker3.min.css" rel=" stylesheet">
        <link href="<?php echo base_url();?>assets/css/bootstrap-datetimepicker.css" rel=" stylesheet">

      <!--Javascript-->
        <script type="text/javascript" src="<?php echo base_url();?>assets/assets_datatabel/DataTables/media/js/jquery.js"></script>
        <script type="text/javascript" src="<?php echo base_url();?>assets/assets_datatabel/DataTables/media/js/jquery.dataTables.js"></script>
        <script src="<?php echo base_url();?>assets/vendor/metisMenu/metisMenu.min.js"></script>
        <script src="<?php echo base_url();?>assets/vendor/raphael/raphael.min.js"></script>
        <script src="<?php echo base_url();?>assets/vendor/morrisjs/morris.min.js"></script>
        <script src="<?php echo base_url();?>assets/js/jquery.canvasjs.min.js"></script>
        <script src="<?php echo base_url();?>assets/js/moment.js"></script>
        <script src="<?php echo base_url();?>assets/js/bootstrap-datepicker.min.js"></script>
        <script src="<?php echo base_url();?>assets/js/bootstrap-datetimepicker.min.js"></script>
        <script src="<?php echo base_url();?>assets/js/jquery.scrollTo.min.js"></script>
        <script src="<?php echo base_url();?>assets/js/jquery.nicescroll.js" type="text/javascript"></script>
        <script src="<?php echo base_url();?>assets/js/jquery.sparkline.js" type="text/javascript"></script>
        <script src="<?php echo base_url();?>assets/js/jquery.rateit.min.js"></script>
        <script src="<?php echo base_url();?>assets/js/jquery.customSelect.min.js" ></script>
        <script src="<?php echo base_url();?>assets/js/scripts.js"></script>
        <script src="<?php echo base_url();?>assets/js/sparkline-chart.js"></script>
        <script src="<?php echo base_url();?>assets/js/jquery-jvectormap-1.2.2.min.js"></script>
        <script src="<?php echo base_url();?>assets/js/jquery-jvectormap-world-mill-en.js"></script>
        <script src="<?php echo base_url();?>assets/js/xcharts.min.js"></script>
        <script src="<?php echo base_url();?>assets/js/jquery.autosize.min.js"></script>
        <script src="<?php echo base_url();?>assets/js/jquery.placeholder.min.js"></script>
        <script src="<?php echo base_url();?>assets/js/gdp-data.js"></script>
        <script src="<?php echo base_url();?>assets/js/morris.min.js"></script>
        <script src="<?php echo base_url();?>assets/js/sparklines.js"></script>
        <script src="<?php echo base_url();?>assets/js/charts.js"></script>
        <script src="<?php echo base_url();?>assets/js/jquery.slimscroll.min.js"></script>
      <script src="<?php echo base_url();?>assets/js/script_chat.js"></script>
<script>
    var base_url="<?php echo base_url();?>";
</script>

  </head>
