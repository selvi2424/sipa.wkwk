<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ajax_pengurus extends CI_Controller {
    protected $base_url;
    public function __construct()
    {        parent::__construct();

        $this->load->model("pembayaranbulanan_model");
        $this->load->model("santri_model");
        $this->load->model("sumbanganinstitusi_model");
        $this->load->model("pengurus_model");
        $this->load->model("pembayaranpendaftaran_model");
        $this->load->model("informasi_model");
        $this->load->model("biaya_model");
        $this->load->model("waktupendaftaran_model");
        $this->base_url = base_url();
    }


//Data Santri
    public function detailsantri($id)
    {
        $data = $this->santri_model->get_by_id($id);

        echo <<<HTML
         <div class="row">
            <div class="col-md-12">
                <section class="panel">
                    <div class="panel-body">
                        <form class="form-horizontal" id="detailsantri" method="get">
                            <table style="font-family:verdana; position:relative;top: -30px;">
                                <tr>
                                    <td><h5><b>Data Pribadi<b></h5></td>
                                </tr>
                                <tr>
                                    <td><h5>Tanggal Penetapan Santri</h5></td>
                                    <td><h5>:</h5></td>
                                    <td><h5>$data->tgl_penetapan</h5></td>
                                </tr>
                                <tr>
                                    <td><h5>NIS<h5></td>
                                    <td><h5>:</h5></td>
                                    <td><h5>$data->nis</h5></td>
                                </tr>
                                <tr>
                                    <td><h5>Jenis Santri</h5></td>
                                    <td><h5>:</h5></td>
                                    <td><h5>$data->nama_biaya</h5></td>
                                </tr><br>
                                <tr>
                                    <td><h5>Nama Lengkap</h5></td>
                                    <td><h5>:</h5></td>
                                    <td><h5>$data->nama_lengkap</h5></td>
                                </tr>
                                <tr>
                                    <td><h5>Nama Panggilan</h5></td>
                                    <td><h5>:</h5></td>
                                    <td><h5>$data->nama_panggilan</h5></td>
                                </tr>
                                <tr>
                                    <td><h5>Jenis Kelamin</h5></td>
                                    <td><h5>:</h5></td>
                                    <td><h5>$data->jenkel</h5></td>
                                </tr>
                                <tr>
                                    <td><h5>Tempat Lahir</h5></td>
                                    <td><h5>:</h5></td>
                                    <td><h5>$data->tempat_lahir</h5></td>
                                </tr>
                                <tr>
                                    <td><h5>Tanggal Lahir</h5></td>
                                    <td><h5>:</h5></td>
                                    <td><h5>$data->tgl_lahir</h5></td>
                                </tr>
                                <tr>
                                    <td><h5>Kewarganegaraan</h5></td>
                                    <td><h5>:</h5></td>
                                    <td><h5>$data->kewarganegaraan</h5></td>
                                </tr>
                                <tr>
                                    <td><h5>Anak Ke-</h5></td>
                                    <td><h5>:</h5></td>
                                    <td><h5>$data->anak_ke</h5></td>
                                </tr>
                                <tr>
                                    <td><h5>Jumlah Saudara Kandung</h5></td>
                                    <td><h5>:</h5></td>
                                    <td><h5>$data->jmlh_saudaraKandung</h5></td>
                                </tr>
                                <tr>
                                    <td><h5>Jumlah Saudara Tiri</h5></td>
                                    <td><h5>:</h5></td>
                                    <td><h5>$data->jmlh_saudaraTiri</h5></td>
                                </tr>
                                <tr>
                                    <td><h5>Jumlah Saudara Angkat</h5></td>
                                    <td><h5>:</h5></td>
                                    <td><h5>$data->jmlh_saudaraAngkat</h5></td>
                                </tr>
                                <tr>
                                    <td><h5>Pendidikan Terakhir</h5></td>
                                    <td><h5>:</h5></td>
                                    <td><h5>$data->pendidikan_terakhir</h5></td>
                                </tr>
                                <tr>
                                    <td><h5>Tahun Lulus</h5></td>
                                    <td><h5>:</h5></td>
                                    <td><h5>$data->tahun_lulus</h5></td>
                                </tr>
                                <tr>
                                    <td><h5><b>Data Tempat Tinggal<b></h5></td>
                                </tr>
                                <tr>
                                    <td><h5>Alamat</h5></td>
                                    <td><h5>:</h5></td>
                                    <td><h5>$data->alamat</h5></td>
                                </tr>
                                <tr>
                                    <td><h5>Kabupaten</h5></td>
                                    <td><h5>:</h5></td>
                                    <td><h5>$data->kabupaten</h5></td>
                                </tr>
                                <tr>
                                    <td><h5>Propinsi</h5></td>
                                    <td><h5>:</h5></td>
                                    <td><h5>$data->propinsi</h5></td>
                                </tr>
                                <tr>
                                    <td><h5>Nomor Telepon</h5></td>
                                    <td><h5>:</h5></td>
                                    <td><h5>$data->no_tlp</h5></td>
                                </tr>
                                <tr>
                                    <td><h5><b> Data Keluarga<b></h5></td>
                                </tr>
                                <tr>
                                    <td><h5><b>ayah<b></h5></td>
                                </tr>
                                <tr>
                                    <td><h5>Nama</h5></td>
                                    <td><h5>:</h5></td>
                                    <td><h5>$data->nama_ayah</h5></td>
                                </tr>
                                <tr>
                                    <td><h5>Pendidikan Terakhir</h5></td>
                                    <td><h5>:</h5></td>
                                    <td><h5>$data->pendidikan_ayah</h5></td>
                                </tr>
                                <tr>
                                    <td><h5>Pekerjaan</h5></td>
                                    <td><h5>:</h5></td>
                                    <td><h5>$data->pekerjaan_ayah</h5></td>
                                </tr>
                                <tr>
                                    <td><h5><b>Ibu<b></h5></td>
                                </tr>
                                <tr>
                                    <td><h5>Nama</h5></td>
                                    <td><h5>:</h5></td>
                                    <td><h5>$data->nama_ibu</h5></td>
                                </tr>
                                <tr>
                                    <td><h5>Pendidikan Terakhir</h5></td>
                                    <td><h5>:</h5></td>
                                    <td><h5>$data->pendidikan_ibu</h5></td>
                                </tr>
                                <tr>
                                    <td><h5>Pekerjaan</h5></td>
                                    <td><h5>:</h5></td>
                                    <td><h5>$data->pekerjaan_ibu</h5></td>
                                </tr>
                                <tr>
                                    <td><h5><b>Wali(Selain Orang Tua)</b></h5></td>
                                </tr>

                                <tr>
                                    <td><h5>Nama</h5></td>
                                    <td><h5>:</h5></td>
                                    <td><h5>$data->nama_wali</h5></td>
                                </tr>
                                <tr>
                                    <td><h5>Pendidikan Terakhir</h5></td>
                                    <td><h5>:</h5></td>
                                    <td><h5>$data->pendidikan_wali</h5></td>
                                </tr>
                                <tr>
                                    <td><h5>Pekerjaan</h5></td>
                                    <td><h5>:</h5></td>
                                    <td><h5>$data->pekerjaan_wali</h5></td>
                                </tr>
                                <tr>
                                    <td><h5><b> Data Kesehatan<b></h5></td>
                                </tr>
                                <tr>
                                    <td><h5>Tinggi Badan(cm)</h5></td>
                                    <td><h5>:</h5></td>
                                    <td><h5>$data->tinggi_badan</h5></td>
                                </tr>
                                <tr>
                                    <td><h5>Berat Badan<kg></h5></td>
                                    <td><h5>:</h5></td>
                                    <td><h5>$data->berat_badan</h5></td>
                                </tr>
                                <tr>
                                    <td><h5><b>Penyakit yang Pernah Diderita<b></h5></td>
                                </tr>

                                <tr>
                                    <td><h5>Jenis</h5></td>
                                    <td><h5>:</h5></td>
                                    <td><h5>$data->jenis_penyakit</h5></td>
                                </tr>
                                <tr>
                                    <td><h5>Lama</h5></td>
                                    <td><h5>:</h5></td>
                                    <td><h5>$data->lama_sakit</h5></td>
                                </tr>
                                <tr>
                                    <td><h5>Tahun</h5></td>
                                    <td><h5>:</h5></td>
                                    <td><h5>$data->tahun_sakit</h5></td>
                                </tr>
                                <tr>
                                    <td><h5>Penyandang Cacat</h5></td>
                                    <td><h5>:</h5></td>
                                    <td><h5>$data->cacat</h5></td>
                                </tr>
                                <tr>
                                    <td><h5><b>Data Minat Dan Bakat<b></h5></td>
                                </tr>
                                <tr>
                                    <td><h5>Hobi</h5></td>
                                    <td><h5>:</h5></td>
                                    <td><h5>$data->hobi</h5></td>
                                </tr>
                                <tr>
                                    <td><h5>Prestasi</h5></td>
                                    <td><h5>:</h5></td>
                                    <td><h5>$data->prestasi</h5></td>
                                </tr>
                                <tr>
                                    <td><h5>Mata Pelajaran Disenangi</h5></td>
                                    <td><h5>:</h5></td>
                                    <td><h5>$data->mapel</h5></td>
                                </tr>
                                <tr>
                                    <td><h5>Cita-cita</h5></td>
                                    <td><h5>:</h5></td>
                                    <td><h5>$data->cita_cita</h5></td>
                                </tr>
                                  </table>
                                </form>
                      </div>
                </section>
            </div>   
        </div>
HTML;
    }

//Pembayaran Pendaftaran
    public function detailpembayaranpendaftaran($id)
    {
        $data = $this->pembayaranpendaftaran_model->get_by_id($id);
        echo <<<HTML
         <div class="row">
            <div class="col-md-12">
                <section class="panel">
                    <div class="panel-body">
                        <form class="form-horizontal" id="detailform" method="get">
                            <table style="font-family:verdana; position:relative;top: -10px;">
                               <tr>
                                    <td><h5>NIS<h5></td>
                                    <td><h5>:</h5></td>
                                    <td><h5>$data->nis</h5></td>
                                </tr>
                                <tr>
                                    <td><h5>Nama Lengkap</h5></td>
                                    <td><h5>:</h5></td>
                                    <td><h5>$data->nama_lengkap</h5></td>
                                </tr>
                                <tr>
                                    <td><h5>Jenis Santri</h5></td>
                                    <td><h5>:</h5></td>
                                    <td><h5>$data->nama_biaya</h5></td>
                                </tr>
                                <tr>
                                    <td><h5>Alamat</h5></td>
                                    <td><h5>:</h5></td>
                                    <td><h5>$data->alamat</h5></td>
                                </tr>
                                <tr>
                                    <td><h5>Nomor Telepon</h5></td>
                                    <td><h5>:</h5></td>
                                    <td><h5>$data->no_tlp</h5></td>
                                </tr>
                                <tr>
                                    <td><h5>Tanggal Bayar</h5></td>
                                    <td><h5>:</h5></td>
                                    <td><h5>$data->tanggal</h5></td>
                                </tr>
                                <tr>
                                    <td><h5>Jumlah Bayar</h5></td>
                                    <td><h5>:</h5></td>
                                    <td><h5>$data->biayapendaftaran</h5></td>
                                </tr>
                             </table>
                            </form>
                      </div>
                </section>
            </div>   
        </div>
HTML;
    }
    public function hapuspembayaranpendaftaran()
    {
        $id =$this->input->post('id_pembayaranpendaftaran');


        if($this->pembayaranpendaftaran_model->deletePembayaranPendaftaran($id))
        {
            echo "Data Berhasil Dihapus";
        }
        else {
            echo "Data Gagal Dihapus";
        }
    }

//Sumbangan Institusi
    public function detailsumbanganinstitusi($id)
    {
        $data = $this->sumbanganinstitusi_model->get_by_id($id);
        echo <<<HTML
         <div class="row">
            <div class="col-md-12">
                <section class="panel">
                    <div class="panel-body">
                        <form class="form-horizontal" id="detailform" method="get">
                            <table style="font-family:verdana; position:relative;top: -10px;">
                                <tr>
                                <td><h5>NIS<h5></td>
                                <td><h5>:</h5></td>
                                <td><h5>$data->nis</h5></td>
                              </tr>
                              <tr>
                                <td><h5>Nama</h5></td>
                                <td><h5>:</h5></td>
                                <td><h5>$data->nama_lengkap</h5></td>
                              </tr>
                              <tr>
                                <td><h5>Jenis Santri</h5></td>
                                <td><h5>:</h5></td>
                                <td><h5>$data->nama_biaya</h5></td>
                              </tr>
                                <tr>
                                    <td><h5>Tanggal Bayar</h5></td>
                                    <td><h5>:</h5></td>
                                    <td><h5>$data->tanggal1</h5></td>
                                </tr>
                              <tr>
                                <td><h5>Cicilan Ke</h5></td>
                                <td><h5>:</h5></td>
                                <td><h5>mas ini gk tau gimana cara ngeluarin isinya kalau count gitu wkwk</h5></td>
                              </tr>
                              <tr>
                                <td><h5>Jumlah</h5></td>
                                <td><h5>:</h5></td>
                                <td><h5>$data->SI_1</h5></td>
                              </tr>
                                <tr>
                                    <td><h5>Bukti Bayar</h5></td>
                                    <td><h5>:</h5></td>
                                    <td><br><img src="wrongname.gif" alt="HTML5 Icon" style="width:128px;height:128px;"></td>
                                </tr>
                             </table>
                            </form>
                      </div>
                </section>
            </div>   
        </div>
HTML;
    }
    public function hapussumbanganinstitusi()
    {
        $id =$this->input->post('id_sumbanganinstitusi');


        if($this->sumbanganinstitusi_model->deleteSumbanganInstitusi($id))
        {
            echo "Data Berhasil Dihapus";
        }
        else {
            echo "Data Gagal Dihapus";
        }
    }

//Pembayaran Bulanan
    public function detailpembayaranbulanan($id)
    {
        $data = $this->pembayaranbulanan_model->get_by_id($id);
        echo <<<HTML
         <div class="row">
            <div class="col-md-12">
                <section class="panel">
                    <div class="panel-body">
                        <form class="form-horizontal" id="detailform" method="get">
                            <table style="font-family:verdana; position:relative;top: -10px;">
                            <tr>
                                <td><h5>NIS<h5></td>
                                <td><h5>:</h5></td>
                                <td><h5>$data->nis</h5></td>
                            </tr>
                            <tr>
                                <td><h5>Nama</h5></td>
                                <td><h5>:</h5></td>
                                <td><h5>$data->nama_lengkap</h5></td>
                            </tr>
                            <tr>
                                <td><h5>Jenis Santri</h5></td>
                                <td><h5>:</h5></td>
                                <td><h5>$data->nama_biaya</h5></td>
                            </tr>
                            <tr>
                                <td><h5>Bulan Bayar</h5></td>
                                <td><h5>:</h5></td>
                                <td><h5>$data->bulan_bayar</h5></td>
                            </tr>
                            <tr>
                                <td><h5>Tanggal Bayar</h5></td>
                                <td><h5>:</h5></td>
                                <td><h5>$data->tanggal</h5></td>
                            </tr>
                            <tr>
                                <td><h5>Bukti Bayar</h5></td>
                                <td><h5>:</h5></td>
                                <td><img src="wrongname.gif" alt="HTML5 Icon" style="width:128px;height:128px;"></td>
                            </tr>
                            <tr>
                                <td><h5>Rincian Pembayaran</h5></td>
                                <td><h5>:</h5></td>
                                <td><h5>
                                        <table style="font-size:12px;" class="table table-striped table-bordered">
                                            <thead>
                                            <tr>
                                                <th>No</th>
                                                <th>Nama</th>
                                                <th>Jumlah</th>
                                            </tr>
                                            </thead>
                                            <tr>
                                                <td>1</td>
                                                <td>Uang Makan
                                                </td>
                                                <td>$data->uang_makan</td>
                                            </tr>
                                            <tr>
                                                <td>2</td>
                                                <td>Syariah Pondok
                                                </td>
                                                <td>$data->syariah_pondok</td>
                                            </tr>
                                            <tr>
                                                <td>3</td>
                                                <td>Khidmad Manaqib
                                                </td>
                                                <td>$data->khidmad_manaqib</td>
                                            </tr>
                                            <tr>
                                                <td>4</td>
                                                <td>Syariah Diniyah
                                                </td>
                                                <td>$data->syariah_diniyah</td>
                                            </tr>
                                            <tr>
                                                <td>5
                                                </td>
                                                <td>Syariah TPQ
                                                </td>
                                                <td>$data->syariah_tpq</td>
                                            </tr>
                                            <tr>
                                                <td>6</td>
                                                <td>Donatur</td>
                                                <td>$data->donatur</td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </h5></td>
                            </tr>
                             </table>
                            </form>
                      </div>
                </section>
            </div>   
        </div>
HTML;
    }
    public function hapuspembayaranbulanan()
    {
        $id =$this->input->post('id_biayabulanan');


        if($this->pembayaranbulanan_model->deletePembayaranBulanan($id))
        {
            echo "Data Berhasil Dihapus";
        }
        else {
            echo "Data Gagal Dihapus";
        }
    }

//Informasi
    public function ubahinformasi($id)
    {
        $data = $this->informasi_model->get_by_id($id);
        echo <<<HTML
         <div class="row">
            <div class="col-md-12">
                <section class="panel">
                    <div class="panel-body">
                        <form class="form-horizontal" action="$this->base_url/ajax_pengurus/simpanubahinformasi" id="ubahinformasi" method="post">
                           <input type="hidden" name="id_informasi"  value="$data->id_informasi"</td>
                            <textarea class="form-control" rows="5" name="isi_informasi" id="informasi">$data->isi_informasi 
                            </textarea>
                        </form>
                    </div>
                </section>
            </div  >   
        </div>
HTML;
    }
    public function simpanubahinformasi()
    {
        $id =$this->input->post('id_informasi');
        $data = array(
            'isi_informasi' => $this->input->post('isi_informasi'),
        );
        if(isset($data) && $this->informasi_model->updateInformasi($id,$data))
        {
            echo "Data Berhasil Diubah";
        }
        else {
            echo "Data Gagal Diubah";
        }
    }
    public function simpaninformasi()
    {
        $data = array(
            'id_informasi' => $this->input->post('id_informasi'),
            'isi_informasi' => $this->input->post('isi_informasi'),
        );

        if(isset($data) && $this->informasi_model->insertInformasi($data))
        {
            echo "Data Berhasil Disimpan";
        }
        else {
            echo "Data Gagal Disimpan";
        }
    }
    public function hapusinformasi()
    {
        $id =$this->input->post('id_informasi');


        if($this->informasi_model->deleteInformasi($id))
        {
            echo "Data Berhasil Dihapus";
        }
        else {
            echo "Data Gagal Dihapus";
        }
    }

//Perubahan Biaya
    public function detailbiaya($id)
    {
        $data = $this->biaya_model->get_by_id($id);
        echo <<<HTML
         <div class="row">
            <div class="col-md-12">
                <section class="panel">
                    <div class="panel-body">
                        <form class="form-horizontal" id="detailform" method="get">
                        <table style="font-size:13px;">
                            <tr>
                                <td>Jenis Biaya</td>
                                <td>:</td>
                                <td>$data->nama_biaya<br></td>
                            </tr>
                            <tr>
                                <td>Waktu Awal Berlaku</td>
                                <td>:</td>
                                <td>$data->waktu_awalberlaku<br></td>
                            </tr>
                            <tr>
                                <td>Waktu Akhir Berlaku</td>
                                <td>:</td>
                                <td>$data->waktu_akhirberlaku<br></td>
                            </tr>
                        </table>
                        <table style="font-size:12px;" class="table table-striped table-bordered data">
                            <thead>
                            <tr>
                                <th>Kategori</th>
                                <th>Jenis Biaya</th>
                                <th>Jumlah</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td rowspan="">Pendaftaran</td>
                                <td>Pendaftaran
                                </td>
                                <td>$data->biayapendaftaran</td>
                            </tr>
                            <tr>
                                <td rowspan="3">Sumbangan Institusi</td>
                                <td>Cicilan 1
                                </td>
                                <td>$data->sumbanganInstitusi_1</td>
                            </tr>
                            <tr>
                                <td>Cicilan 2
                                </td>
                                <td>$data->sumbanganInstitusi_2</td>
                            </tr>
                            <tr>
                                <td>Cicilan 3
                                </td>
                                <td>$data->sumbanganInstitusi_3</td>
                            </tr>
                            <tr>
                                <td rowspan="6">Biaya Bulanan</td>
                                <td> Uang Makan
                                </td>
                                <td>$data->uang_makan</td>
                            </tr>
                            <tr>
                                <td>Syariah Pondok
                                </td>
                                <td>$data->syariah_pondok</td>
                            </tr>
                            <tr>
                                <td>Khidmad Manaqib
                                </td>
                                <td>$data->khidmad_manaqib</td>
                            </tr>
                            <tr>
                                <td>Syariah Diniyah
                                </td>
                                <td>$data->syariah_diniyah</td>
                            </tr>
                            <tr>
                                <td>Syariah TPQ
                                </td>
                                <td>$data->syariah_tpq</td>
                            </tr>
                            <tr>
                                <td>Tabungan Haul
                                </td>
                                <td>$data->tabungan_haul</td>
                            </tr>
                            </tbody>
                        </table>
                    </form>
                      </div>
                </section>
            </div>   
        </div>
HTML;
    }
    public function ubahbiaya($id)
    {
        $data = $this->biaya_model->get_by_id($id);
        echo <<<HTML
         <div class="row">
            <div class="col-md-12">
                <section class="panel">
                    <div class="panel-body">
                        <form class="form-horizontal" action="$this->base_url/ajax_pengurus/simpanubahbiaya" id="ubahbiaya" method="post">
                           <input type="hidden" name="idbiaya"  value="$data->idbiaya"</td>
                        <table style="font-size:12px;">
                              <tr>
                                <td>Jenis Biaya</td>
                                <td>:</td>
                                <td>$data->nama_biaya</td>
                                <input type="hidden" name="nama_biaya"  value="$data->nama_biaya"</td>
                            </tr>
                            <tr> 
                                <td>Waktu Awal Berlaku</td>
                                <td>:</td>
                                <td><div class='input-group date' id='datetimepicker1'>
                                    <input type='text' class="form-control" value="$data->waktu_awalberlaku" name="waktu_awalBerlaku"/>
                                    <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-calendar"></span></span>
                                </div>
                                </td>
                            </tr>
                            <tr>
                                <td>Waktu Akhir Berlaku</td>
                                <td>:</td>
                                <td><div class='input-group date' id='datetimepicker2'>
                                    <input type='text' class="form-control" value="$data->waktu_akhirberlaku"  name="waktu_akhirBerlaku"/>
                                    <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-calendar"></span></span>
                                </div>
                                </td>
                            </tr>
                        </table>
                        <table style="font-size:12px;" class="table table-striped table-bordered data">
                            <thead>
                            <tr>
                                <th>Kategori</th>
                                <th>Jenis Biaya</th>
                                <th>Jumlah</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td>Pendaftaran</td>
                                <td>Pendaftaran
                                </td>
                                <td><input maxlength="6" size="6" type="text" class="form-control" value="$data->biayapendaftaran" name="biayaPendaftaran" ></td>
                            </tr>
                            <tr>
                                <td rowspan="3">Sumbangan Institusi</td>
                                <td>Cicilan 1
                                </td>
                                <td><input maxlength="6" size="6" type="text" class="form-control" value="$data->sumbanganInstitusi_1" name="sumbanganInstitusi_1" ></td>
                            </tr>
                            <tr>
                                <td>Cicilan 2
                                </td>
                                <td><input maxlength="6" size="6" type="text" class="form-control"  value="$data->sumbanganInstitusi_2" name="sumbanganInstitusi_2"></td>
                            </tr>
                            <tr>
                                <td>Cicilan 3
                                </td>
                                <td><input maxlength="6" size="6" type="text" class="form-control"  value="$data->sumbanganInstitusi_3" name="sumbanganInstitusi_3"></td>
                            </tr>
                            <tr>
                                <td rowspan="6">Biaya Bulanan</td>
                                <td> Uang Makan
                                </td>
                                <td><input maxlength="6" size="6" type="text" class="form-control"  value="$data->uang_makan" name="uang_makan"></td>
                            </tr>
                            <tr>
                                <td>Syariah Pondok
                                </td>
                                <td><input maxlength="6" size="6" type="text" class="form-control"  value="$data->syariah_pondok" name="syariah_pondok"></td>
                            </tr>
                            <tr>
                                <td>Khidmad Manaqib
                                </td>
                                <td><input maxlength="6" size="6" type="text" class="form-control" value="$data->khidmad_manaqib" name="khidmad_manaqib"></td>
                            </tr>
                            <tr>
                                <td>Syariah Diniyah
                                </td>
                                <td><input maxlength="6" size="6" type="text" class="form-control" value="$data->syariah_diniyah" name="syariah_diniyah"></td>
                            </tr>
                            <tr>
                                <td>Syariah TPQ
                                </td>
                                <td><input maxlength="6" size="6" type="text" class="form-control" value="$data->syariah_tpq" name="syariah_tpq"></td>
                            </tr>
                            <tr>
                                <td>Tabungan Haul
                                </td>
                                <td><input maxlength="6" size="6" type="text" class="form-control" value="$data->tabungan_haul" name="tabungan_haul"></td>
                            </tr>
                            </tbody>
                        </table>
                        </form>
                         <script type="text/javascript">
                            $(function () {
                                $('#datetimepicker1').datetimepicker();
                                $('#datetimepicker2').datetimepicker();
                            });
                        </script>
                    </div>
                </section>
            </div>   
        </div>
HTML;
    }
    public function simpanubahbiaya()
    {
        $tgl_pembukaan = $this->input->post('waktu_awalBerlaku');
        if(strtotime($tgl_pembukaan) == ''){
            echo "Tanggal pembukaan harus diisi";
            die;
        }
        $tgl_pembukaan = strtotime($tgl_pembukaan);
        $tgl_pembukaan = date('Y-m-d H:i:s', $tgl_pembukaan);

        $tgl_penutupan = $this->input->post('waktu_akhirBerlaku');
        if(strtotime($tgl_penutupan) == ''){
            echo "Tanggal penutupan harus diisi";
            die;
        }
        $tgl_penutupan = strtotime($tgl_penutupan);
        $tgl_penutupan = date('Y-m-d H:i:s', $tgl_penutupan);

        $id =$this->input->post('idbiaya');
        $data = array(
            'nama_biaya' => $this->input->post('nama_biaya'),
            'waktu_awalBerlaku' => $tgl_pembukaan,
            'waktu_akhirBerlaku' =>$tgl_penutupan,
            'biayapendaftaran' => $this->input->post('biayaPendaftaran'),
            'sumbanganInstitusi_1' => $this->input->post('sumbanganInstitusi_1'),
            'sumbanganInstitusi_2' => $this->input->post('sumbanganInstitusi_2'),
            'sumbanganInstitusi_3' => $this->input->post('sumbanganInstitusi_3'),
            'uang_makan' => $this->input->post('uang_makan'),
            'syariah_pondok' => $this->input->post('syariah_pondok'),
            'khidmad_manaqib' => $this->input->post('khidmad_manaqib'),
            'syariah_diniyah'=> $this->input->post('syariah_diniyah'),
            'syariah_tpq'=> $this->input->post('syariah_tpq'),
            'tabungan_haul' => $this->input->post('tabungan_haul'),
        );
        if(isset($data) && $this->biaya_model->updateBiaya($id,$data))
        {
            echo "Data Berhasil Diubah";
        }
        else {
            echo "Data Gagal Diubah";
        }
    }

//Perubahan Waktu
    function simpanwaktupendaftaran(){
        $tgl_pembukaan = $this->input->post('tgl_pembukaan');
        if(strtotime($tgl_pembukaan) == ''){
            echo "Tanggal pembukaan harus diisi";
            die;
        }
        $tgl_pembukaan = strtotime($tgl_pembukaan);
        $tgl_pembukaan = date('Y-m-d H:i:s', $tgl_pembukaan);

        $tgl_penutupan = $this->input->post('tgl_penutupan');
        if(strtotime($tgl_penutupan) == ''){
            echo "Tanggal penutupan harus diisi";
            die;
        }
        $tgl_penutupan = strtotime($tgl_penutupan);
        $tgl_penutupan = date('Y-m-d H:i:s', $tgl_penutupan);
        $data = array(
            'waktu_mulai' => $tgl_pembukaan,
            'waktu_akhir' => $tgl_penutupan,
        );
        if($this->waktupendaftaran_model->insertWaktuPendaftaran($data))
        {
            echo "Data Berhasil Disimpan";
        }
        else{
            echo "Data Gagal Disimpan";
        }
    }
    public function hapuswaktupendaftaran()
    {
        $id =$this->input->post('id_waktu');


        if($this->waktupendaftaran_model->deleteWaktuPendaftaran($id))
        {
            echo "Data Berhasil Dihapus";
        }
        else {
            echo "Data Gagal Dihapus";
        }
    }
    public function ubahwaktupendaftaran($id)
    {
        $data = $this->waktupendaftaran_model->get_by_id($id);
        echo <<<HTML
    <form class="form-horizontal">

         <form action="$this->base_url/ajax_pengurus/simpanubahwaktupendaftaran" id="ubahwaktupendaftaran"class="form-horizontal" method="post" role="form">
            <div class="form-group" data-date-format="YYYY MM DD" >
                <label class="col-sm-2 control-label">Waktu Pembukaan</label>
                <div class="col-sm-10">
                    <div class='input-group date' id='datetimepicker1'>
                        <input type='text' class="form-control" value="$data->waktu_mulai" name="tgl_pembukaan"/>
                        <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span></span>
                    </div>
                </div>
            </div>
            <div class="form-group" >
                <label class="col-sm-2 control-label">Waktu Penutupan</label>
                <div class="col-sm-10">
                    <div class='input-group date' id='datetimepicker2'>
                        <input type='text' value="$data->waktu_akhir" class="form-control"  name="tgl_penutupan"/>
                        <span class="input-group-addon">
                            <span class="glyphicon glyphicon-calendar"></span></span>
                    </div>
                </div>
            </div>
        </form>
        <script type="text/javascript">
                $(function () {
                    $('#datetimepicker1').datetimepicker();
                    $('#datetimepicker2').datetimepicker();
                });
        </script>
HTML;
    }
    public function simpanubahwaktupendaftaran()
    {
        $id =$this->input->post('id_waktu');
        $data = array(
            'waktu_mulai' => $this->input->post('tgl_pembukaan'),
            'waktu_akhir' => $this->input->post('tgl_penutupan'),

        );
        if(isset($data) && $this->waktupendaftaran_model->updateWaktuPendaftaran($id,$data))
        {
            echo "Data Berhasil Diubah";
        }
        else {
            echo "Data Gagal Diubah";
        }
    }
//Rekap Data
}
