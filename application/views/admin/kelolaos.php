<div class="container" style="position:relative;left:90px;" class="col-md-5">
    <div class="container">
    </div><br />
    <br/>
    <br/>
    <h3 style="text-align: center;">Data Santri</h3><hr>
    <!-- Modal Detail-->
    <div class="modal fade" id="detail" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div   class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Data Identitas Santri</h4>
                </div>
                <div id="detailform"" class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <section class="panel">
                                <div class="panel-body">
                                    <form class="form-horizontal " method="get">
                                        <table style="font-family:verdana; position:relative;top: -50px;">
                                            <form>
                                            </form>
                                        </table>
                                </div>
                            </section>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>

        </div>
    </div>
    <!--Modal End Detail-->

    <!-- Modal Sunting-->
    <div class="modal fade" id="sunting" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div  class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Data Identitas Santri</h4>
                </div>
                <div class="modal-body" id="suntingform">
                    <div class="row">
                        <div class="col-md-12">
                            <section class="panel">
                                <div class="panel-body">
                                    <form class="form-horizontal " method="post">
                                </div>
                            </section>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-info" onclick="simpanperubahan()" >Simpan</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
                </div>
            </div>

        </div>
    </div>
    <!--Modal End Sunting-->

    <!-- Modal Ganti Password -->
    <div class="modal fade" id="gantipassword1" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Ubah Password</h4>
                </div>
                <div class="modal-body" id="gantipasswordform">
                    <form class="form-horizontal" role="form">

                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-info" onclick="gantipasswordsantri()">Simpan</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
                </div>
            </div>

        </div>
    </div>
    <!-- Modal End-->

<!--Modal Hapus-->
<div class="modal fade" id="hapus" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h3 class="modal-title">Apakah anda yakin akan menghapus santri?</h3>
            </div>
            <div class="modal-body">
                Jika anda menghapus data tidak dapat dipulihkan kembali
            </div>
            <form action="<?=base_url()?>ajax_admin/hapussantri" id="hapusform" method="post">
                <input id="id_hapus" type="hidden" name="nis">
            </form>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" onclick="hapus()" data-dismiss="modal">Hapus</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
            </div>
        </div>

    </div>
</div>
<!--Modal End Hapus-->

    <script>
        function detail(id)
        {
            $.ajax({url: base_url+"ajax_admin/detailsantri/"+id,
                success: function(result){
                    $('#detail').modal('show');

                    $("#detailform").html(result);

                }});
        }
        function gantipasswordsantri(id)
        {
            $.ajax({url: base_url+"ajax_admin/gantipasswordsantri/"+id,
                success: function(result){
                    $('#gantipassword1').modal('show');

                    $("#gantipasswordform").html(result);

                }});
        }
        function simpanperubahan(){
            var frm = $('#ubahsantri');
            $.ajax({
                type: frm.attr('method'),
                url: frm.attr('action'),
                data: frm.serialize(),
                success: function (data) {
                    alert(data);
                    location.reload();
                    console.log('Submission was successful.');
                    console.log(data);
                },
                error: function (data) {
                    alert("Terjadi kesalahan, jika masih berlanjut hubungi system admin");

                    console.log('An error occurred.');
                    console.log(data);
                },
            });
        }

        function sunting(id)
        {
            $.ajax({url: base_url+"ajax_admin/ubahsantri/"+id,
                success: function(result){
                    $('#sunting').modal('show');

                    $("#suntingform").html(result);
                }});
        }
        function hapus()
        {
            var frm = $('#hapusform');
            $.ajax({
                type: 'post',
                url: frm.attr('action'),
                data: frm.serialize(),
                success: function (data) {
                    location.reload();

                    alert(data);
                    console.log('Submission was successful.');
                    console.log(data);
                },
                error: function (data) {
                    alert("Terjadi kesalahan, jika masih berlanjut hubungi system admin");
                    location.reload();
                    console.log('An error occurred.');
                    console.log(data);
                },
            });
        }
        function updatehapus(row)
        {
            $("#id_hapus").val(row);
        }

        function sunting(id)
        {
            $.ajax({url: base_url+"ajax_admin/ubahsantri/"+id,
                success: function(result){
                    $('#sunting').modal('show');

                    $("#suntingform").html(result);

                }});
        }

    </script>
    <script>

    </script>
    <div >
        <table style="font-size:12px;" class="table table-striped table-bordered data">
            <thead>
            <tr>
                <th>No</th>
                <th>NIS</th>
                <th>Nama</th>
                <th>Alamat</th>
                <th>Jenis Santri</th>
                <th>Aksi</th>
            </tr>
            </thead>
            <tbody>
            <?php  $no=1; foreach ($list as $lists) {
                echo <<<HTML
            <tr>
                <td>$no</td>
                <td>$lists->nis</td>
                <td>$lists->nama_lengkap</td>
                <td>$lists->alamat</td>
                <td>$lists->nama_biaya</td>
                <td>
                    <button type="button" class="btn btn-info btn-xs" onclick="detail($lists->nis)" >Detail</button>
                    <button type="button" class="btn btn-success btn-xs"  onclick="sunting($lists->nis)">Sunting</button>
                    <button type="button" class="btn btn-warning btn-xs" onclick="suntingpassword($lists->nis)" data-toggle="modal" data-target="#gantipassword1">Sunting Password</button>
                    <button type="button" class="btn btn-danger btn-xs" onclick="updatehapus($lists->nis)" data-toggle="modal" data-target="#hapus">Hapus</button>
                </td>
            </tr>
HTML;
                $no++;           } ?>
            </tbody>
        </table>
        <script type="text/javascript">
            $(document).ready(function(){
                $('.data').DataTable({columnDefs: [
                    { orderable: false, targets: -1 }
                ]});

            });
        </script>
    </div>
