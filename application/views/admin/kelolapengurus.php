<div class="container" style="position:relative;left:90px;" class="col-md-5">
    <div class="container">
    </div><br />
    <br/>
    <br/>
    <h3 style="text-align: center;">Data Pengurus</h3><hr>

    <button type="button" class="btn btn-info btn-sm" data-toggle="modal" data-target="#tambahPengurus">Tambah Pengurus</button>
    <!-- Modal tambah Pengurus-->
    <div class="modal fade" id="tambahPengurus" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Tambah Pengurus</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <section class="panel">
                                <div class="panel-body">
                                    <form class="form-horizontal" id="formPengurus" action="<?=base_url()?>/ajax_admin/simpanpengurus" method="post">
                                        <table style="font-family:verdana; position:relative;top: -10px;">
                                            <form>
                                                <tr>
                                                    <td><h5>ID Pengurus</h5></td>
                                                    <td><h5>:</h5></td>
                                                    <td style="font-size: 13px;"><?php echo $last_id;?></td>
                                                </tr>
                                                <tr>
                                                    <td><h5>Nama Pengurus</h5></td>
                                                    <td><h5>:</h5></td>
                                                    <td><input type="text" class="form-control" name="nama" required style="font-size: 13px;"></td>
                                                </tr>
                                                <tr>
                                                    <td><h5>Password Baru</h5></td>
                                                    <td><h5>:</h5></td>
                                                    <td><input type="password" class="form-control" name="password" required style="font-size: 13px;"></td>
                                                </tr>
                                                <tr>
                                                    <td><h5>Konfirmasi Password</h5></td>
                                                    <td><h5>:</h5></td>
                                                    <td><input type="password" class="form-control" name="confirm_password" required style="font-size: 13px;"></td>
                                                </tr>
                                            </form>
                                        </table>
                                </div>
                            </section>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-info" onclick="simpan()" data-dismiss="modal">Simpan</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
                </div>
            </div>

        </div>
    </div>
    <!-- Modal tambah Pengurus End-->

    <!--Modal Hapus-->
    <div class="modal fade" id="hapus" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h3 class="modal-title">Apakah anda yakin akan menghapus pengurus?</h3>
                </div>
                <div class="modal-body">
                    Jika anda menghapus data tidak dapat dipulihkan kembali
                </div>
                <form action="<?=base_url()?>/ajax_admin/hapuspengurus" id="hapusform" method="post">
                    <input id="id_hapus" type="hidden" name="id_pengurus">
                </form>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" onclick="hapus()" data-dismiss="modal">Hapus</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
                </div>
            </div>

        </div>
    </div>
    <!--Modal End Hapus-->

    <!-- Modal Sunting Pengurus-->
    <div class="modal fade" id="sunting" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Data Pengurus</h4>
                </div>
                <div class="modal-body" id="suntingform">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-info" onclick="simpanperubahan()" >Simpan</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
                </div>
            </div>

        </div>
    </div>
    <!--Modal Sunting End-->

    <script>
        function simpanperubahan(){
            var frm = $('#ubahpengurus');
            $.ajax({
                type: frm.attr('method'),
                url: frm.attr('action'),
                data: frm.serialize(),
                success: function (data) {
                    alert(data);
                    location.reload();
                    console.log('Submission was successful.');
                    console.log(data);
                },
                error: function (data) {
                    alert("Terjadi kesalahan, jika masih berlanjut hubungi system admin");

                    console.log('An error occurred.');
                    console.log(data);
                },
            });
        }
        function simpan()
        {
            var frm = $('#formPengurus');
            $.ajax({
                type: frm.attr('method'),
                url: frm.attr('action'),
                data: frm.serialize(),
                success: function (data) {
                    alert(data);
                    location.reload();
                    console.log('Submission was successful.');
                    console.log(data);
                },
                error: function (data) {
                    alert("Terjadi kesalahan, jika masih berlanjut hubungi system admin");

                    console.log('An error occurred.');
                    console.log(data);
                },
            });
        }
        function hapus()
        {
            var frm = $('#hapusform');
            $.ajax({
                type: 'post',
                url: frm.attr('action'),
                data: frm.serialize(),
                success: function (data) {
                    alert(data);
                    location.reload();
                    console.log('Submission was successful.');
                    console.log(data);
                },
                error: function (data) {
                    alert("Terjadi kesalahan, jika masih berlanjut hubungi system admin");
                    location.reload();
                    console.log('An error occurred.');
                    console.log(data);
                },
            });
        }
        function updatehapus(row)
        {
            $("#id_hapus").val(row);
        }

        function sunting(id)
        {
            $.ajax({url: base_url+"ajax_admin/ubahpengurus/"+id,
                success: function(result){
                    $('#sunting').modal('show');

                    $("#suntingform").html(result);

                }});
        }
    </script>

    <div >
        <table style="font-size:12px;" class="table table-striped table-bordered data">
            <thead>
            <tr>
                <th>No</th>
                <th>Id Pengurus</th>
                <th>Nama</th>
                <th>Aksi</th>
            </tr>
            </thead>
            <tbody>
            <?php $no=1; foreach($list as $lists) {

                echo <<<HTML
                <tr>
                    <td>$no</td>
                    <td>$lists->id_pengurus</td>
                    <td>$lists->nama</td>
                 <td>
                        <button type="button" class="btn btn-success btn-xs" onclick="sunting($lists->id_pengurus)">Sunting</button>
                        <button type="button" class="btn btn-danger btn-xs" onclick="updatehapus($lists->id_pengurus)" data-toggle="modal" data-target="#hapus">Hapus</button>
                    </td>
                </tr>
HTML;
                $no++;

            } ?>
            </tbody>
            <script type="text/javascript">
                $(document).ready(function(){
                    $('.data').DataTable({columnDefs: [
                        { orderable: false, targets: -1 }
                    ]});

                });
            </script>
    </div>