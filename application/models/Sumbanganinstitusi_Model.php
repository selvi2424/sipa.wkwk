<?php

class Sumbanganinstitusi_Model extends CI_Model{
    public $primary_key = 'id_sumbanganinstitusi';
    public $table_sumbanganinstitusi = 'sumbanganinstitusi';

    function __construct(){
        parent::__construct();
    }
//PENGURUS
    function getAllRecords(){
        $query =   $this->db->select('t1.id_sumbanganinstitusi, t1.nis as nis, t2.nama_lengkap as nama_lengkap, t3.nama_biaya as nama_biaya, 
                                    t3.sumbanganInstitusi_1 as SI_1, t3.sumbanganInstitusi_2 as SI_2,
                                    t3.sumbanganInstitusi_3 as SI3, t1.tanggal1 as tanggal1, t1.tanggal1 as tanggal1,
                                    t1.tanggal2 as tanggal2, t1.tanggal3 as tanggal3, t1.bukti_sumbanganInstitusi1 as bukti_SI1, 
                                    t1.bukti_sumbanganInstitusi2 as bukti_SI2, t1.bukti_sumbanganInstitusi3 as bukti_SI3, 
                                    t1.id_pengurus1 as pengurus1, t1.id_pengurus2 as pengurus2, t1.id_pengurus3 as pengurus3')
            ->from('sumbanganinstitusi as t1')
            ->join('santri as t2', 't1.nis = t2.nis','left')
            ->join('biaya as t3', 't1.idbiaya = t3.idbiaya', 'left')
            ->get();
        return $query->result();

    }

    function insertSumbanganInstitusi($data){
        $result =  $this->db->insert($this->table_sumbanganinstitusi,$data);
        return $result; // Kode ini digunakan untuk mengembalikan hasil $res

    }

    public function get_by_id($id)
    {
        $query =   $this->db->select('t1.*, t2.nama_lengkap as nama_lengkap, t3.nama_biaya as nama_biaya, 
                                    t3.sumbanganInstitusi_1 as SI_1, t3.sumbanganInstitusi_2 as SI_2,
                                    t3.sumbanganInstitusi_3 as SI3')
            ->from('sumbanganinstitusi as t1')
            ->join('santri as t2', 't1.nis = t2.nis','left')
            ->join('biaya as t3', 't1.idbiaya = t3.idbiaya', 'left')
             ->where('id_sumbanganinstitusi', $id)
            ->get();
        return $query->row();
    }

    function updateSumbanganInstitusi($sumbanganinstitusi_id,$data){
        $this->db->where('id_sumbanganinstitusi',$sumbanganinstitusi_id); // Kode ini digunakan untuk merubah record yang sudah ada dalam sebuah tabel
        $result = $this->db->update($this->table_sumbanganinstitusi, $data); // Kode ini digunakan untuk merubah record yang sudah ada dalam sebuah tabel
        return $result;
    }

    public function deleteSumbanganInstitusi($sumbanganinstitusi_id){
        $this->db->where('id_sumbanganinstitusi',$sumbanganinstitusi_id); // Kode ini digunakan untuk merubah record yang sudah ada dalam sebuah tabel

        $result = $this->db->delete($this->table_sumbanganinstitusi); // Kode ini digunakan untuk menghapus record yang sudah ada
        return $result;

    }

    public function jumlahSumbanganInstitusi(){
        $query = $this->db->query("SELECT t1.*, t2.id_waktu as id_waktu, count(nis) AS nis FROM santri as t1, waktupendaftaran as t2 where t1.id_waktu=t2.id_waktu");
        // print_r($query->result());
        return $query->result();
    }
//SANTRI
    function getAllRecordByNis(){
        $user_id = $this->session->userdata('nis');
        var_dump($this->session->userdata('nis'));
        $query =   $this->db->select('t1.id_sumbanganinstitusi, t1.nis as nis, t2.nama_lengkap as nama_lengkap, t3.nama_biaya as nama_biaya, 
                                    t3.sumbanganInstitusi_1 as SI_1, t3.sumbanganInstitusi_2 as SI_2,
                                    t3.sumbanganInstitusi_3 as SI3, t1.tanggal1 as tanggal1, t1.tanggal1 as tanggal1,
                                    t1.tanggal2 as tanggal2, t1.tanggal3 as tanggal3, t1.bukti_sumbanganInstitusi1 as bukti_SI1, 
                                    t1.bukti_sumbanganInstitusi2 as bukti_SI2, t1.bukti_sumbanganInstitusi3 as bukti_SI3, 
                                    t1.id_pengurus1 as pengurus1, t1.id_pengurus2 as pengurus2, t1.id_pengurus3 as pengurus3')
            ->from('sumbanganinstitusi as t1')
            ->join('santri as t2', 't1.nis = t2.nis','left')
            ->join('biaya as t3', 't1.idbiaya = t3.idbiaya', 'left')
            ->where('t1.nis="'.$user_id.'"')
            ->get();
        return $query->result();
    }
}
?>
