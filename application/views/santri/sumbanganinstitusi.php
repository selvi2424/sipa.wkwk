<div class="container" style="position:relative;left:90px;" class="col-md-5">
    <br/>
    <br/><br>
    <h3 style="text-align: center;">Sumbangan Institusi</h3><hr>
    <!-- Modal Pembayaran Sumbangan Institusi-->
    <div class="modal fade" id="modalBayar" role="dialog">
      <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Pembayaran Sumbangan Institusi</h4>
          </div>
          <div class="modal-body">
            <div class="row">
              <div class="col-md-12">
                <section class="panel">
                    <div class="panel-body">
                        <form class="form-horizontal " style="position: relative;top: -30px;" method="get">
                          <table style="top: -20px;font-family:verdana;">
                              <tr>
                                <td><h5><b>Data Sumbangan Institusi<b></h5></td>
                              </tr>
                              <tr>
                                <td><h5>NIS<h5></td>
                                <td><h5>:</h5></td>
                                  <input type="hidden" name="nis" value="$lists->nis">
                                <td><h5>$lists->nis</h5></td>
                              </tr>
                              <tr>
                                  <td><h5>Jenis Santri</h5></td>
                                  <td><h5>:</h5></td>
                                  <input type="hidden" name="nama_biaya" value="$lists->nama_biaya">
                                  <td><h5>$lists->nama_biaya</h5></td>
                              </tr><br>
                              <tr>
                                  <td><h5>Pembayaran Cicilan Pertama</h5></td>
                                  <td>:</td>
                                  <td><input type="checkbox" name="cicilan_1" value="$lists->cicilan_1">$lists->cicilan_1<br></td>
                              </tr>
                              <tr>
                                  <td><h5>Pembayaran Cicilan Kedua</h5></td>
                                  <td>:</td>
                                  <td><input type="checkbox" name="cicilan_2" value="$lists->cicilan_2">$lists->cicilan_2<br></td>
                              </tr>
                              <tr>
                                  <td><h5>Pembayaran Cicilan Ketiga</h5></td>
                                  <td>:</td>
                                  <td><input type="checkbox" name="cicilan_3" value="$lists->cicilan_3">$lists->cicilan_2<br></td>
                              </tr>
                              <tr>
                                  <td><h5>Upload Bukti Pembayaran</h5></td>
                                  <td>:</td>
                                  <td><input type="file" name="bukti_sumbanganinstitusi"></td>
                              </tr>
                          </table>
                        </form>
                    </div>
                </section>
              </div>
            </div>
          </div>
          <div class="modal-footer">
              <button type="button" class="btn btn-info" data-dismiss="modal">Simpan</button>
              <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>

          </div>
        </div>

      </div>
    </div>
    <!--Modal Bayar End-->

    <!-- Detail Pembayaran Sumbangan Institusi-->
    <div class="modal fade" id="detail" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Pembayaran Sumbangan Institusi</h4>
                </div>
                <div class="modal-body" id="detailform">
                    <div class="row">
                        <div class="col-md-12">
                            <section class="panel">
                                <div class="panel-body">
                                    <form class="form-horizontal " style="position: relative;top: -30px;" method="get">
                                        <table style="top: -20px;font-family:verdana;">
                                            <tr>
                                                <td><h5>NIS<h5></td>
                                                <td><h5>:</h5></td>
                                                <td><h5>17002008</h5></td>
                                            </tr>
                                            <tr>
                                                <td><h5>Jenis Santri</h5></td>
                                                <td><h5>:</h5></td>
                                                <td><h5>Menetap</h5></td>
                                            </tr><br>
                                            <tr>
                                                <td><h5>Pembayaran Cicilan Pertama</h5></td>
                                                <td>:</td>
                                                <td>Sudah Dibayar<br></td>
                                            </tr>
                                            <tr>
                                                <td><br><img src="wrongname.gif" alt="HTML5 Icon" style="width:128px;height:128px;"></td>
                                            </tr>
                                            <tr>
                                                <td><h5>Pembayaran Cicilan Kedua</h5></td>
                                                <td>:</td>
                                                <td>Sudah Dibayar<br></td>
                                            </tr>
                                            <tr>
                                                <td><br><img src="coba.jpg" alt="HTML5 Icon" style="width:128px;height:128px;"></td>
                                            </tr>
                                            <tr>
                                                <td><h5>Pembayaran Cicilan Ketiga</h5></td>
                                                <td>:</td>
                                                <td>Sudah Dibayar<br></td>
                                            </tr>
                                            <tr>
                                                <td><br><img src="coba.jpg" alt="HTML5 Icon" style="width:128px;height:128px;"></td>
                                            </tr>
                                        </table>
                                    </form>
                                </div>
                            </section>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>

                </div>
            </div>

        </div>
    </div>
    <!--Modal End-->
    <script>
        function detail(id)
        {
            $.ajax({url: base_url+"ajax_santri/detailsumbanganinstitusi/"+id,
                success: function(result){
                    $('#detail').modal('show');

                    $("#detailform").html(result);

                }});
        }
        function simpan()
        {
            var frm = $('#formSumbanganInstitusi');
            $.ajax({
                type: frm.attr('method'),
                url: frm.attr('action'),
                data: frm.serialize(),
                success: function (data) {
                    location.reload();
                    alert(data);
                    console.log('Submission was successful.');
                    console.log(data);
                },
                error: function (data) {
                    alert("Terjadi kesalahan, jika masih berlanjut hubungi system admin");

                    console.log('An error occurred.');
                    console.log(data);
                },
            });
        }
    </script>
        <button type="button" class="btn btn-info btn-sm" data-toggle="modal" data-target="#modalBayar">Bayar</button>
    <br><br>
    <div >
    <table style="font-size:12px;" class="table table-striped table-bordered data">
      <thead>
        <tr>
          <th>No</th>
          <th>Cicilan Ke</th>
          <th>Tanggal Bayar</th>
          <th>Aksi</th>
        </tr>
      </thead>
      <tbody>
      <?php $no=1;foreach($list_sumbanganinstitusi as $lists) {

          echo <<<HTML
            <tr>
                <td>$no</td>
                <td>$lists->SI_1</td>
                <td>$lists->tanggal1</td>
             <td>
                    <button type="button" class="btn btn-success btn-xs" data-toggle="modal" data-target="#detail">Belum Konfirmasi</button>
                    <button type="button" class="btn btn-info btn-xs" onclick="detail($lists->id_sumbanganinstitusi)" data-toggle="modal" data-target="#detail">Detail</button>
                </td>
            </tr>
HTML;
          $no++;

      } ?>
      </tbody>
    </table>
    </div>
</div>

